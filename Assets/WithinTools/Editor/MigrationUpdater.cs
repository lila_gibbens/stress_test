﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    public static class MigrationUpdater
    {
        private struct MigrationVersion
        {
            public delegate void UpgradeFunctionDelegate();
            
            public readonly Version Version;
            public readonly string[] DeletedFiles;
            public readonly KeyValuePair<string, string>[] RenamedFiles;
            public readonly UpgradeFunctionDelegate UpgradeFunction;
    
            public MigrationVersion(string version, string[] deletedFiles, KeyValuePair<string, string>[] renamedFiles, UpgradeFunctionDelegate upgradeFunction = null)
            {
                Version = new Version(version);
                DeletedFiles = deletedFiles;
                RenamedFiles = renamedFiles;
                UpgradeFunction = upgradeFunction ?? delegate { };
            }
        }
    
        private static readonly MigrationVersion[] migrationVersions =
        {
            new MigrationVersion(
                "0.5",
                new string[] { },
                new KeyValuePair<string, string>[] { },
                StoryEditor.MigrationFix_Version0dot5), 
            new MigrationVersion(
                "0.6",
                new string[] { }, 
                new KeyValuePair<string, string>[]
                {
                    new KeyValuePair<string, string>("Assets/WithinTools/Scripts/Sequencer/Playables/EndOfPagePlayable", "Assets/WithinTools/Scripts/Sequencer/Playables/PageManagement")
                }),
            new MigrationVersion(
                "0.7",
                new string[] { }, 
                new KeyValuePair<string, string>[]
                {
                    new KeyValuePair<string, string>("Assets/WithinTools/Scripts/InteractSystem/InteractBehaviours/InteractBehaviour_FinishCurrentLoop.cs", 
                        "Assets/WithinTools/Scripts/InteractSystem/InteractBehaviours/InteractBehaviour_AddSkipLoopCounter.cs")
                }),
            new MigrationVersion(
                "0.8",
                new string[] { }, 
                new KeyValuePair<string, string>[] { },
                MigrationFix_Version0dot8)
        };
    
        public static void AutoUpdate()
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode)
            {
                return;
            }
    
            if (SBAssets.GetStoryData() == null)
            {
                return;
            }
            
            if (!File.Exists(SBToolsPaths.STORY_TOOL_VERSION_FILE_PATH))
            {
                File.WriteAllText(SBToolsPaths.STORY_TOOL_VERSION_FILE_PATH, "0.0");
            }
    
            Version oldVersion = new Version(File.ReadAllText(SBToolsPaths.STORY_TOOL_VERSION_FILE_PATH).Split(new[] { " " }, StringSplitOptions.None)[0]);
            foreach (MigrationVersion version in migrationVersions)
            {
                if (oldVersion < version.Version)
                {
                    PerformUpgrade(version);
                }
            }
            
            File.WriteAllText(SBToolsPaths.STORY_TOOL_VERSION_FILE_PATH, WithinEditorTools.GetToolVersion());
            
            AssetDatabase.Refresh();
        }
    
        private static void PerformUpgrade(MigrationVersion migration)
        {
            foreach (string asset in migration.DeletedFiles)
            {
                AssetDatabase.DeleteAsset(asset);
            }
    
            foreach (KeyValuePair<string, string> rename in migration.RenamedFiles)
            {
                AssetDatabase.MoveAsset(rename.Key, rename.Value);
            }
    
            migration.UpgradeFunction();
            
            Debug.Log("[Migration] Successfully updated to " + migration.Version);
        }
        
        #if SB_DEBUG_ENABLED
        [MenuItem("AR Books/Debug/Upgrade 0.8")]
#endif
        public static void MigrationFix_Version0dot8()
        {
            var storyDataToUpdate = SBAssets.GetStoryData();
            if (storyDataToUpdate == null)
            {
                return;
            }
            
            foreach (StoryPageData pageData in storyDataToUpdate.DataPages)
            {
                TimelineAsset timeline = pageData.Sequence;

                TrackAsset startPageTrackAsset = null;
                TrackAsset endPageTrackAsset   = null;
                TrackAsset pageManagementTrackAsset   = null;
                
                for (int i = 0; i < timeline.outputTrackCount; i++)
                {
                    if (timeline.GetOutputTrack(i).GetType().Name == "StartPagePlayableTrack")
                    {
                        startPageTrackAsset = timeline.GetOutputTrack(i);
                    }
                    else if (timeline.GetOutputTrack(i).GetType().Name == "EndOfPagePlayableTrack")
                    {
                        endPageTrackAsset = timeline.GetOutputTrack(i);
                    }
                    else if (timeline.GetOutputTrack(i).GetType() == typeof(PageManagementPlayableTrack))
                    {
                        pageManagementTrackAsset = timeline.GetOutputTrack(i);
                    } 
                }
                
                //-- if there is no tracks to migrate, just finish checking for this page
                if (startPageTrackAsset == null && endPageTrackAsset == null)
                {
                    continue;
                }

                // -1 means there is no need to override the value
                double startPageClipStart = -1f; 
                double startPageClipDuration = -1f;
                
                double endPageClipStart = -1f;
                double endPageClipDuration = -1f;

                if (startPageTrackAsset != null)
                {
                    IEnumerable<TimelineClip> clips = startPageTrackAsset.GetClips();
                    
                    if (clips.Any())
                    {
                        TimelineClip clip = clips.First();
                        startPageClipStart = clip.start;
                        startPageClipDuration = clip.duration;
                    }
                }
                
                if (endPageTrackAsset != null)
                {
                    IEnumerable<TimelineClip> clips = endPageTrackAsset.GetClips();
                    
                    if (clips.Any())
                    {
                        TimelineClip clip = clips.First();
                        endPageClipStart = clip.start;
                        endPageClipDuration = clip.duration;
                    }
                }

                if (pageManagementTrackAsset == null)
                {
                    pageManagementTrackAsset = timeline.CreateTrack<PageManagementPlayableTrack>(null, "Page Management");    
                }

                IEnumerable<TimelineClip> pageManagementClips = pageManagementTrackAsset.GetClips();

                TimelineClip startClip = null;
                TimelineClip endClip = null;
                
                foreach (TimelineClip clip in pageManagementClips)
                {
                    if (clip.asset.GetType() == typeof(StartPagePlayableClip))
                    {
                        startClip = clip;
                    }
                    else if (clip.asset.GetType() == typeof(EndOfPagePlayableClip))
                    {
                        endClip = clip;
                    }
                }

                if (startClip == null)
                {
                    startClip = pageManagementTrackAsset.CreateClip<StartPagePlayableClip>();    
                }

                if (endClip == null)
                {
                    endClip = pageManagementTrackAsset.CreateClip<EndOfPagePlayableClip>();   
                }

                // if startClip start/duration requires override
                if (startPageClipStart != -1)
                {
                    startClip.start = startPageClipStart;
                    startClip.duration = startPageClipDuration;    
                }

                // if endClip start/duration requires override
                if (endPageClipStart != -1)
                {
                    endClip.start = endPageClipStart;
                    endClip.duration = endPageClipDuration;    
                }

                if (startPageTrackAsset != null)
                {
                    timeline.DeleteTrack(startPageTrackAsset);
                }
                if (endPageTrackAsset != null)
                {
                    timeline.DeleteTrack(endPageTrackAsset);
                }
            } // end of foreach page
            
            #if !SB_MAIN_APP
            File.Delete(Application.dataPath + "/WithinTools/Scripts/Sequencer/Playables/PageManagement/StartPagePlayableTrack.cs");
            File.Delete(Application.dataPath + "/WithinTools/Scripts/Sequencer/Playables/PageManagement/EndOfPagePlayableTrack.cs");
            #endif
            
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
        }
    }
}