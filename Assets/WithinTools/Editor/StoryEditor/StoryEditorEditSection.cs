﻿using CrazyMinnow.SALSA;
using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
    public class StoryEditorEditSection : WithinEditorWindowSection<StoryEditorWindow>
    {
        public StoryEditorEditSection(StoryEditorWindow _parentWindow) : base(_parentWindow) { }

        private bool moreOptionsFoldout;

        /// <summary>
        /// This is used to call the actions executed by the buttons, AFTER the layouts finish drawing
        /// to avoid UI stack errors because of the delay of the actions
        /// </summary>
        private delegate void DoOnFinishDrawing_Delegate();
        private DoOnFinishDrawing_Delegate doOnFinishDrawing = delegate { };
        
        protected override void DoDraw()
        {
            // SelectedPageData becomes null momentarily during story export
            if (!StoryEditor.Instance.IsDataReady)
            {
                return;
            }

            string labelSection = string.Format("Page {0} Details", (StoryEditor.Instance.SelectedIndex + 1)); 
            GUILayout.Label(labelSection, EditorStyles.boldLabel);

            using (new GUI_VERTICAL_LAYOUT("Box", GUILayout.MinWidth(200), GUILayout.MaxWidth(1200), GUILayout.ExpandWidth(true)))
            {
                int textPartsQty = StoryEditor.Instance.SelectedPageData.PartsQty;

                StoryEditor.Instance.SelectedPageData.Passage.Text = EditorGUILayout.TextField("Passage Text: ",
                StoryEditor.Instance.SelectedPageData.Passage.Text, GUILayout.MinHeight(40), GUILayout.MaxHeight(90));
                StoryEditor.Instance.SelectedPageData.UpdateClips();
                
                using (new GUI_CHANGE_CHECK(() => StoryEditor.Instance.SetNarrationIsAvailable()))
                {
                    for (int i = 0; i < StoryEditor.Instance.SelectedPageData.PartsQty; i++)
                    {
                        StoryEditor.Instance.SelectedPageData.PreRecordedClips[i] = EditorGUILayout.ObjectField(
                            "Pre Recorded Audio:",
                            StoryEditor.Instance.SelectedPageData.PreRecordedClips[i],
                            typeof(AudioClip),
                            false,
                            GUILayout.Width(400)) as AudioClip;    
                    }
                }

                if (textPartsQty == StoryEditor.Instance.SelectedPageData.PartsQty)
                {
                    StoryEditor.Instance.SetNarrationIsAvailable();
                }

                StoryEditor.Instance.SelectedPageInstance.SalsaCharacter = EditorGUILayout.ObjectField(
                    "LipSync Character:",
                    StoryEditor.Instance.SelectedPageInstance.SalsaCharacter,
                    typeof(Salsa3D),
                    true,
                    GUILayout.Width(400)) as Salsa3D;

                if (GUILayout.Button("Open Timeline", GUILayout.MaxWidth(180)))
                {
                    doOnFinishDrawing += ExecuteOpenTimeline;
                }

                moreOptionsFoldout = EditorGUILayout.Foldout(moreOptionsFoldout, "More Options");
                if (moreOptionsFoldout)
                {
                    Draw_InsideFoldout();
                }
            }

            doOnFinishDrawing();
        }
        
        private void Draw_InsideFoldout()
        {
            GUILayout.BeginHorizontal();
                DrawButton_MoveDown();
                DrawButton_MoveUp();
            GUILayout.EndHorizontal();
    
            DrawButton_DeletePage();
        }
        
        private void DrawButton_MoveDown()
        {
            bool disabled = StoryEditor.Instance.SelectedIndex < 1;
            
            EditorGUI.BeginDisabledGroup(disabled);
            
            if (GUILayout.Button(" < Move Down", GUILayout.MaxWidth(120)))
            {
                doOnFinishDrawing += ExecuteMovePageDown;
            }
            
            EditorGUI.EndDisabledGroup();
        }

        private void DrawButton_MoveUp()
        {
            bool disabled = StoryEditor.Instance.SelectedIndex + 1 >= StoryEditor.Instance.PageCount;
            
            EditorGUI.BeginDisabledGroup(disabled);
            
            if (GUILayout.Button("Move Up > ", GUILayout.MaxWidth(120)))
            {
                doOnFinishDrawing += ExecuteMovePageUp;
            }
            
            EditorGUI.EndDisabledGroup();
        }

        private void DrawButton_DeletePage()
        {
            bool isValid = StoryEditor.Instance.PageCount > 0;
            EditorGUI.BeginDisabledGroup(!isValid);
            if (GUILayout.Button("Delete Page " + (StoryEditor.Instance.SelectedIndex + 1) + " From Story", GUILayout.MaxWidth(180)))
            {
                doOnFinishDrawing += ExecuteDeletePage;
            }
            EditorGUI.EndDisabledGroup();
        }

        private void ExecuteOpenTimeline()
        {
            doOnFinishDrawing -= ExecuteOpenTimeline;
            
            EditorApplication.ExecuteMenuItem("Window/Timeline");
        }

        private void ExecuteMovePageUp()
        {
            doOnFinishDrawing -= ExecuteMovePageUp;

            StoryEditor.Instance.MoveSelectedPageUp();
        }
        
        private void ExecuteMovePageDown()
        {
            doOnFinishDrawing -= ExecuteMovePageDown;
            
            StoryEditor.Instance.MoveSelectedPageDown();
        }
        
        private void ExecuteDeletePage()
        {
            doOnFinishDrawing -= ExecuteDeletePage;
            
            if (
                EditorUtility.DisplayDialog(
                    "Delete " + StoryEditor.Instance.SelectedPageData.PageName + " ?",
                    "Are you sure you wish to completely remove the selected Page from the Story?", "Delete", "Cancel"))
            {
                StoryEditor.Instance.DeleteSelectedPage();
            }
        }
    }
}
    