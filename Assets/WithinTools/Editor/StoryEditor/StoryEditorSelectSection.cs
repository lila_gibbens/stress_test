﻿﻿using System;
using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
    public class StoryEditorSelectSection: WithinEditorWindowSection<StoryEditorWindow>
    {
        private int selectedIndex = 0;

        private string[] pageOptions;
        
        public StoryEditorSelectSection(StoryEditorWindow _parentWindow) : base(_parentWindow)
        {
            SBEditorEvents.onPageSelectionChange.AddListener(OnSelectionChange);
        }
        
        protected override void DoDraw()
        {
            if (!StoryEditor.Instance.IsInited)
            {
                return;
            }
            
            GUI.SetNextControlName("Story_Pages");
            GUILayout.Label("Page Select", EditorStyles.boldLabel);

            SetSelectionOptions();
            
            using (new GUI_VERTICAL_LAYOUT("Box", GUILayout.MinWidth(200), GUILayout.MaxWidth(1200), GUILayout.ExpandWidth(true)))
            {
                using (new GUI_CHANGE_CHECK(OnIndexChange))
                {
                    selectedIndex = GUILayout.SelectionGrid(selectedIndex, pageOptions, 22, EditorStyles.radioButton);    
                } // end Change Check
            } // end Vertical Layout
        }

        private void SetSelectionOptions()
        {
            if (pageOptions != null && pageOptions.Length == StoryEditor.Instance.PageCount)
                return;
                    
            pageOptions = new string[StoryEditor.Instance.PageCount];

            for (int i = 1; i <= StoryEditor.Instance.PageCount; i++)
            {
                pageOptions[i-1] = i.ToString("00");
            }

        }

        private void OnSelectionChange()
        {
            selectedIndex = StoryEditor.Instance.SelectedIndex;
        }
        
        private void OnIndexChange()
        {
            StoryEditor.Instance.SelectPageAt(selectedIndex); 
            GUI.FocusControl("Story_Pages");
        }

    }
}