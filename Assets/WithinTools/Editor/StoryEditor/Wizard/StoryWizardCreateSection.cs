﻿using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Within.SuperBloom
{
    public class StoryWizardCreateSection : WithinEditorWindowSection<StoryWizardEditorWindow>
    {
        private string textWithPhrases;
        private string[] phrases;
        private string babelPartnerID;

        public StoryWizardCreateSection(StoryWizardEditorWindow _parentWindow) : base(_parentWindow) { }

        private bool storyCreated = false;
        
        protected override void DoDraw()
        {
            if (storyCreated)
            {
                return;
            }

            using (var verticalLayout = new GUI_VERTICAL_LAYOUT())
            {
                EditorGUILayout.LabelField("Story Details", EditorStyles.boldLabel);

                using (new GUI_VERTICAL_LAYOUT("Box"))
                {
                    StoryWizardEditor.Instance.MockStoryData.Title = EditorGUILayout.TextField("Title: ", StoryWizardEditor.Instance.MockStoryData.Title);

                    GUILayout.Label("Description:");
                    StoryWizardEditor.Instance.MockStoryData.Description = EditorGUILayout.TextArea(StoryWizardEditor.Instance.MockStoryData.Description, GUILayout.Height(60));
                }
                
                GUILayout.Space(10);

                using (new GUI_VERTICAL_LAYOUT("Box"))
                {
                    GUILayout.Space(8);

                    EditorGUILayout.LabelField("Story Text and Phrases:");

                    EditorGUILayout.HelpBox("Be sure to properly format the text by placing the HASH SYMBOL (#) after each PASSAGE",
                        MessageType.Info);

                    GUILayout.Space(5);

                    EditorStyles.textField.wordWrap = true;

                    textWithPhrases = EditorGUILayout.TextArea(textWithPhrases, GUILayout.MinWidth(200), GUILayout.MaxWidth(440),
                        GUILayout.ExpandWidth(true), GUILayout.MinHeight(100), GUILayout.MaxHeight(600));
                }

                using (new GUI_VERTICAL_LAYOUT("Box"))
                {
                    EditorGUILayout.HelpBox("You should have received a partner identifier from WITHIN. This is required for voice " +
                                            "recognition support in your story. You can also set this later using the StoryEditor " +
                                            "window.", MessageType.Info);
                    GUILayout.Space(5);
                    babelPartnerID = EditorGUILayout.TextField("Babel Partner ID:", babelPartnerID);
                    GUILayout.Space(5);
                }

                using (var horizontalLayout = new GUI_HORIZONTAL_LAYOUT())
                {
                    GUILayout.FlexibleSpace();

                    bool disableCreateStory = string.IsNullOrEmpty(StoryWizardEditor.Instance.MockStoryData.Title) ||
                                                    string.IsNullOrEmpty(textWithPhrases);

                    using (new GuiDisabledGroup(disableCreateStory))
                    {
                        if (GUILayout.Button("Create Story", GUILayout.Width(150)))
                        {
                            storyCreated = true;
                        }
                    }
                }
            }

            if (storyCreated)
            {
                CreateStoryStructure();                
            }
        }

        private void CreateStoryStructure()
        {
            Debug.Log("Wizard: Starting Creation");
            
            CreateStoryAsset();
            
            Debug.Log("Wizard: StoryAsset Created");
            
            StoryWizardTools.CreateProjectDataFolders();
            
            Debug.Log("Wizard: Data Folders Created");

            StoryWizardTools.CreateStorySceneAndOpen();
            
            Debug.Log("Wizard: Story Scene Created and Opened");

            StoryWizardTools.CreateStoryObjectInScene();
            
            Debug.Log("Wizard: StoryObjects in Scene Created");
            
            if (!ParseStoryTextCheck())
            {
                Debug.LogError("Wizard: ERROR. Story could not be parsed");
                return;
            }
            
            Debug.Log("Wizard: Story Parsed");

            SBEditorEvents.storyWizardWindowRefresh.Invoke();

            for (int i = 0; i < EditorSceneManager.loadedSceneCount; i++)
            {
                EditorSceneManager.SaveScene(EditorSceneManager.GetSceneAt(i));
            }

            AssetDatabase.SaveAssets();
            
            // Ensure the story scene is added to the scene list for local testing
            OnLoadSteps.AddRequiredScenesToBuildSettings();
            
            EditorSceneManager.SaveOpenScenes();
            StoryEditor.Instance.Initialize();
            if (!string.IsNullOrEmpty(babelPartnerID))
            {
                StoryEditor.Instance.Babel.Setup(babelPartnerID);
            }
            SBEditorEvents.storyEditorWindowRefresh.Invoke();
        }

        private bool ParseStoryTextCheck()
        {
            try
            {
                phrases = textWithPhrases.Split(new string[] { "#" }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < phrases.Length; i++)
                {
                    phrases[i] = phrases[i].Trim();
                }
                
                phrases = phrases.Where(p => p.Length > 0).ToArray();
            
                StoryEditor.Instance.Initialize();
            
                StoryEditor.Instance.CreateBulkPagesForStory(phrases);
            }
            catch (Exception e)
            {
                Debug.LogError("[Exception when Creating Pages] = " + e.Message);
                return false;
            }
            
            return true;
        }

        private void CreateStoryAsset()
        {
            if (SBAssets.GetStoryData() != null)
            {
                Debug.LogError("[StoryWizard] Can't create Story since it already exists");
                return;
            }

            StoryData asset = ScriptableObject.CreateInstance<StoryData>();

            string path = SBToolsPaths.STORY_DATA_ASSET_PATH + ".asset";
            asset.CopyFrom(StoryWizardEditor.Instance.MockStoryData);
            
            string directoryPath = SBToolsPaths.ConvertToAbsolute(SBToolsPaths.URL_STORY_DATA_FOLDER);
            
            if (!Directory.Exists(directoryPath))
            {    
                Directory.CreateDirectory(directoryPath);
            }
            
            AssetDatabase.CreateAsset(asset, path);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            
            string toolVersion = WithinEditorTools.GetToolVersion();
            File.WriteAllText(SBToolsPaths.STORY_TOOL_VERSION_FILE_PATH, toolVersion);
        }
    }
}