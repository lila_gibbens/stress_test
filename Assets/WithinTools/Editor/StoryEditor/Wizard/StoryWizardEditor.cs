﻿using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
    public class StoryWizardEditor
    {
        private static StoryWizardEditor instance;

        public static StoryWizardEditor Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new StoryWizardEditor();
                    instance.RefreshData();
                }
                return instance;
            }
        }

        public StoryWizardEditorWindow Window;

        /// <summary>
        /// The editor will use this object to save the properties and will copy from this to the real asset when
        /// the user presses "Save"
        /// </summary>
        private StoryData mockStoryData;
        public StoryData MockStoryData
        {
            get { return mockStoryData; }
        }

        [MenuItem("AR Books/Story Wizard Creation")]
        public static void ShowWindow()
        {
            Instance.RefreshData();
            
            Instance.Window = EditorWindow.GetWindow(typeof(StoryWizardEditorWindow)) as StoryWizardEditorWindow;
            Instance.Window.Initialize();
            Instance.Window.Show();
        }
        
        public StoryWizardEditor()
        {
            instance = this;

            SBEditorEvents.storyWizardWindowRefresh.AddListener(RefreshData);
        }

        public void RefreshData()
        {
            var storyData = SBAssets.GetStoryData();
            if (storyData)
            {
                mockStoryData = storyData.Clone();    
            }
            else
            {
                mockStoryData = ScriptableObject.CreateInstance<StoryData>();
            }
        }
    }
}