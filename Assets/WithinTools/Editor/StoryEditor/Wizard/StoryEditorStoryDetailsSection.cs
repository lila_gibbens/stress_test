﻿using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
    public class StoryEditorStoryDetailsSection : WithinEditorWindowSection<StoryEditorWindow>
    {
        private bool editDetailsfoldout;
        private string babelPartnerID;

        public StoryEditorStoryDetailsSection(StoryEditorWindow _parentWindow) : base(_parentWindow) 
        {
            babelPartnerID = StoryEditor.Instance.Babel.PartnerKey;
        }
        
        protected override void DoDraw()
        {
            EditorGUILayout.BeginVertical("Box", GUILayout.MinWidth(200), GUILayout.MaxWidth(440), GUILayout.ExpandWidth(true));

            editDetailsfoldout = EditorGUILayout.Foldout(editDetailsfoldout, SBAssets.GetStoryData().Title + " Story Details");
            if (editDetailsfoldout)
            {
                StoryEditor.Instance.MockStoryData.Title = EditorGUILayout.TextField("Title: ", StoryEditor.Instance.MockStoryData.Title, GUILayout.MinWidth(200), GUILayout.MaxWidth(410));
                
                GUILayout.Label("Description:");
                StoryEditor.Instance.MockStoryData.Description = EditorGUILayout.TextArea(StoryEditor.Instance.MockStoryData.Description, GUILayout.MaxHeight(80), GUILayout.MinWidth(200), GUILayout.MaxWidth(410));

                EditorGUILayout.HelpBox("You should have received a partner identifier from WITHIN. This is required for voice " +
                                            "recognition support in your story.", MessageType.Info);
                GUILayout.Space(5);
                babelPartnerID = EditorGUILayout.TextField("Babel Partner ID:", babelPartnerID);
                GUILayout.Space(5);

                using (new GUI_HORIZONTAL_LAYOUT())
                {
                    if (GUILayout.Button("Save", GUILayout.Width(100)))
                    {
                        StoryEditor.Instance.SaveStoryData();
                        StoryEditor.Instance.Babel.Setup(babelPartnerID);
                    }    
                }
            }
            EditorGUILayout.EndVertical();
        }
    }
}