﻿using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
    [InitializeOnLoad]
    public class StoryWizardEditorWindow : EditorWindow
    {
        private static Vector2 windowSize = new Vector2(500, 700);
        
        private static StoryWizardEditorWindow windowInstance;

        private StoryWizardCreateSection sectionCreate;
        public StoryWizardCreateSection SectionCreate { get { return sectionCreate; } }

        private StoryWizardFinishedSection finishedSection;
        public StoryWizardFinishedSection FinishedSection { get { return finishedSection; } }

        private void OnEnable()
        {
            Initialize();
        }

        public void Initialize()
        {            
            this.minSize = windowSize;
            this.maxSize = windowSize;
            
            this.titleContent = new GUIContent("Story Wizard");
            
            sectionCreate = new StoryWizardCreateSection(this);
            finishedSection   = new StoryWizardFinishedSection(this);
        }
        
        private void OnGUI()
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode)
            {
                GUILayout.Label("Quit Play Mode to Use this Window");
                return;
            }

            if (SBAssets.GetStoryData() == null)
            {
                sectionCreate.Draw();
            }
            else
            {
                finishedSection.Draw();
            }
        }
    }
}