﻿using UnityEditor;

namespace Within.SuperBloom
{
    public class StoryWizardFinishedSection : WithinEditorWindowSection<StoryWizardEditorWindow>
    {        
        public StoryWizardFinishedSection(StoryWizardEditorWindow _parentWindow) : base(_parentWindow)
        {
        }
        
        protected override void DoDraw()
        {
            EditorGUILayout.LabelField("Story Created:");
            EditorGUILayout.LabelField(" Story Creation Complete. You May Now Close The Story Wizard Window.");
            EditorGUILayout.HelpBox("You can make changes to the Story from the Story Editor (AR Books->StoryEditor)", 
                MessageType.Info);
        }
    }
}