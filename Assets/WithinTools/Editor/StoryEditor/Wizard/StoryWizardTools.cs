﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

namespace Within.SuperBloom
{
    public class StoryWizardTools
    {
        public static void CreateStoryObjectInScene()
        {
            if (GameObject.FindObjectOfType<StoryHierarchy>() != null)
            {
                EditorUtility.DisplayDialog(
                    "Story not created",
                    "Another hierarchy was found in the scene. Please remove it if you want to create the story from scratch",
                    "Ok");

                throw new Exception("[StoryWizardTools] Story Hierarchy already exists in Scene");
            }

            GameObject storyRootGo = WithinEditorTools.CreateGameObjectInHierarchy("Story");
            storyRootGo.AddComponent<StoryHierarchy>();

            GameObject sequencerGo = WithinEditorTools.CreateGameObjectInHierarchy("StorySequencer", storyRootGo);
            GameObject pagesContainerGo = WithinEditorTools.CreateGameObjectInHierarchy("Pages", sequencerGo);

            GameObject storyContentGo = WithinEditorTools.CreateGameObjectInHierarchy("StoryObjects", storyRootGo);
            storyContentGo.AddComponent<StoryContentContainer>();
            storyContentGo.hideFlags = HideFlags.NotEditable;

            // ARInterface requires the camera have a root. This object is updated to align the AR and Real worlds
            GameObject cameraRootGo = WithinEditorTools.CreateGameObjectInHierarchy("ARCameraRoot");
            cameraRootGo.hideFlags = HideFlags.NotEditable;

            // AR will always use the main camera
            GameObject initialCameraGo = WithinEditorTools.CreateGameObjectInHierarchy("ARCamera", cameraRootGo);
            initialCameraGo.AddComponent<Camera>();
            initialCameraGo.AddComponent<AudioListener>();
            initialCameraGo.tag = "MainCamera";

            GameObject storyAreaGo = WithinEditorTools.CreateGameObjectInHierarchy("StoryArea", storyContentGo);
            storyAreaGo.AddComponent<AnchorSelectorPlane>();
            storyAreaGo.AddComponent<StoryAreaPlane>();

            WithinEditorTools.CreateGameObjectInHierarchy("PLACE ALL STORY CONTENT HERE", storyAreaGo);

            sequencerGo.AddComponent<StorySequencer>();
            sequencerGo.AddComponent<EventTrigger>();

            sequencerGo.hideFlags = HideFlags.NotEditable;

            pagesContainerGo.hideFlags = HideFlags.NotEditable;
        }
        
        public static void CreateProjectDataFolders()
        {
            //-- the folder for the StoryData and subfolders for page assets
            List<string> pathsToCreate = new List<string>()
            {
                SBToolsPaths.ConvertToAbsolute(SBToolsPaths.URL_STORY_DATA_FOLDER),
                SBToolsPaths.ConvertToAbsolute(SBToolsPaths.STORY_PAGES_DATA_PATH),
                SBToolsPaths.ConvertToAbsolute(SBToolsPaths.STORY_PAGES_DATA_DELETED_PATH)
            };

            foreach (string path in pathsToCreate)
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public static void CreateStorySceneAndOpen()
        {
            Scene storyScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);

            bool sceneCreated = EditorSceneManager.SaveScene(storyScene, SBToolsPaths.STORY_SCENE_ASSET_PATH, false);

            if (!sceneCreated)
            {
                throw new Exception("[StoryWizardTools] Story Scene coudn't be created");
            }

            EditorSceneManager.OpenScene(storyScene.path);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        
        private static bool VerifyTransformAndNotify(Transform transformToVerify, string gameObjectName, int fixedNumOfChildren = -1)
        {
            if (transformToVerify == null)
            {
                EditorUtility.DisplayDialog(
                    "Scene Is Corrupeted",
                    string.Format("ERROR. Gameobject <{0}> has not been found ", gameObjectName),
                    "Ok");

                return false;
            }

            return VerifyGameObjectAndNotify(transformToVerify.gameObject, gameObjectName, fixedNumOfChildren);
        }

        private static bool VerifyGameObjectAndNotify(GameObject objectToVerify, string gameObjectName, int fixedNumOfChildren = -1)
        {
            if (objectToVerify == null)
            {
                EditorUtility.DisplayDialog(
                    "Scene Is Corrupeted",
                    string.Format("ERROR. Gameobject <{0}> has not been found ", gameObjectName),
                    "Ok");

                return false;
            }

            if (fixedNumOfChildren >= 0)
            {
                if (objectToVerify.transform.childCount != fixedNumOfChildren)
                {
                    EditorUtility.DisplayDialog(
                        "Scene Is Corrupeted",
                        string.Format("ERROR. Gameobject <{0}> should have {1} children but it doesn't", gameObjectName, fixedNumOfChildren),
                        "Ok");

                    return false;
                }
            }
            
            return true;
        }

        private static bool CheckRequiredComponentsAndNotify(GameObject objectToCheck, List<Type> componentTypesToCheck)
        {
            foreach (Type componentType in componentTypesToCheck)
            {
                if (objectToCheck.GetComponents(componentType).Length != 1)
                {
                    EditorUtility.DisplayDialog(
                        "Scene Is Corrupeted",
                        string.Format(
                            "Page object: {0} doesn't have a <{1}> Script",
                            objectToCheck.name,
                            componentType.Name),
                        "Ok");
                    return false;
                }
            }

            return true;
        }

        private static bool CheckRequiredComponentsInChildrenAndNotify(GameObject objectToCheck, List<Type> componentTypesToCheck)
        {
            for (int i = 0; i < objectToCheck.transform.childCount; i++)
            {
                GameObject childObj = objectToCheck.transform.GetChild(i).gameObject;

                bool isOk = CheckRequiredComponentsAndNotify(childObj, componentTypesToCheck);

                if (!isOk)
                {
                    return false;
                }
            }

            return true;
        }

        public static void VerifyScene()
        {
            //--- Game object "Story" must exist            
            GameObject storyRootGo = GameObject.Find("Story");
            if (!VerifyGameObjectAndNotify(storyRootGo, "Story", 2))
            {
                return;
            }

            //--- Game object "StorySequencer" must exist inside Story/StorySequencer
            Transform storySequencerT = storyRootGo.transform.Find("StorySequencer");
            if (!VerifyTransformAndNotify(storySequencerT, "StorySequencer"))
            {
                return;
            }

            //--- Game object "StoryObjects" must exist inside Story/StoryObjects
            Transform storyObjectsT = storyRootGo.transform.Find("StoryObjects");
            if (!VerifyTransformAndNotify(storyObjectsT, "StoryObjects"))
            {
                return;
            }
            
            //--- Game object "Pages" must exist inside Story/StorySequencer/Pages
            Transform storyPagesT = storySequencerT.gameObject.transform.Find("Pages");
            if (!VerifyTransformAndNotify(storyObjectsT, "Pages"))
            {
                return;
            }

            //--- every children of gameObject "Pages" need to include these components:
            if (!CheckRequiredComponentsInChildrenAndNotify(
                storyPagesT.gameObject,
                new List<Type>()
                {
                    typeof(StoryPage),
                    typeof(StoryPageDataBinder),
                    typeof(PlayableDirector)
                }))
            {
                return;
            }

            EditorUtility.DisplayDialog("Verification Complete", "The scene structure looks fine.", "Ok");
        }
    }
}