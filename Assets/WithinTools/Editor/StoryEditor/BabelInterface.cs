using System;
using System.Linq;
using UnityEditor;

namespace Within.SuperBloom
{
    public class BabelInterface
    {
        public static readonly string ModelDirectory = "Assets/WithinTools/External/ROGER/ROGER_files/Models";

        public string PartnerKey
        {
            get { return babelService.PartnerKey; }
        }

        private ROGERBabelService babelService;

        public BabelInterface()
        {
            babelService = new ROGERBabelService(ModelDirectory);
        }

        public void Setup(string partnerId)
        {
            if (partnerId != babelService.PartnerKey)
            {
                try
                {
                    EditorUtility.DisplayProgressBar("Babel Setup", string.Empty, 0.5f);
                    babelService.Setup(partnerId);
                }
                finally
                {
                    EditorUtility.ClearProgressBar();
                }
            }
        }

        public bool GenerateModel()
        {
            // TODO: Kris - Check that story data is reasonable. Do we have a title? Do we have enough text?
            StoryData story = StoryEditor.Instance.MockStoryData;
            string modelName = StoryData.GetTitleFilename(story.Title);

            // Combine text from all passages into one model
            string storyText = story.dataPages.Select(p => p.Passage.Text).Aggregate((current, next) => current + " " + next);

            // Remove all punctuation as Babel doesn't like it
            storyText = new string(storyText.Where(c => !(char.IsPunctuation(c)) || c == '\'').ToArray());
            storyText = storyText.Replace("\r\n", " ");
            storyText = storyText.Replace("\n", " ");

            bool error = false;
            try
            {
                EditorUtility.DisplayProgressBar("Generating Voice Recognition Model...", string.Empty, 0.5f);
                error = !babelService.GenerateModel(modelName, storyText);
            }
            catch (Exception)
            {
                error = true;
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
            if (error)
            {
                bool result = EditorUtility.DisplayDialog("Error", "Unable to generate a voice recognition model. Please check the Unity Console for errors. Voice recognition will not be available for this story, do you want to continue?", "Yes", "No");
                if (!result)
                {
                    return false;
                }
            }
            return true;
        }
    }
}