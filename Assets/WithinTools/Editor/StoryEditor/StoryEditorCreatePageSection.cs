﻿using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
    public class StoryEditorCreatePageSection : WithinEditorWindowSection<StoryEditorWindow>
    {
        public StoryEditorCreatePageSection(StoryEditorWindow _parentWindow) : base(_parentWindow) { }

        private bool addPagesFoldout;
        private int newPageNumber = 0;

        protected override void DoDraw()
        {
            bool generatePageClicked = false;
            
            GUILayout.BeginVertical("Box", GUILayout.MinWidth(200), GUILayout.MaxWidth(440), GUILayout.ExpandWidth(true));

            addPagesFoldout = EditorGUILayout.Foldout(addPagesFoldout, "Add Page");

            if (addPagesFoldout)
            {
                newPageNumber = EditorGUILayout.IntField("New Page Number:", newPageNumber, GUILayout.MinHeight(20f), GUILayout.MaxWidth(240));

                ParentWindow.NewPageText = EditorGUILayout.TextField("New Passage Text:", ParentWindow.NewPageText, GUILayout.MinHeight(40f));

                generatePageClicked = GUILayout.Button("Generate Page");
            }

            GUILayout.EndVertical();

            if (generatePageClicked)
            {
                GeneratePage();
            }
        }

        private void GeneratePage()
        {
            if (Application.isPlaying)
            {
                EditorUtility.DisplayDialog("Unable To Create Pages in Play Mode",
                    "Please Exit Play Mode Before Attempting to Create New Pages", "OK");
            }
            else
            {
                int index = newPageNumber - 1;
                if (newPageNumber > (StoryEditor.Instance.PageCount + 1))
                {
                    index = StoryEditor.Instance.PageCount;
                    Debug.LogWarning("Page Out Of Range. New Page Will Be Created At End Of Story");
                }
                else if (index < 0)
                {
                    index = 0;
                    Debug.LogWarning("Page Out Of Range. New Page Will Be Created At Beginning Of Story");
                }
                StoryEditor.Instance.InsertPageAtIndex(index, ParentWindow.NewPageText);
                StoryEditor.Instance.SelectPageAt(index);
                ParentWindow.NewPageText = "enter story text here";
                SBEditorEvents.storyEditorWindowRefresh.Invoke();
            }
        }
    }
}