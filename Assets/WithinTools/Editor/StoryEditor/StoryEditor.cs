﻿﻿﻿using System.Collections.Generic;
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    public class StoryEditor
    {
        private static StoryEditor instance;

        public static StoryEditor Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new StoryEditor();
                }

                return instance;
            }
        }

        private List<StoryPage> ListOfPageInstances;

        /// <summary>
        /// The editor will use this object to save the properties and will copy from this to the real asset when
        /// the user presses "Save"
        /// </summary>
        private StoryData mockStoryData;
        public StoryData MockStoryData
        {
            get { return mockStoryData; }
        }

        private StoryData storyData;

        private BabelInterface babel;
        public BabelInterface Babel
        {
            get { return babel; }
        }

        public int SelectedIndex;

        private bool isInited;
        public bool IsInited
        {
            get { return isInited; }
        }

        public bool IsDataReady
        {
            get { return isInited && SelectedPageData != null && SelectedPageInstance != null; }
        }

        public void Initialize()
        {
            ListOfPageInstances = new List<StoryPage>();
           
            mockStoryData = StoryData.CreateInstance<StoryData>();
            
            storyData = SBAssets.GetStoryData();

            if (storyData != null)
            {
                mockStoryData.CopyFrom(storyData);
            }

            SBEditorEvents.storyEditorWindowRefresh.AddListener(refresh);

            babel = new BabelInterface();

            isInited = true;
        }
        
        /// <summary>
        ///  Called when a scene different than the Story Scene is Opened
        /// </summary>
        public void Close()
        {
            isInited = false;
        }

        private void refresh()
        {
            if (StoryModeOptions.IsRunningFromBundle)
            {
                return;
            }

            storyData = SBAssets.GetStoryData();
            if (storyData == null)
            {
                Debug.LogWarning("Story not created yet");
                return;
            }
            
            // -- if the Story exists but the user is in a different scene than the StoryScene, returns
            if (! SBEditorScenesHandler.IsStoryOpened() || SBSceneLocator.StorySequencerScript == null )
            {
                return;
            }

            SBSceneLocator.StorySequencerScript.StoryData = storyData;
            
            mockStoryData.CopyFrom( storyData );
            
            ListOfPageInstances.Clear();
            
            List<StoryPage> scenePages = SBSceneLocator.PagesContainer.transform.GetComponentsInChildren<StoryPage>().ToList();

            foreach (StoryPageData pageData in mockStoryData.DataPages)
            {
                StoryPage pageInstance = scenePages.Find(pi => pi.name == pageData.name);
                
                pageInstance.SetDataFromBinder();
                pageInstance.gameObject.hideFlags = pageInstance.gameObject.hideFlags & HideFlags.NotEditable;
                
                ListOfPageInstances.Add(pageInstance);
            }

            SelectPageAt(SelectedIndex);
        }

        public void SaveStoryData()
        {
            EditorUtility.SetDirty(storyData);
            storyData.CopyFrom(MockStoryData);
        }

        public int PageCount
        {
            get { return mockStoryData.DataPages.Count; }
        }

        public void SelectPageAt(int index = 0)
        {
            SelectedIndex = index;
            
            Selection.activeGameObject =  ListOfPageInstances[index].gameObject;
            EditorUtility.SetDirty(mockStoryData.DataPages[index]);
            
            SBEditorEvents.onPageSelectionChange.Invoke();
        }

        public StoryPageData SelectedPageData
        {
            get { return mockStoryData.DataPages.Count > SelectedIndex ? mockStoryData.DataPages[SelectedIndex] : null; }
        }
        
        public StoryPage SelectedPageInstance
        {
            get { return mockStoryData.DataPages.Count > SelectedIndex ? ListOfPageInstances[SelectedIndex] : null;  }
        }
        
        public void MoveSelectedPageUp()
        {
            if(SelectedIndex == PageCount + 1)
                return;

            SwapSelectedPageWith(SelectedIndex + 1);
        }

        public void MoveSelectedPageDown()
        {
            if(SelectedIndex == 0)
                return;
            
            SwapSelectedPageWith(SelectedIndex - 1);
        }

        private void SwapSelectedPageWith(int targetIndex)
        {
            StoryPageData currentPageData = mockStoryData.DataPages[SelectedIndex];
            StoryPageData targetPageData  = mockStoryData.DataPages[targetIndex]; 
            
            mockStoryData.DataPages[SelectedIndex] = targetPageData;
            mockStoryData.DataPages[targetIndex] = currentPageData;
            
            SaveStoryData();
            refresh();
            
            SelectPageAt(targetIndex);
        }
        
        public void DeleteSelectedPage()
        {
            DeletePageAt(SelectedIndex);
        }

        private void DeletePageAt(int index)
        {
            GameObject.DestroyImmediate( ListOfPageInstances[index].gameObject);
            
            StoryPageData pageData = mockStoryData.DataPages[index];

            mockStoryData.DataPages.RemoveAt(index);
            SaveStoryData();
            ListOfPageInstances.RemoveAt(index);
            
            AssetDatabase.DeleteAsset(pageData.GetTimelineAssetPath());
            AssetDatabase.DeleteAsset(pageData.GetAssetPath());
            
            if (SelectedIndex > 0)
            {
                SelectedIndex --;
            }
            
            SBEditorScenesHandler.SaveStoryScene();
            
            refresh();
        }


        private StoryPageData createPageDataAsset(int _index, string _passageText, out TimelineAsset timeline)
        {
            StoryPageData pageData = ScriptableObject.CreateInstance<StoryPageData>();
            timeline = ScriptableObject.CreateInstance<TimelineAsset>();
            timeline.durationMode = TimelineAsset.DurationMode.FixedLength;
            timeline.fixedDuration = 500000;
            
            pageData.Passage = new StoryPassage(_passageText);
            pageData.Sequence = timeline;
            pageData.PageName = "StoryPage" + DateTime.Now.ToString("yyMMddHHmmss") + _index;
            
            AssetDatabase.CreateAsset(timeline, pageData.GetTimelineAssetPath() );
            AssetDatabase.CreateAsset(pageData, pageData.GetAssetPath() );

            TrackAsset pageManagementTrack = timeline.CreateTrack<PageManagementPlayableTrack>(null, "Page Management");

            TimelineClip startClip = pageManagementTrack.CreateClip<StartPagePlayableClip>();
            startClip.duration = 1;
            startClip.start = 0;
            
            TimelineClip endClip = pageManagementTrack.CreateClip<EndOfPagePlayableClip>();
            endClip.duration = 1;
            endClip.start = 3;

            return pageData;
        }

        private StoryPage createPageInstance(StoryPageData pageData, TimelineAsset timeline)
        {
            GameObject instanceGameObject = WithinEditorTools.CreateGameObjectInHierarchy(pageData.PageName, SBSceneLocator.PagesContainer);

            StoryPage page = instanceGameObject.AddComponent<StoryPage>();
            instanceGameObject.GetComponent<StoryPageDataBinder>().SetData(pageData);
            instanceGameObject.GetComponent<StoryPage>().SetDataFromBinder();

            PlayableDirector director = instanceGameObject.AddComponent<PlayableDirector>();
            director.playableAsset = timeline;
            director.playOnAwake = false;
            director.extrapolationMode = DirectorWrapMode.Hold;

            page.TimelineDirector = director;

            //instanceGameObject.hideFlags = HideFlags.HideInHierarchy;
            return page;
        }

        private void CreatePageAt(int _index, string passage)
        {
            TimelineAsset timeline;

            StoryPageData pageData = createPageDataAsset(_index, passage, out timeline);
            StoryPage pageInstance = createPageInstance(pageData, timeline);
            
            mockStoryData.DataPages.Insert(_index, pageData);
            SaveStoryData();
            
            ListOfPageInstances.Insert(_index, pageInstance);
            AssetDatabase.SaveAssets();
            
            SBEditorScenesHandler.SaveStoryScene();
        }
        
        public void InsertPageAtIndex(int _index, string passage)
        {
            _index = Math.Min(_index, PageCount);
            
            CreatePageAt(_index, passage);
            
            refresh();
        }

        public void CreateBulkPagesForStory(string[] passages)
        {
            for (int i = 0; i < passages.Length; i++)
            {
                CreatePageAt(i, passages[i]);    
            }
            
            AssetDatabase.SaveAssets();
            SaveStoryData();
            SBEditorScenesHandler.SaveStoryScene();
        }
        
        public void ForceDeleteEntireStory()
        {
            StoryPage[] pageInstances = GameObject.FindObjectsOfType<StoryPage>();
            for (int i = 0; i < pageInstances.Length; i++)
            {
                GameObject.DestroyImmediate(pageInstances[i].gameObject);
            }
            Array.ForEach(System.IO.Directory.GetFiles(@SBToolsPaths.STORY_PAGES_DATA_PATH, "*.asset"),
                    delegate(string path) { System.IO.File.Delete(path); });
            AssetDatabase.DeleteAsset(SBToolsPaths.STORY_DATA_ASSET_PATH + ".asset");
            AssetDatabase.Refresh();
        }

        public void SetNarrationIsAvailable()
        {
            bool narrationAvailable = true;
            foreach (var pageData in mockStoryData.dataPages)
            {
                foreach (var clip in pageData.PreRecordedClips)
                {
                    narrationAvailable = narrationAvailable && (clip != null);
                }
            }

            mockStoryData.IsPrerecordedNarrationOn = narrationAvailable;
            SaveStoryData();
        }

      #if SB_DEBUG_ENABLED
        [MenuItem("AR Books/Debug/Upgrade 0.5")]
      #endif
        public static void MigrationFix_Version0dot5()
        {
            string searchPath = SBToolsPaths.STORY_PAGES_DATA_PATH.Substring(0, SBToolsPaths.STORY_PAGES_DATA_PATH.Length - 1);
            string[] pageAssetGuids = AssetDatabase.FindAssets("t: StoryPageData", new[] {searchPath});
            
            StoryData storyData = SBAssets.GetStoryData();

            EditorUtility.SetDirty(storyData);

            List<StoryPageData> listPages = new List<StoryPageData>();
            
            foreach (var guid in pageAssetGuids)
            {
                string pathToPageData = AssetDatabase.GUIDToAssetPath(guid);
                StoryPageData pageData = AssetDatabase.LoadAssetAtPath<StoryPageData>(pathToPageData);
                listPages.Add(pageData);
            }
            
            listPages.Sort( (a,b) => a.Index.CompareTo(b.Index));
            
            storyData.SetPageDataList(listPages);
        }
    }
} 