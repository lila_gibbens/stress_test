﻿using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
    public class StoryEditorWindow : EditorWindow
    {
        private static StoryEditorWindow windowInstance;
        
        public string NewPageText = "enter story text here";
        
        private StoryEditorCreatePageSection sectionCreatePage;
        public StoryEditorCreatePageSection SectionCreatePage { get { return sectionCreatePage; } }

        private StoryEditorSelectSection sectionSelect;
        public StoryEditorSelectSection SectionSelect { get { return sectionSelect; } }
        
        private StoryEditorEditSection sectionEdit;
        public StoryEditorEditSection SectionEdit { get { return sectionEdit; } }
        
        private StoryEditorStoryDetailsSection storyEdit;
        public StoryEditorStoryDetailsSection StoryEdit { get { return storyEdit; } }

        private Vector2 scrollPosition = Vector2.zero;
        
        [MenuItem("AR Books/StoryEditor")]
        public static void ShowWindow()
        {
            windowInstance = EditorWindow.GetWindow(typeof(StoryEditorWindow)) as StoryEditorWindow;
            windowInstance.titleContent = new GUIContent("Story Editor");
            windowInstance.Show();
        }

        internal void OnFocus()
        {
            SBEditorEvents.storyEditorWindowRefresh.Invoke();
        }
        
        internal void OnEnable()
        {
            Initialize();
        }

        public void Initialize()
        {
            this.titleContent = new GUIContent("Story Editor");

            sectionCreatePage = new StoryEditorCreatePageSection(this);
            sectionSelect = new StoryEditorSelectSection(this);
            sectionEdit   = new StoryEditorEditSection(this);
            storyEdit   = new StoryEditorStoryDetailsSection(this);
            
            StoryEditor.Instance.Initialize();
            
            SBEditorEvents.storyEditorWindowRefresh.Invoke();
        }

        private void OnGUI()
        {
            EditorStyles.textField.wordWrap = true;
            EditorGUIUtility.labelWidth = 140f;

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, false, false, GUIStyle.none, GUI.skin.verticalScrollbar, GUI.skin.scrollView);
            if (StoryModeOptions.IsRunningFromBundle)
            {
                EditorGUILayout.LabelField("PLAYING FROM A BUNDLE. EDITOR NOT AVAILABLE.");
                return;
            }
            
            if (SBAssets.GetStoryData() == null)
            {
                EditorGUILayout.HelpBox("Story Does Not Exist: " +
                                        "\nUse The Story Wizard to Create a New Story.", 
                    MessageType.Error);
            }
            else if (SBSceneLocator.PagesContainer == null)
            {
                EditorGUILayout.HelpBox("Incorrect opened scenes" +
                                        "\nPlease open the Story Scene to edit the story", 
                    MessageType.Warning);
                
                if (GUILayout.Button("Open Story Scene", GUILayout.Width(120)))
                {
                    SBEditorScenesHandler.OpenStoryScenes();
                }
            }
            else if (!StoryEditor.Instance.IsInited)
            {
                EditorGUILayout.LabelField("LOADING...");
            }
            else
            {
                sectionSelect.Draw();
                
                bool isInPlayMode = EditorApplication.isPlaying || EditorApplication.isPaused;
                EditorGUI.BeginDisabledGroup(isInPlayMode);
                
                sectionEdit.Draw();
                sectionCreatePage.Draw();
                storyEdit.Draw();
                
                EditorGUI.EndDisabledGroup();
            }
            
            EditorGUILayout.EndScrollView();
        }
    }
}