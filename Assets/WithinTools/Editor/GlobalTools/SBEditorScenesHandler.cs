﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom
{
    [InitializeOnLoad]
    public class SBEditorScenesHandler
    {
        static SBEditorScenesHandler()
        {
            EditorSceneManager.sceneLoaded += OnSceneLoaded;
            EditorSceneManager.sceneOpened += OnSceneOpened;

            EditorApplication.playModeStateChanged += CheckOnEnterPlay;
        }

        private static void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            CheckOpenedStoryScenes();
        }

        private static void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            CheckOpenedStoryScenes();
        }

        private static void OnSceneClosed(Scene scene)
        {
            CheckOpenedStoryScenes();
        }
        
        private static void CheckOpenedStoryScenes()
        {
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                CheckOnEditorTime();
            }
        }

        public static bool IsStoryOpened()
        {
            List<string> loadedScenes = GetLoadedSceneNames();

            return loadedScenes.Contains(SBToolsPaths.STORY_SCENE_NAME);
        }
        
        private static void CheckOnEnterPlay(PlayModeStateChange state)
        {
            RunAfterOpeningStoryScene();
            
            if (state == PlayModeStateChange.EnteredEditMode)
            {
                StoryModeOptions.Reset();
                SBEditorEvents.storyEditorWindowRefresh.Invoke();
            }

            if (state != PlayModeStateChange.EnteredPlayMode)
            {
                return;
            }

            List<string> loadedScenes = new List<string>();

            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                loadedScenes.Add(SceneManager.GetSceneAt(i).name);
            }

            if (loadedScenes.Contains(SBToolsPaths.STORY_SCENE_NAME))
            {
                StoryScenesLoader.Instance.LoadStoryFromBuildSettings();
            }
        }

        private static List<string> GetLoadedSceneNames()
        {
            List<string> loadedScenes = new List<string>();

            for (int i = 0; i < EditorSceneManager.loadedSceneCount; i++)
            {
                loadedScenes.Add(EditorSceneManager.GetSceneAt(i).name);
            }

            return loadedScenes;
        }

        private static void CheckOnEditorTime()
        {
            StoryEditor.Instance.Close();

            List<string> loadedScenes = GetLoadedSceneNames();

            //--- If the Story Scene is opened but not the Debug UI and AR Scenes, open them!
            if (IsStoryOpened())
            {
                if (!loadedScenes.Contains(SBToolsPaths.SEQUENCER_FINAL_UI_SCENE_NAME))
                {
                    EditorSceneManager.OpenScene(SBToolsPaths.SEQUENCER_FINAL_UI_SCENE_PATH, OpenSceneMode.Additive);
                }
                if (!loadedScenes.Contains(SBToolsPaths.AR_SCENE_NAME))
                {
                    EditorSceneManager.OpenScene(SBToolsPaths.AR_SCENE_PATH, OpenSceneMode.Additive);
                }
                
                StoryEditor.Instance.Initialize();
            }

            RunAfterOpeningStoryScene();
        }

        public static void OpenStoryScenes()
        {
            EditorSceneManager.OpenScene(SBToolsPaths.STORY_SCENE_ASSET_PATH, OpenSceneMode.Single);
        }

        public static void SaveStoryScene()
        {
            if (Application.isPlaying)
            {
                return;
            }
            
            Scene storyScene = EditorSceneManager.GetSceneByName(SBToolsPaths.STORY_SCENE_NAME);

            EditorSceneManager.MarkSceneDirty(storyScene);

            EditorSceneManager.SaveScene(storyScene, storyScene.path);
        }
        
        // This checks the status of the Story Scene and do changes if needed
        private static void RunAfterOpeningStoryScene()
        {
            if (IsStoryOpened())
            {
                GameObject deprecatedMusicPlayer = GameObject.Find("BackgroundMusicPlayer");
                if (deprecatedMusicPlayer != null)
                {
                    GameObject.DestroyImmediate(deprecatedMusicPlayer);
                    SaveStoryScene();
                }    
            }
        }
    }
}