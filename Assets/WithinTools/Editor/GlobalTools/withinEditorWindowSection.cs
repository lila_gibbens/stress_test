﻿using UnityEditor;

namespace Within.SuperBloom
{
    public class WithinEditorWindowSection<T> where T : EditorWindow
    {
        public bool IsVisible = true;

        private T parentWindow;
        public T ParentWindow { get { return parentWindow; } }

        public WithinEditorWindowSection(T _parentWindow)
        {
            parentWindow = _parentWindow;
        }

        public virtual void Initialize() { }
        
        public void Draw()
        {
            if (!IsVisible)
            {
                return;
            }
            
            DoDraw();
        }

        protected virtual void DoDraw() { }
    }
}