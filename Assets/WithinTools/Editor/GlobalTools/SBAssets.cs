﻿using UnityEditor;

namespace Within.SuperBloom
{
	public static class SBAssets
	{	
		private static StoryData dataStory;
	
		public static StoryData GetStoryData()
		{
            if (dataStory == null)
            {
                dataStory = AssetDatabase.LoadAssetAtPath<StoryData>(SBToolsPaths.STORY_DATA_ASSET_PATH + ".asset");
            }
	
			return dataStory;
		}
	}
}