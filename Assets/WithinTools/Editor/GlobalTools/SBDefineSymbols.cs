﻿using UnityEditor;

namespace Within.SuperBloom
{
	public static class SBDefineSymbols 
	{		
		private const string SB_DEBUG_ENABLED = "SB_DEBUG_ENABLED";

		private static void DisableSymbol(string defineSymbol)
		{
			BuildTargetGroup selectedGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
			string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(selectedGroup);
			
			if (defines.Contains(defineSymbol))
			{
				defines = defines.Replace(defines, string.Empty);
			}

            PlayerSettings.SetScriptingDefineSymbolsForGroup(selectedGroup, defines);
		}
        
		private static void EnableSymbol(string defineSymbol)
		{
			BuildTargetGroup selectedGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
			string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(selectedGroup);
					
			if (!defines.Contains(defineSymbol))
			{
				defines += ";" + defineSymbol;
			}

            PlayerSettings.SetScriptingDefineSymbolsForGroup(selectedGroup, defines);
		}
        
		public static void ToggleSymbol_DebugEnabled(bool newEnable)
		{
			if (newEnable)
			{
				EnableSymbol(SB_DEBUG_ENABLED);
			}
			else
			{
				DisableSymbol(SB_DEBUG_ENABLED);				
			}
		}
	}
}
