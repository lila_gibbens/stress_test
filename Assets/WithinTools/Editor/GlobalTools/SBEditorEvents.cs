﻿﻿using UnityEditor;
using UnityEngine.Events;

namespace Within.SuperBloom
{
	[InitializeOnLoad]
	public static class SBEditorEvents
	{
		public static readonly UnityEvent storyWizardWindowRefresh = new UnityEvent();		
		public static readonly UnityEvent storyEditorWindowRefresh = new UnityEvent();
		
		public static readonly UnityEvent onPageSelectionChange = new UnityEvent();
	}	
}
