﻿using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
    internal class WithinEditorTools_createGameObject
    {
        public string Name;
        public bool IncrementalIndex = true;
        public bool LabelInheritance = true;
        public GameObject selectedParent = null;
    }

    /// <summary>
    /// This Editor Window is intended for Development Test purposes only.
    /// It provides an interface to test the methods from withinEditorTools class
    /// </summary>
    [InitializeOnLoad]
    public class WithinEditorToolsTestWindow : EditorWindow
    {
        private static WithinEditorToolsTestWindow window;

        // -- Objects for fields for every section
        #region serializable_ongui_objects

        [SerializeField]
        private static WithinEditorTools_createGameObject createGameObjectProperties = new WithinEditorTools_createGameObject();

        #endregion

        public static void ShowWindow()
        {
            window = EditorWindow.GetWindow(typeof(WithinEditorToolsTestWindow)) as WithinEditorToolsTestWindow;

            window.titleContent = new GUIContent("Editor Tools");

            window.Show();
        }

        private void OnGUI()
        {
            EditorGUILayout.BeginVertical("Box");

            EditorGUILayout.LabelField("Create GameObject");

            GameObject selectedParent = Selection.gameObjects.Length == 0 ? null : Selection.gameObjects[0];

            if (selectedParent != null)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("Selected Parent:");
                createGameObjectProperties.selectedParent = EditorGUILayout.ObjectField(createGameObjectProperties.selectedParent, typeof(GameObject), true) as GameObject;

                EditorGUILayout.EndHorizontal();
            }
            else
            {
                EditorGUILayout.LabelField("Selected Parent: NONE");
            }
            
            createGameObjectProperties.Name = EditorGUILayout.TextField("Name:", createGameObjectProperties.Name);
            createGameObjectProperties.IncrementalIndex = EditorGUILayout.Toggle("Incremental: ", createGameObjectProperties.IncrementalIndex);
            createGameObjectProperties.LabelInheritance = EditorGUILayout.Toggle("Label Inheritance: ", createGameObjectProperties.LabelInheritance);

            if (GUILayout.Button("Create", GUILayout.Width(150)))
            {
                WithinEditorTools.CreateGameObjectInHierarchy(
                    createGameObjectProperties.Name,
                    createGameObjectProperties.selectedParent,
                    createGameObjectProperties.IncrementalIndex,
                    createGameObjectProperties.LabelInheritance);
            }

            EditorGUILayout.EndVertical();
        }
    }
}