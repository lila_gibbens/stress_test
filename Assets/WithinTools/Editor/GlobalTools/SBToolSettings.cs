﻿using System;
using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
    public class SBToolSettings : ScriptableObject
    {
        [SerializeField]
        private bool enableDebugMenu;
        public bool EnableDebugMenu
        {
            get
            {
                return enableDebugMenu;
            }

            set
            {
                enableDebugMenu = value;
                OnValidate();
            }
        }

        private void OnValidate()
        {
            SBDefineSymbols.ToggleSymbol_DebugEnabled(enableDebugMenu);
        }

        public static void CreateAsset()
        {
            SBToolSettings settingsAsset = AssetDatabase.LoadAssetAtPath<SBToolSettings>(SBToolsPaths.URL_SETTINGS_ASSET + ".asset");

            if (settingsAsset != null)
            {
                throw new Exception("[SBToolSettings] Settings Asset already exists in project");
            }

            settingsAsset = CreateInstance<SBToolSettings>();

            settingsAsset.name = SBToolsPaths.SETTINGS_ASSET_NAME;

            AssetDatabase.CreateAsset(settingsAsset, SBToolsPaths.URL_SETTINGS_ASSET + ".asset");

            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
        }
    }
}