﻿using UnityEditor;

namespace Within.SuperBloom
{
    public class SBPostProcessor : AssetPostprocessor
    {
        public static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            foreach (string str in deletedAssets)
            {
                if (str == SBToolsPaths.STORY_DATA_ASSET_PATH + ".asset")
                {
                    SBEditorEvents.storyWizardWindowRefresh.Invoke();
                }
            }
        }
    }
}