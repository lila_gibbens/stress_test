﻿using System;
using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
    public class GUI_HORIZONTAL_LAYOUT : IDisposable
    {
        public GUI_HORIZONTAL_LAYOUT(params GUILayoutOption[] options)
        {
            EditorGUILayout.BeginHorizontal(options);
        }

        public GUI_HORIZONTAL_LAYOUT(out Rect rect, params GUILayoutOption[] options)
        {
            rect = EditorGUILayout.BeginHorizontal(options);
        }

        public GUI_HORIZONTAL_LAYOUT(GUIStyle style, params GUILayoutOption[] options)
        {
            EditorGUILayout.BeginHorizontal(style, options);
        }

        public GUI_HORIZONTAL_LAYOUT(out Rect rect, GUIStyle style, params GUILayoutOption[] options)
        {
            rect = EditorGUILayout.BeginHorizontal(style, options);
        }

        public void Dispose()
        {
            EditorGUILayout.EndHorizontal();
        }
    }

    public class GUI_VERTICAL_LAYOUT : IDisposable
    {
        public GUI_VERTICAL_LAYOUT(params GUILayoutOption[] options)
        {
            EditorGUILayout.BeginVertical(options);
        }

        public GUI_VERTICAL_LAYOUT(out Rect rect, params GUILayoutOption[] options)
        {
            rect = EditorGUILayout.BeginVertical(options);
        }

        public GUI_VERTICAL_LAYOUT(GUIStyle style, params GUILayoutOption[] options)
        {
            EditorGUILayout.BeginVertical(style, options);
        }

        public GUI_VERTICAL_LAYOUT(out Rect rect, GUIStyle style, params GUILayoutOption[] options)
        {
            rect = EditorGUILayout.BeginVertical(style, options);
        }

        public void Dispose()
        {
            EditorGUILayout.EndVertical();
        }
    }

    public class GUIScrollLayout
    {
        private Vector2 position = new Vector2();
        private GUILayoutOption[] options = null;
        private GUIStyle style = null;

        public GUIScrollLayout(GUIStyle style, params GUILayoutOption[] options) : this(options)
        {
            this.style = style;
        }

        public GUIScrollLayout(params GUILayoutOption[] options)
        {
            this.options = options;
        }

        public ScrollDisposable Start()
        {
            return new ScrollDisposable(ref position, style, options);
        }

        public void ScrollTo(float y = 0)
        {
            position.y = y;
        }

        public class ScrollDisposable : IDisposable
        {
            public ScrollDisposable(ref Vector2 position, GUIStyle style, params GUILayoutOption[] options)
            {
                if (style == null)
                {
                    position = EditorGUILayout.BeginScrollView(position, options);
                }
                else
                {
                    position = EditorGUILayout.BeginScrollView(position, style, options);
                }
            }

            public void Dispose()
            {
                EditorGUILayout.EndScrollView();
            }
        }
    }

    public class GUI_CHANGE_CHECK : IDisposable
    {
        private readonly Action onChanged;

        public GUI_CHANGE_CHECK(Action onChanged)
        {
            EditorGUI.BeginChangeCheck();
            this.onChanged = onChanged;
        }

        public void Dispose()
        {
            if (EditorGUI.EndChangeCheck())
            {
                onChanged.Invoke();
            }
        }
    }

    public class GuiDisabledGroup : IDisposable
    {
        public GuiDisabledGroup(bool disabled)
        {
            EditorGUI.BeginDisabledGroup(disabled);
        }

        public void Dispose()
        {
            EditorGUI.EndDisabledGroup();
        }
    }
}