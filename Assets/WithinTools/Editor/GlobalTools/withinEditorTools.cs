﻿using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Within.SuperBloom
{
    /// <summary>
    /// Static class with tool methods to be used in Unity's Editor
    /// </summary>
    /// 
    [InitializeOnLoad]
    public static class WithinEditorTools
    {
        /// <summary>
        /// 	Creates a new Game Object in the Hierarchy and returns a reference to it
        /// </summary>
        /// <param name="_name"></param>
        /// <param name="parentGameObject"></param>
        /// <param name="_useIncrementalSubIndex">
        /// 	If true, will avoid to create a duplicated name for the Gameobject and add a numerical index after the name.
        /// </param>
        /// <param name="_sameLayerAsParent">
        /// 	if true, will assign to the new object the same label than its parent, if any
        /// </param> 
        /// <returns>the newly created GameObject</returns>
        public static GameObject CreateGameObjectInHierarchy(string _name, GameObject parentGameObject = null, 
            bool _useIncrementalSubIndex = true, bool _sameLayerAsParent = true)
        {
            string nameToUse = _name;

            if (_useIncrementalSubIndex)
            {
                int qty = 0;

                // it bases the index on the childs of current selected Gameobject, or the root scene if none is selected
                if (parentGameObject != null)
                {
                    while (parentGameObject.transform.Find(nameToUse) != null)
                    {
                        nameToUse = _name + "_" + qty++;
                    }
                }
                else
                {
                    while (GameObject.Find(nameToUse) != null)
                    {
                        nameToUse = _name + "_" + qty++;
                    }
                }
            }

            GameObject go = new GameObject(nameToUse);

            if (parentGameObject != null)
            {
                go.transform.SetParent(parentGameObject.transform);

                if (_sameLayerAsParent)
                {
                    go.layer = parentGameObject.layer;
                }
            }

            go.transform.localPosition = Vector3.zero;
            go.transform.localRotation = Quaternion.identity;
            go.transform.localScale = Vector3.one;

            return go;
        }

        public static T InstantiateFromPrefabAsset<T>(string prefabName) where T : Object
        {
            T fromPrefab = null;
            string[] guids = AssetDatabase.FindAssets(prefabName + " t:prefab");
            if (guids == null || guids.Length == 0)
            {
                Debug.LogError("No matching prefab '" + prefabName + "' found while trying to InstantiateFromPrefab");
                return null;
            }

            string pathOfPrefab = null;
            for (int i = 0; i < guids.Length; i++)
            {
                // Making things a bit more robust against other matching file names in the project
                string pathOfThisGuid = AssetDatabase.GUIDToAssetPath(guids[i]);
                if (IsInWithinToolsFolder(pathOfThisGuid))
                {
                    if (pathOfPrefab != null)
                    {
                        Debug.LogError(string.Format("More than one matching prefab found for '{0}'. '{1}' and '{2}'", prefabName, pathOfPrefab, pathOfThisGuid));
                        Debug.LogError("Prefab not instantiated");
                        return null;
                    }
                    pathOfPrefab = pathOfThisGuid;
                }
            }

            if (!string.IsNullOrEmpty(pathOfPrefab))
            {
                GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(pathOfPrefab);
                fromPrefab = PrefabUtility.InstantiatePrefab(prefab) as T;
            }

            return fromPrefab;
        }

        public static bool IsInWithinToolsFolder(string path)
        {
            string[] pathParts = path.Split('/');
            foreach (string part in pathParts)
            {
                if (part.ToUpper().Equals("WITHINTOOLS"))
                {
                    return true;
                }
            }

            return false;
        }

        public static string GetToolVersion()
        {
            string version = File.ReadAllText(SBToolsPaths.VERSION_FILE_PATH);

            return version;
        }
        
        private static string GetStoryToolVersion()
        {
            string version = File.ReadAllText(SBToolsPaths.STORY_TOOL_VERSION_FILE_PATH);

            return version;
        }

        public static void ShowHiddenObjects()
        {
            var allObjects = GameObject.FindObjectsOfType<GameObject>();
            foreach (var go in allObjects)
            {
                if ((go.hideFlags & HideFlags.HideInHierarchy) != 0)
                {
                    go.hideFlags = HideFlags.None;
                }
            }
        }

        public static void HideSelectedObjects()
        {
            GameObject[] selected = Selection.gameObjects;

            foreach (var go in selected)
            {
                go.hideFlags = go.hideFlags | HideFlags.HideInHierarchy;
                EditorSceneManager.MarkAllScenesDirty();
            }
        }

        public static void LockSelectedObjects()
        {
            GameObject[] selected = Selection.gameObjects;

            foreach (var go in selected)
            {
                go.hideFlags = go.hideFlags | HideFlags.NotEditable;
                EditorSceneManager.MarkAllScenesDirty();
            }
        }

        public static void UnlockSelectedObjects()
        {
            GameObject[] selected = Selection.gameObjects;

            foreach (var go in selected)
            {
                go.hideFlags = go.hideFlags & ~HideFlags.NotEditable;
                EditorSceneManager.MarkAllScenesDirty();
            }
        }
    }
}