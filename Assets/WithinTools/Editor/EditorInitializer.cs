﻿using UnityEditor;
using Within.SuperBloom;

public class EditorInitializer 
{
	[InitializeOnLoadMethod]
	public static void OnEditorStart()
	{
		StoryEditor.Instance.Close();
		
		MigrationUpdater.AutoUpdate();
		
		StoryEditor.Instance.Initialize();
		
		SBEditorEvents.storyEditorWindowRefresh.Invoke();
	}
}
