﻿using System;

namespace Within.SuperBloom
{
    public class BuildUtils
    {
        public static string GetBuildVersionFromCommandLine()
        {
            string[] args = Environment.GetCommandLineArgs();
            string versionArg = Array.Find(args, s => s.StartsWith("-buildVersion"));
            if (versionArg != null)
            {
                return versionArg.Substring(versionArg.IndexOf("=", StringComparison.Ordinal) + 1);
            }
            return string.Empty;
        }
    }
}