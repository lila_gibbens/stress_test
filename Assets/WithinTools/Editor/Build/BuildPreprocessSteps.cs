﻿using System;
using UnityEditor;
using UnityEditor.Build;
using UnityEngine;

namespace Within.SuperBloom
{
    /// <summary>
    /// OnPreprocessBuild is called by Unity after a build command has been issued. Scene list is already locked in at this point.
    /// </summary>
    public class BuildPreprocessSteps : IPreprocessBuild
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Implementing IPreprocessBuild ")]
        public int callbackOrder
        {
            get { return 0; }
        }

        public void OnPreprocessBuild(BuildTarget target, string path)
        {
            Debug.Log("Preprocessing build...");
            PrePlaySteps.RunSteps();

#if !SB_MAIN_APP
            GenerateVoiceRecognitionModel();
#endif
            UpdateUsageDescriptions();
            UpdateBuildNumber();
        }

        private void GenerateVoiceRecognitionModel()
        {
            if (!StoryEditor.Instance.Babel.GenerateModel())
            {
                throw new Exception("Canceling build on user request.");
            }
        }

        private void UpdateUsageDescriptions()
        {
            if (string.IsNullOrEmpty(PlayerSettings.iOS.cameraUsageDescription))
            {
                PlayerSettings.iOS.cameraUsageDescription = "Camera permission is required for Augmented Reality features.";
            }

            if (string.IsNullOrEmpty(PlayerSettings.iOS.microphoneUsageDescription))
            {
                PlayerSettings.iOS.microphoneUsageDescription = "Microphone permission is required to allow you to read aloud to progress the story.";
            }
        }

        private void UpdateBuildNumber()
        {
            string buildVersion = BuildUtils.GetBuildVersionFromCommandLine();
            if (!string.IsNullOrEmpty(buildVersion))
            {
                PlayerSettings.iOS.buildNumber = buildVersion;
            }
        }
    }
}
