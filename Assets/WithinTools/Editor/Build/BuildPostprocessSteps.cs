using System.IO;
using UnityEditor;
using UnityEditor.Build;

#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif

public class BuildPostprocessSteps : IPostprocessBuild
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Implementing IPostprocessBuild ")]
    public int callbackOrder
    {
        get { return 0; }
    }

    public void OnPostprocessBuild(BuildTarget target, string path)
    {
        if (target == BuildTarget.iOS)
        {
            AddExemptEncryptionEntry(path);
        }
    }

    private void AddExemptEncryptionEntry(string path)
    {
#if UNITY_IOS
        // Get plist
        string plistPath = path + "/Info.plist";
        PlistDocument plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(plistPath));

        // Get root
        PlistElementDict rootDict = plist.root;
        rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", false);

        // Write to file
        File.WriteAllText(plistPath, plist.WriteToString());
#endif
    }
}
