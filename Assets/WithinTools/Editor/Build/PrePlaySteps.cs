﻿using UnityEditor;

namespace Within.SuperBloom
{
    [InitializeOnLoad]
    public class PrePlaySteps
    {
        static PrePlaySteps()
        {
            EditorApplication.playModeStateChanged += PlaymodeStateChanged;
        }

        private static void PlaymodeStateChanged(PlayModeStateChange stateChange)
        {
            if (stateChange == PlayModeStateChange.ExitingEditMode)
            {
                RunSteps();
            }
        }

        public static void RunSteps()
        {
            // Currently not doing anything but leaving this here for any 
            // actions we want to perform before playing in the editor.
        }
    }
}
