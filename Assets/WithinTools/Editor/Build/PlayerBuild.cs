using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Within.SuperBloom
{
    public static class PlayerBuild
    {
        [MenuItem("AR Books/Build/Build iOS")]
        public static void BuildPlayeriOS()
        {
            DoBuildPlayeriOS(false);
        }

        [MenuItem("AR Books/Build/Build iOS Development")]
        public static void BuildPlayeriOSDevelopment()
        {
            DoBuildPlayeriOS(true);
        }

        private static void DoBuildPlayeriOS(bool isDevelopment)
        {
            Debug.Log("Building iOS Player...");

            BuildPlayerOptions options = new BuildPlayerOptions();
            options.locationPathName = "ios";
            options.scenes = EditorBuildSettings.scenes.Where(s => s.enabled).Select(s => s.path).ToArray();
            options.target = BuildTarget.iOS;

            if (isDevelopment)
            {
                options.options = BuildOptions.Development;
            }

            string error = BuildPipeline.BuildPlayer(options);

            if (!string.IsNullOrEmpty(error))
            {
                Debug.LogError("Player build failed with error: " + error);
            }
            else
            {
                Debug.Log("Build completed successfully");
            }
            
            if (InternalEditorUtility.inBatchMode)
            {
                if (string.IsNullOrEmpty(error))
                {
                    EditorApplication.Exit(0);
                }
                else
                {
                    EditorApplication.Exit(1);
                }
            }
        }
    }
}