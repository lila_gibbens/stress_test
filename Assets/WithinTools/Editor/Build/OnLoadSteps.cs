﻿using System.Collections.Generic;
using UnityEditor;

namespace Within.SuperBloom
{
    [InitializeOnLoad]
    public class OnLoadSteps
    {
        static OnLoadSteps()
        {
            // If we add any steps that take time to run, we should determine
            // if we've already run them for the current tools version
            // before running them again.
            AddRequiredScenesToBuildSettings();
        }
        
        public static void AddRequiredScenesToBuildSettings()
        {
            List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);
            foreach (string scenePath in SBToolsPaths.REQUIRED_SCENES)
            {
                if (!editorBuildSettingsScenes.Exists(s => s.path == scenePath) && System.IO.File.Exists(SBToolsPaths.GetProjectDirectory() + scenePath))
                {
                    editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(scenePath, true));
                }
            }

            EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();
        }
    }
}