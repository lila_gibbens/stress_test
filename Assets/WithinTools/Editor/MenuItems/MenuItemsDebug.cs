﻿#if SB_DEBUG_ENABLED
using UnityEditor;
using Within.SuperBloom;

namespace Within.SuperBloom
{
    public static class MenuItemsDebug 
    {
    	[MenuItem("AR Books/Debug/Hierarchy/Show Hidden Objects")]
    	private static void ShowHiddenObjects()
    	{
    		WithinEditorTools.ShowHiddenObjects();
    	}

    	[MenuItem("AR Books/Debug/Hierarchy/Hide Selected Object")]
    	public static void HideSelectedObjects()
    	{
    		WithinEditorTools.HideSelectedObjects();
    	}
    	
    	[MenuItem("AR Books/Debug/Hierarchy/Unlock Selected Object")]
    	public static void UnlockSelectedObjects()
    	{
    		WithinEditorTools.UnlockSelectedObjects();
    	}
    	
    	[MenuItem("AR Books/Debug/Hierarchy/Lock Selected Object")]
    	public static void LockSelectedObjects()
    	{
    		WithinEditorTools.LockSelectedObjects();
    	}
    		
    	[MenuItem("AR Books/Debug/Tool Version")]
    	public static void ShowVersionInEditor()
    	{
    		EditorUtility.DisplayDialog("AR Books Version", "Version: " + WithinEditorTools.GetToolVersion(), "Ok");
    	}

    	[MenuItem("AR Books/Debug/Wizard/Create DataFolders")]
    	public static void Wizard_CreateProjectDataFolders()
    	{
    		StoryWizardTools.CreateProjectDataFolders();
    	}

    	[MenuItem("AR Books/Debug/Wizard/Verify SceneStructure")]
    	public static void Wizard_VerifyScene()
    	{
    		StoryWizardTools.VerifyScene();
    	}

    	[MenuItem("AR Books/Debug/Wizard/Create Structure in Hierarchy")]
    	public static void Wizard_CreateStoryObjectInScene()
    	{
    		StoryWizardTools.CreateStoryObjectInScene();
    	}
    	
    	[MenuItem("AR Books/Debug/Clean/All Story [Forced]")]
    	public static void Clean_ForcePermaDeleteStoryPageAssets()
    	{
    		StoryEditor.Instance.ForceDeleteEntireStory();
    	}

    	[MenuItem("AR Books/Debug/Windows/EditorTools Window")]
    	public static void Show_EditorTools_Window()
    	{
    		WithinEditorToolsTestWindow.ShowWindow();
    	}

    	[MenuItem("AR Books/Debug/Settings/Create Asset")]
    	public static void Settings_CreateAsset()
    	{
    		SBToolSettings.CreateAsset();
    	}
    }
}
#endif