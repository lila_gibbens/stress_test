﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Within.SuperBloom.MenuItems
{
    public class StoryExport
    {
        [MenuItem("AR Books/Export Story")]
        private static void ExportStory()
        {
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
            EditorSceneManager.SaveOpenScenes();

            if (!StoryEditor.Instance.Babel.GenerateModel())
            {
                Debug.Log("Cancelling Story Export");
                return;
            }

            string outputPath = SBToolsPaths.URL_ASSET_BUNDLE_EXPORT_FOLDER;
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
            }

            StoryData storyDataAsset = SBAssets.GetStoryData();

            StoryDataMetaInfo exportMetaInfo = new StoryDataMetaInfo(storyDataAsset);

            WriteStoryJsonFile(exportMetaInfo);
            bool success = CreateStorySceneAssetBundle(exportMetaInfo.AssetBundleName, outputPath);
            AssetDatabase.Refresh();

            if (!success)
            {
                EditorUtility.DisplayDialog("Error", "Story Export failed. See Unity Console for details", "OK");
                return;
            }

            string packageLocation = CreateAssetPackage(exportMetaInfo.AssetBundleName, outputPath);

            EditorUtility.DisplayDialog("Export successful", "Story Exported Successfully to " + packageLocation, "OK");
        }

        private static void WriteStoryJsonFile(StoryDataMetaInfo storyDataMetaInfo)
        {
            string[] toJson = { JsonUtility.ToJson(storyDataMetaInfo, true) };

            var jsonPath = SBToolsPaths.GetStoryDataJsonPath(storyDataMetaInfo.AssetBundleName);

            File.WriteAllLines(jsonPath, toJson);
        }

        internal static bool CreateStorySceneAssetBundle(string bundleName, string outputPath)
        {
#if SB_DEBUG_ENABLED
            BuildTarget buildTarget = EditorUserBuildSettings.activeBuildTarget;
#else
            BuildTarget buildTarget = BuildTarget.iOS;
#endif
            AssetImporter.GetAtPath(SBToolsPaths.STORY_SCENE_ASSET_PATH).SetAssetBundleNameAndVariant(bundleName, string.Empty);

            AssetBundleBuild[] sceneBundleBuild = { new AssetBundleBuild() };
            sceneBundleBuild[0].assetBundleName = bundleName + ".unity3d";
            sceneBundleBuild[0].assetNames = new string[] { SBToolsPaths.STORY_SCENE_ASSET_PATH };

            return (BuildPipeline.BuildAssetBundles(outputPath, sceneBundleBuild, 0, buildTarget) != null);
        }

        private static string CreateAssetPackage(string storyName, string bundlePath)
        {
            // Copy the bundle files to StreamingAssets before packaging
            string streamingAssets = "Assets/StreamingAssets/";
            string bundle = storyName + ".unity3d";
            string json = storyName + ".json";

            Directory.CreateDirectory(SBToolsPaths.GetProjectDirectory() + streamingAssets);
            AssetDatabase.CopyAsset(bundlePath + bundle, streamingAssets + bundle);
            AssetDatabase.CopyAsset(SBToolsPaths.GetStoryDataJsonPath(storyName), streamingAssets + json);

            // Package includes asset bundle, story data and roger model (fst + txt)
            List<string> packagedAssets = new List<string>
            {
                streamingAssets + bundle,
                streamingAssets + json,
                BabelInterface.ModelDirectory + "/" + storyName + "/" + storyName + ".fst",
                BabelInterface.ModelDirectory + "/" + storyName + "/" + storyName + ".txt"
            };
            AssetDatabase.ExportPackage(packagedAssets.ToArray(), storyName + ".unitypackage", ExportPackageOptions.Recurse | ExportPackageOptions.Default);

            // Clean up StreamingAssets
            AssetDatabase.DeleteAsset(streamingAssets + bundle);
            AssetDatabase.DeleteAsset(streamingAssets + json);

            return SBToolsPaths.GetProjectDirectory() + storyName + ".unitypackage";
        }
    }
}