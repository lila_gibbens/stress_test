﻿using UnityARInterface;
using UnityEditor;

namespace Within.SuperBloom
{
	[CustomEditor(typeof(ARController))]
	public class ARControllerInspector : Editor 
	{
		private ARControllerSharedInspector<ARController, ARRemoteEditor> sharedInspector;
		private void OnEnable()
		{
			sharedInspector = new ARControllerSharedInspector<ARController, ARRemoteEditor>(this, "Switch To Remote Control");
		}

		public override void OnInspectorGUI()
		{
			sharedInspector.OnInspectorGUI();
		}
	}
}