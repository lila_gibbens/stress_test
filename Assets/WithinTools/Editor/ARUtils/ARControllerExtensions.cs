﻿using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom.ExtensionMethods
{
	public static class SerializedObjectExtensions
	{
		/// <summary>
		/// Copies properties specified from object to this object.
		/// </summary>
		/// <param name="thisObject"></param>
		/// <param name="properties"></param>
		/// <param name="fromObject"></param>
		/// <exception cref="System.ArgumentException">Throws if properties not specified or empty, or if fromObject is null</exception>
		/// <returns>The number of properties copied</returns>
		static public int CopyProperties(this SerializedObject thisObject, string[] properties, SerializedObject fromObject)
		{
			if (properties == null || properties.Length == 0)
			{
				throw new System.ArgumentException("No properties specified, CopyProperties failed.");
			}
			if (fromObject == null)
			{
				throw new System.ArgumentException("Object to copy properties from is null, CopyProperties failed.");
			}

			int propertiesCopied = 0;			
			foreach (string propertyName in properties)
			{
				SerializedProperty prop = fromObject.FindProperty(propertyName);
				if (prop == null)
				{
					Debug.LogWarning(string.Format("Property '{0}' not found on object while copying properties", propertyName));
                }
				else
				{
					propertiesCopied++;
					thisObject.CopyFromSerializedProperty(prop);
				}
			}

			return propertiesCopied;
		}
	}
}