﻿using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
    [CustomEditor(typeof(StoryAreaPlane))]
    public class StoryAreaPlaneInspector : Editor
    {
        private const float SIDE_AREA_SIZE = 2.5f;
        private const float HALF_SIDE_AREA_SIZE = SIDE_AREA_SIZE * 0.5f;

        private Color fillColor = new Color(0, 0.5f, 0, 0.05f);
        private Color outlineColor = new Color(0, 1, 0, 0.5f);

        private void OnSceneGUI()
        {
            StoryAreaPlane storyArea = (StoryAreaPlane)target;
            Vector3 position = storyArea.transform.position;

            float halfWidth = storyArea.Width * 0.5f;
            float halfHeight = storyArea.Height * 0.5f;

            if (storyArea.ZAxisFrontSide != StoryAreaPlane.AxisDirection.None)
            {
                float offsetDirection = (storyArea.ZAxisFrontSide == StoryAreaPlane.AxisDirection.Positive ? 1 : -1);
                var centerOffset = (offsetDirection * (halfHeight + HALF_SIDE_AREA_SIZE));

                Vector3[] corners = 
                    {
                        new Vector3(position.x - halfWidth, position.y, centerOffset + (position.z - HALF_SIDE_AREA_SIZE)),
                        new Vector3(position.x - halfWidth, position.y, centerOffset + (position.z + HALF_SIDE_AREA_SIZE)),
                        new Vector3(position.x + halfWidth, position.y, centerOffset + (position.z + HALF_SIDE_AREA_SIZE)),
                        new Vector3(position.x + halfWidth, position.y, centerOffset + (position.z - HALF_SIDE_AREA_SIZE))
                    };

                Handles.DrawSolidRectangleWithOutline(corners, fillColor, outlineColor);
            }

            if (storyArea.XAxisFrontSide != StoryAreaPlane.AxisDirection.None)
            {
                float offsetDirection = (storyArea.XAxisFrontSide == StoryAreaPlane.AxisDirection.Positive ? 1 : -1);
                var centerOffset = (offsetDirection * (halfWidth + HALF_SIDE_AREA_SIZE));

                Vector3[] corners = 
                    {
                        new Vector3(centerOffset + (position.x - HALF_SIDE_AREA_SIZE), position.y, position.z - halfHeight),
                        new Vector3(centerOffset + (position.x + HALF_SIDE_AREA_SIZE), position.y, position.z - halfHeight),
                        new Vector3(centerOffset + (position.x + HALF_SIDE_AREA_SIZE), position.y, position.z + halfHeight),
                        new Vector3(centerOffset + (position.x - HALF_SIDE_AREA_SIZE), position.y, position.z + halfHeight)
                    };

                Handles.DrawSolidRectangleWithOutline(corners, fillColor, outlineColor);
            }
        }
    }
}