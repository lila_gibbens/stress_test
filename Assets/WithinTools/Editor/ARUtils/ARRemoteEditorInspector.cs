﻿using UnityARInterface;
using UnityEditor;

namespace Within.SuperBloom
{
	[CustomEditor(typeof(ARRemoteEditor))]
	public class ARRemoteEditorInspector : Editor 
	{
		private ARControllerSharedInspector<ARRemoteEditor, ARController> sharedInspector;
		private void OnEnable()
		{
			sharedInspector = new ARControllerSharedInspector<ARRemoteEditor, ARController>(this, "Disable Remote Control");
		}

		public override void OnInspectorGUI()
		{
			sharedInspector.OnInspectorGUI();
		}
	}
}