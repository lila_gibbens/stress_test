﻿using UnityARInterface;
using UnityEditor;
using UnityEngine;
using Within.SuperBloom.ExtensionMethods;

namespace Within.SuperBloom
{
	/// <summary>
	/// T is the controller this editor is for. U is the controller it can be replaced with.
	/// </summary>
	public class ARControllerSharedInspector<T, U> where T : ARController where U : ARController 
	{
		public static readonly string[] COMMON_PROPERTIES = 
            {
                "m_PlaneDetection", "m_ARCamera", "m_LightEstimation", "m_PointCloud",
                "m_Scale", "pointOfInterest", "m_BackgroundRendering"
            };

		private Editor controllerEditor;
		private string buttonName;

		public ARControllerSharedInspector(Editor controllerEditor, string buttonName)
		{
			this.controllerEditor = controllerEditor;
			this.buttonName = buttonName;
		}

		/// <summary>
		/// Must be called from an ARController/ARRemoteEditor's custom inspector
		/// </summary>
		public void OnInspectorGUI()
		{
			ARController aRController = (ARController)controllerEditor.target;
			controllerEditor.DrawDefaultInspector();

			if (GUILayout.Button(buttonName))
			{
				SwitchToOtherController(aRController);				
			}
		}

		public void SwitchToOtherController(ARController currentController)
		{
			GameObject controllerGO = currentController.gameObject;
			U newController = controllerGO.AddComponent<U>();
			
			SerializedObject newControllerSO = new UnityEditor.SerializedObject(newController);
			newControllerSO.CopyProperties(COMMON_PROPERTIES, controllerEditor.serializedObject);
			newControllerSO.ApplyModifiedProperties();

			Editor.DestroyImmediate(currentController);		
		}
	}
}