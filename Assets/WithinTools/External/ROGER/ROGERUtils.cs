﻿using System;
using System.Text;
using System.Collections.Generic;

public static class ROGERUtils {
	
	public static string[] ExtractWords(string line) {
		string l = line.Trim();

		StringBuilder sb = new StringBuilder();
		List<string> words = new List<string>();

		for (int j = 0; j < l.Length; j++) {
			char c = l[j];

			if (c == ' ') {
				if (sb.Length != 0) {
					words.Add(sb.ToString());
					sb = new StringBuilder();
				}
				continue;
			} else if (c == '.') {
				// . is only valid when in words like U.S or Mr.
				// note when . is at the end of a line I assume it's period not part of word
				if (j == 0 || j == l.Length - 1 || !char.IsLetter(l[j-1])) {
					continue;
				}
			} else if (c == '\'') {
				// ' is only valid when in words like Mike's or ants'
				if (j == 0 || !char.IsLetter(l[j-1])) {
					continue;
				}
			} else if (c == '-' || c == '_') {
				// - and _ are only valid when in middle of a word like t-shirt
				if (j == 0 || j == l.Length - 1 || !char.IsLetter(l[j-1]) || !char.IsLetter(l[j+1])) {
					continue;
				}
			} else if (!char.IsLetter(c)) {
				continue;
			}

			sb.Append(c);
		}

		if (sb.Length != 0) {
			words.Add(sb.ToString());
		}

		return words.ToArray();
	}

}

