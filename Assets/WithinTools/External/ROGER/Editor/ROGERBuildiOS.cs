﻿#if UNITY_IOS
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System.IO;

public class ROGERBuildiOS : MonoBehaviour {

	private static string rogerFolderName = "ROGER_files";

	[PostProcessBuildAttribute]
	public static void OnPostprocessBuild(BuildTarget target, string path) {
		if (target == BuildTarget.iOS) {
			PBXProject project = new PBXProject ();

			string projectPath = PBXProject.GetPBXProjectPath (path);

			project.ReadFromFile (projectPath);

			string targetGUID = project.TargetGuidByName ("Unity-iPhone");

			// Copy language model files for ROGER
			string targetFolder = Path.Combine (path, rogerFolderName);
			CopyFiles ("Assets/WithinTools/External/ROGER/" + rogerFolderName, targetFolder);
			AddFiles (project, targetGUID, targetFolder);

			// Perform necessary project settings for ROGER
			project.AddFrameworkToProject (targetGUID, "Accelerate.framework", false);

			project.SetBuildProperty (targetGUID, "VALID_ARCHS", "arm64");
			project.SetBuildProperty (targetGUID, "ENABLE_BITCODE", "NO");
			project.SetBuildProperty (targetGUID, "ONLY_ACTIVE_ARCH", "YES");

			project.WriteToFile (projectPath);
		}
	}

	internal static void AddFiles (PBXProject project, string guid, string srcFolder) {
		foreach (var file in Directory.GetFiles (srcFolder)) {
			project.AddFileToBuild(guid, project.AddFile (file, Path.Combine (rogerFolderName, Path.GetFileName (file))));
		}

		foreach (var folder in Directory.GetDirectories (srcFolder)) {
			AddFiles (project, guid, Path.Combine (srcFolder, Path.GetFileName (folder)));
		}
	}

	internal static void CopyFiles (string srcPath, string dstPath)
	{
		if (Directory.Exists (dstPath))
			Directory.Delete (dstPath, true);
		
		if (File.Exists (dstPath))
			File.Delete (dstPath);

		Directory.CreateDirectory (dstPath);

		foreach (var file in Directory.GetFiles (srcPath)) {
			string ext = Path.GetExtension (file);
			if (ext == ".meta" || ext == ".DS_Store")
				continue;
			File.Copy (file, Path.Combine (dstPath, Path.GetFileName (file)));
		}

		foreach (var dir in Directory.GetDirectories (srcPath))
			CopyFiles(dir, Path.Combine(dstPath, Path.GetFileName (dir)));
	}
}
#endif