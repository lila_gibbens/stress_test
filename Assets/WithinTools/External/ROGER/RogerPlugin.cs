﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using AOT;


public class ROGERPlugin: MonoBehaviour {
	
	/// <summary>
	/// This delegate is triggerred by ROGER every x seconds(x is specified by pollRate when start) after it's started
	/// </summary>
	/// <param name="result"> A string contaning recognized words so far, words are seperated by space, all cap, no punctuations </param>
	public delegate void RogerResultDelegate (string result);
	public static event RogerResultDelegate onRogerRawResult;

	/// <summary>
	/// This delegate provides the word for word matching result
	/// </summary>
	/// <param name="matchCnt"> An int showing how many words counting from the beginning have matched so far </param>
	public delegate void RogerWordForWordMatchDelegate (int matchCnt);
	public static event RogerWordForWordMatchDelegate onRogerWordForWordResult;

	private static int maxMatchCnt = 0;
	
	private static string prevCachedResult;

	private static string lastRogerResult;
	
#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void roger_initMicrophone ();

	[DllImport("__Internal")]
	private static extern void roger_overrideToBottomSpeaker ();

	[DllImport("__Internal")]
	private static extern void roger_initDecoder ();

	[DllImport("__Internal")]
	private static extern void roger_initDecoderWithModel (string modelId);

	[DllImport("__Internal")]
	private static extern void roger_setupModel (string modelId);

	[DllImport("__Internal")]
	private static extern void roger_setResultCallback (RogerResultDelegate callback);

	[DllImport("__Internal")]
	private static extern void roger_start (float pollRate);

	[DllImport("__Internal")]
	private static extern void roger_stop ();

	[MonoPInvokeCallback(typeof (RogerResultDelegate))]
	private static void rogerResultReceived (string result) {
		result = prevCachedResult + result;

		if (result == lastRogerResult) {
			return;
		}

		lastRogerResult = result;

		if (onRogerRawResult != null) {
			onRogerRawResult (result);
		}

		string[] resultWords = result.Split (' ');

		int matchCnt = 0, matchPos = 0;
		int lastMatchPos = 0;

		while (matchCnt < curWords.Length && matchPos < resultWords.Length) {
			if (matchCnt < curWords.Length - 1 && skippableWords.Contains (curWords [matchCnt])) {
				matchCnt++;
			} else if (curWords [matchCnt] == resultWords [matchPos]) {
				matchCnt++;
				lastMatchPos = matchPos;
				matchPos++;
			} else {
				matchPos++;
			}
		}

		int prevMatchCnt = matchCnt;
		// find last trailing skippable word
		int m = matchCnt - 1;
		for (; m >= 0; m--) {
			if (!skippableWords.Contains (curWords [m]))
				break;
		}
		m += 1;
		matchCnt = m;

		int n = lastMatchPos + 1;
		while (m < prevMatchCnt && n < resultWords.Length) {
			if (curWords [m] == resultWords [n]) {
				matchCnt++;
				m++;
			}
			n++;
		}

		if (matchCnt == 0)
			return;

		// ams tends to predict one word faster than what was actually said
		// hense some handling 
		if (matchCnt == 1) {
			// matched first word
		} else if (matchCnt == curWords.Length) {
			// matched last word
			if (matchPos == resultWords.Length) {
				// very likely the last word hasn't been said so don't count as match yet
				matchCnt--;
			}
		} else {
			// compensate for this ams tendency
			matchCnt--;
		}

		if (matchCnt > maxMatchCnt) {
			// don't go back for now
			maxMatchCnt = matchCnt;
			if (onRogerWordForWordResult != null) {
				onRogerWordForWordResult (matchCnt);
			}
		}
	}

	#endif

	private static bool isMicrophoneInitialized = false;
	/// <summary>
	/// Ask for microphone permission and start audio session with ROGER default settings
	/// </summary>
	public static void initializeMicrophone() {
		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			if (!isMicrophoneInitialized) {
				roger_initMicrophone();
				isMicrophoneInitialized = true;
			}
		}
		#endif
	}

	/// <summary>
	/// Utility function for overriding the sound output to bottom speaker for iOS
	/// This is because Unity Microphone.Start() has a bug that always direct output to top speaker for phone call 
	/// https://issuetracker.unity3d.com/issues/ios-audio-will-be-played-from-earspeaker-if-microphone-dot-start-is-used
	/// </summary>
	public static void overrideToBottomSpeaker () {
		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			roger_overrideToBottomSpeaker ();
		}
		#endif
	}

	private static bool isRogerInitialized = false;
	/// <summary>
	/// Initialize ROGER, optionally with a specific model setup as well
	/// </summary>
	/// <param name="modelId"> The model name for ROGER to recognize </param>
	public static void initializeRoger (string modelId = null) {
		if (isRogerInitialized) {
			return;
		}

		loadSkippableWords ();

		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			if (modelId != null) {
				roger_initDecoderWithModel (modelId);
				curModelId = modelId;
			} else {
				roger_initDecoder();
			}
			roger_setResultCallback (rogerResultReceived);
		}
		#endif

		isRogerInitialized = true;
	}

	// TODO read from file
	private static HashSet<string> skippableWords;
	private static void loadSkippableWords () {
		skippableWords = new HashSet<string> ();
		skippableWords.Add ("THE");
		skippableWords.Add ("A");
		skippableWords.Add ("AN");
	}

	private static string[] curWords;
	private static string curModelId;

	/// <summary>
	/// Setup ROGER to recognize a specific sentence with a specific model
	/// </summary>
	/// <param name="modelId"> The model Id for ROGER to recognize </param>
	public static void setupRoger (string modelId, string curSentence) {
		if (hasStarted ()) {
			Debug.LogWarning ("ROGER has been started with model: " + curModelId + " Stopping it before new setup");
			stopRoger ();
		}

		curWords = new string(curSentence.Where(c => !char.IsPunctuation(c)).ToArray()).Split(' ');
		for (int i = 0; i < curWords.Length; i++) {
			curWords [i] = curWords [i].ToUpper ();
		}

		maxMatchCnt = 0;

		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			if (modelId != curModelId) {
				curModelId = modelId;
				roger_setupModel(modelId);
			}
		}
		#endif
	}

	private static float pollRate = 0.1f;
	private static ROGERSession session;
	public static bool hasStarted () {
		return session != null;
	}

	/// <summary>
	/// Start ROGER voice recognition
	/// </summary>
	/// <param name="pollRate"> The interval we are polling the result, in seconds. 
	/// Larger interval means better perf, slower recognition </param>
	/// <param name="maxContinuousTime"> The max duration ROGER will decode continuously, in seconds.
	/// After this duration ROGER will cache the current result and restart, this is so perf doesn't deterioate over time because of increasing data set.
	/// 0 is for infinite </param>
	public static void startRoger (float thePollRate, float maxContinuousTime = 30f) {
		Assert.IsTrue (isMicrophoneInitialized, "Microphone is not initialized");
		Assert.IsTrue (isRogerInitialized, "ROGER is not initialized");
		Assert.IsNotNull (curModelId, "ROGER is not setup with a lang model");

		if (!isMicrophoneInitialized || !isRogerInitialized || curModelId == null) {
			return;
		}

        pollRate = thePollRate;
		session = new GameObject ().AddComponent<ROGERSession> ();
		session.timeBeforeRestart = maxContinuousTime;

		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			roger_start (pollRate);
		}
		#endif
	}

	/// <summary>
	/// Stop ROGER void recognition
	/// </summary>
	public static void stopRoger () {
		if (!hasStarted ()) {
			return;
		}

		maxMatchCnt = 0;
		prevCachedResult = "";

		session.stop ();
		Destroy (session);
		session = null;

		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			roger_stop();
		}
		#endif
	}

	private static bool isPaused = false;
	/// <summary>
	/// If ROGER is active
	/// </summary>
	/// <returns><c>true</c>, if ROGER is actively listening and returing result <c>false</c> otherwise.</returns>
	public static bool isListening () {
		return hasStarted () && !isPaused;
	}

	/// <summary>
	/// Temporarily pause ROGER, ROGER will not listen and return any more result until resumed
	/// Utilize this to avoid unnecessary ROGER processing
	/// </summary>
	public static void pauseRoger () {
		if (!hasStarted () || isPaused) {
			return;
		}

		isPaused = true;

		prevCachedResult = lastRogerResult;

		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			roger_stop ();
		}
		#endif
	}

	/// <summary>
	/// Resumes ROGER, ROGER will continue listen and return result
	/// </summary>
	public static void resumeRoger () {
		if (!hasStarted () || !isPaused) {
			return;
		}

		isPaused = false;

		#if UNITY_IOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			roger_start (pollRate);
		}
		#endif
	}
}