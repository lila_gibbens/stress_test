﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ROGERSession : MonoBehaviour {
	
	public float timeBeforeRestart = 10f;

	void Start () {
		StartCoroutine (waitAndRestart ());
	}

	IEnumerator waitAndRestart () {
		if (timeBeforeRestart == 0) {
			yield break;
		}

		while (true) {
			yield return new WaitForSeconds (timeBeforeRestart);

			// This is a performance optimization hack
			// By doing this roger caches the result up til now and only parse new voice input going forward
			ROGERPlugin.pauseRoger ();
			ROGERPlugin.resumeRoger ();
		}
	}

	public void stop () {
		StopAllCoroutines ();
	}

	private bool shouldResume = false;
	IEnumerator OnApplicationFocus (bool hasFocus) {
		if (!hasFocus) {
			shouldResume = ROGERPlugin.isListening ();
			StopAllCoroutines ();
			ROGERPlugin.pauseRoger ();
		} else {
			if (shouldResume) {
				// Wait for 1 sec so that others finishes handling with microphone
				yield return new WaitForSeconds (1f);
				ROGERPlugin.resumeRoger ();
				StartCoroutine (waitAndRestart ());
			}
		}
	}
}
