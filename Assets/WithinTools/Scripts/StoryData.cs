﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Within.SuperBloom
{
    [Serializable]
    public class StoryData : ScriptableObject
    {
        public static string GetTitleFilename(string title)
        {
            Regex regex = new Regex("[^a-zA-Z0-9-]");
            return regex.Replace(title, String.Empty).ToLower();
        }

        public StoryData Clone()
        {
            StoryData newData = ScriptableObject.CreateInstance<StoryData>();
            newData.CopyFrom(this);

            return newData;
        }

        public void CopyFrom(StoryData originData)
        {
            this.Title = originData.Title;
            this.Description = originData.Description;
            this.IsPrerecordedNarrationOn = originData.IsPrerecordedNarrationOn;
            
            this.dataPages = new List<StoryPageData>(originData.dataPages);
        }

        [SerializeField]
        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        [SerializeField]
        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [SerializeField]
        private bool isPrerecordedNarrationOn;
        public bool IsPrerecordedNarrationOn
        {
            get { return isPrerecordedNarrationOn; }
            set { isPrerecordedNarrationOn = value; }
        }

        [SerializeField] 
        public List<StoryPageData> dataPages = new List<StoryPageData>();
        public List<StoryPageData> DataPages
        {
            get { return dataPages; }
        }

        public void SetPageDataList(List<StoryPageData> list)
        {
            dataPages = new List<StoryPageData>(list);
        }
    }
    
    public class StoryDataMetaInfo
    {
        public string Title;
        public string Description;
        public string ToolsVersion;
        public string AssetBundleName;
        public bool IsPrerecordedNarrationOn;

        public StoryDataMetaInfo(StoryData fromData)
        {
            Title = fromData.Title;
            Description = fromData.Description;
            IsPrerecordedNarrationOn = fromData.IsPrerecordedNarrationOn;
            AssetBundleName = StoryData.GetTitleFilename(Title);
        }
    }
}