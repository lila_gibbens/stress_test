﻿using UnityEngine.Events;

namespace Within.SuperBloom
{
    public class SBAppEvents
    {
        public class StoryLoad_RequestEvent : UnityEvent<string> { }
        
        public static readonly StoryLoad_RequestEvent LoadRequestStoryRequest = new StoryLoad_RequestEvent();
        
        public static readonly UnityEvent OnStoryReadyToPlay = new UnityEvent();     
    }
}