using UnityEditor;

namespace Within.SuperBloom
{
    [CustomEditor(typeof(SBInteractable))]
    public class SBInteractableAbstractDrawer : Editor
    {
        private SBInteractable interactable;

        private void OnEnable()
        {
            interactable = serializedObject.targetObject as SBInteractable;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            if (interactable.LifeTime == EInteractableLifeTime.Count)
            {
                DrawDefaultInspector();
            }
            else
            {
                DrawPropertiesExcluding(serializedObject, new string[] { "QtyInteractionsRemaining" });
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}