﻿using UnityEngine;

public class ToggleEnable_OnStart : MonoBehaviour
{
	public bool ToggleTo;
	
	private void Start() 
	{
		gameObject.SetActive(ToggleTo);		
	}
}
