﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Within.SuperBloom
{
    public enum EInteractableLifeTime
    {
        Once, Count, Infinite
    }

    public abstract class SBInteractable : MonoBehaviour
    {
        [Tooltip("How many times this can be interacted")]
        public EInteractableLifeTime LifeTime = EInteractableLifeTime.Infinite;

        public bool InteractionEnabled = true;

        [Tooltip("Objects that contain InteractBehaviours")]
        public GameObject[] TargetObjects;

        [Tooltip("Amount of interactions before this becomes inactive")]
        public int QtyInteractionsRemaining;
        
        // todo: change to trigger any kind of Behaviour instead of just the exit loop one
        [Tooltip("If bigger than 0, will decrease on each interaction and trigger to exit current Timeline loop when reaches 0")]
        public int TriggerExitLoopAfter;

        private List<SBInteractBehaviour> allBehaviours;

        protected void Awake()
        {
            allBehaviours = new List<SBInteractBehaviour>();

            switch (LifeTime)
            {
                case EInteractableLifeTime.Once:
                    QtyInteractionsRemaining = 1;
                    break;
                case EInteractableLifeTime.Count:

                    break;
                case EInteractableLifeTime.Infinite:
                    QtyInteractionsRemaining = int.MaxValue;
                    break;
            }

            foreach (GameObject go in TargetObjects)
            {
                allBehaviours.AddRange(go.GetComponents<SBInteractBehaviour>());
            }

            OnAwake();
        }

        /// <summary>
        ///  Remember, you CANNOT override the default Awake method, so use this one instead
        /// </summary>
        protected abstract void OnAwake();

        protected void OnInteractionStarted(Hashtable args = null)
        {
            if (QtyInteractionsRemaining <= 0 && LifeTime != EInteractableLifeTime.Infinite)
            {
                return;
            }

            QtyInteractionsRemaining--;

            if (TriggerExitLoopAfter > 0)
            {
                TriggerExitLoopAfter--;

                if (TriggerExitLoopAfter == 0)
                {
                    TimelineGraphEvents.OnFinishLoopRequested.Invoke();
                }
            }

            if (args == null)
            {
                args = new Hashtable();
            }

            foreach (SBInteractBehaviour behav in allBehaviours)
            {
                if (behav.gameObject.activeInHierarchy)
                {
                    behav.OnBehaviourStarted(args);                    
                }
            }
        }

        protected void OnInteractionUpdate(Hashtable args = null)
        {
            if (QtyInteractionsRemaining <= 0 && LifeTime != EInteractableLifeTime.Infinite)
            {
                return;
            }

            if (args == null)
            {
                args = new Hashtable();
            }

            foreach (SBInteractBehaviour behav in allBehaviours)
            {
                if (behav.gameObject.activeInHierarchy)
                {
                    behav.OnBehaviourUpdate(args);                    
                }
            }
        }

        protected void OnInteractionFinished(Hashtable args = null)
        {
            if (QtyInteractionsRemaining <= 0 && LifeTime != EInteractableLifeTime.Infinite)
            {
                return;
            }

            if (args == null)
            {
                args = new Hashtable();
            }

            foreach (SBInteractBehaviour behav in allBehaviours)
            {
                if (behav.gameObject.activeInHierarchy)
                {
                    behav.OnBehaviourFinished(args);                    
                }
            }
        }
    }
}