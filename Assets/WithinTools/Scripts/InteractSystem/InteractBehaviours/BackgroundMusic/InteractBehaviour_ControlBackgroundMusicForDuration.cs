﻿using System.Collections;
using UnityEngine;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_ControlBackgroundMusicForDuration : SBInteractBehaviour
    {
        [Tooltip("Either SetVolume, Pause, Start Ducking Music or Stop Ducking Music.")]
        public MusicEffectControlType EffectControlType;
        
        [Tooltip("For how long the effect will be applied")]
        public float EffectDuration;

        [Tooltip("Apply crossfade effect")]
        public bool CrossfadeEffect = true;
        
        [Tooltip("Duration of the crossfade")]
        public float CrossfadeDuration = BackgroundMusicPlayer.DEFAULT_FADE_DURATION;
        
        [Tooltip("The target volume of the music after the crossfade.")]
        [Range(0, 1)] 
        public float SetVolume = 1f;
        
        [Tooltip("Target low pass frequency.")]
        public float SetLowPassFrequency = BackgroundMusicPlayer.DEFAULT_LOWPASS_FREQUENCY;

        public override void OnBehaviourStarted(Hashtable args)
        {
            StartCoroutine(PlayEffectForDuration(EffectDuration));
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
        }

        private IEnumerator PlayEffectForDuration(float duration)
        {
            float restoreVolume = BackgroundMusicPlayer.Instance.GetBackgroundMusicVolume();
            AudioClip currentClip = BackgroundMusicPlayer.Instance.GetBackgroundMusicClip();
            float restoreFrequency = BackgroundMusicPlayer.Instance.GetDuckFrequency();

            BGMCommand musicCommand = new BGMCommand
            {
                ControlType = EffectControlType,
                Clip = null,
                Volume = SetVolume,
                CrossFadeTime = CrossfadeDuration,
                LowPassFrequency = SetLowPassFrequency
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommand);

            float startTime = Time.time;
            while (Time.time - startTime <= duration)
            {
                yield return null;
            }

            BGMCommand musicCommandReverse = new BGMCommand
            {
                ControlType = EffectControlType,
                Clip = currentClip,
                Volume = restoreVolume,
                CrossFadeTime = CrossfadeDuration,
                LowPassFrequency = restoreFrequency
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommandReverse);
        }
    }
}