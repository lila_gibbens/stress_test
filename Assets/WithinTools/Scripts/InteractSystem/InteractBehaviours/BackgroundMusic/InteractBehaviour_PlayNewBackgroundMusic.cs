﻿using System.Collections;
using UnityEngine;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_PlayNewBackgroundMusic : SBInteractBehaviour
    {
        [Tooltip("The music audio clip to be played.")]
        public AudioClip MusicClip;
        
        [Tooltip("If true, if this music was already played before and it's being played again, will resume from the last point it was positioned instead of the beginning.")]
        public bool ResumeMusicClipIfPreviouslyPlayed = true;

        [Tooltip("Apply crossfade effect")]
        public bool CrossfadeMusic = true;
        
        [Tooltip("Duration of the crossfade")]
        public float CrossfadeDuration = BackgroundMusicPlayer.DEFAULT_FADE_DURATION;
        
        [Tooltip("The target volume of the music after the crossfade")]
        [Range(0, 1)] 
        public float SetVolume = 1f;

        public override void OnBehaviourStarted(Hashtable args)
        {
            PlayNewMusicClip();
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
        }

        private void PlayNewMusicClip()
        {
            BGMCommand musicCommand = new BGMCommand
            {
                ControlType = MusicEffectControlType.PlayMusic,
                Clip = MusicClip,
                Volume = SetVolume,
                CrossFadeTime = CrossfadeDuration
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
        }
    }
}