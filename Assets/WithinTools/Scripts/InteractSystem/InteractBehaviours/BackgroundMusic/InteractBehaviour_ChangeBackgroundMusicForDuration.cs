﻿using System.Collections;
using UnityEngine;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_ChangeBackgroundMusicForDuration : SBInteractBehaviour
    {
        [Tooltip("The music audio clip to be played.")]
        public AudioClip MusicClip;
        
        [Tooltip("For how long this music will be played before going back to the previous track (if any)")]
        public float MusicClipDuration;

        [Tooltip("Apply crossfade effect")]
        public bool CrossfadeMusic = true;
        
        [Tooltip("Duration of the crossfade")]
        public float CrossfadeDuration = BackgroundMusicPlayer.DEFAULT_FADE_DURATION;
        
        [Tooltip("The target volume of the music after the crossfade")]
        [Range(0, 1)] 
        public float SetVolume = 1f;

        public override void OnBehaviourStarted(Hashtable args)
        {
            StartCoroutine(PlayMusicForDuration(MusicClipDuration));
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
        }

        private IEnumerator PlayMusicForDuration(float duration)
        {
            float restoreVolume = BackgroundMusicPlayer.Instance.GetBackgroundMusicVolume();
            AudioClip currentClip = BackgroundMusicPlayer.Instance.GetBackgroundMusicClip();
            
            BGMCommand musicCommand = new BGMCommand
            {
                ControlType = MusicEffectControlType.PlayMusic,
                Clip = MusicClip,
                Volume = SetVolume,
                CrossFadeTime = CrossfadeDuration
            };
            
            BackgroundMusicPlayer.Instance.DoCommand(musicCommand);

            float startTime = Time.time;
            while (Time.time - startTime <= duration)
            {
                yield return null;
            }

            BGMCommand musicCommandResume = new BGMCommand
            {
                ControlType = MusicEffectControlType.ResumeMusic,
                Clip = currentClip,
                Volume = restoreVolume,
                CrossFadeTime = CrossfadeDuration
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommandResume);
        }
    }
}