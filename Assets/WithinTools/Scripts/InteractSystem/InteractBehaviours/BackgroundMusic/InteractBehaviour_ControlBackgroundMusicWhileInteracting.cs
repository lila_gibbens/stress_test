﻿using System.Collections;
using UnityEngine;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_ControlBackgroundMusicWhileInteracting : SBInteractBehaviour
    {
        [Tooltip("Either SetVolume, Pause, Start Ducking Music or Stop Ducking Music.")]
        public MusicEffectControlType EffectControlType;

        [Tooltip("Apply crossfade effect.")]
        public bool CrossfadeEffect = true;
        
        [Tooltip(" Duration of the crossfade")]
        public float CrossfadeDuration = BackgroundMusicPlayer.DEFAULT_FADE_DURATION;
        
        [Tooltip("The target volume of the music after the crossfade.")]
        [Range(0, 1)] 
        public float SetVolume = 1f;
        
        [Tooltip("Target low pass frequency.")]
        public float SetLowPassFrequency = BackgroundMusicPlayer.DEFAULT_LOWPASS_FREQUENCY;

        private float restoreVolume;
        private float restoreFrequency;
        private AudioClip currentClip;

        public override void OnBehaviourStarted(Hashtable args)
        {
            restoreVolume = BackgroundMusicPlayer.Instance.GetBackgroundMusicVolume();
            currentClip = BackgroundMusicPlayer.Instance.GetBackgroundMusicClip();
            restoreFrequency = BackgroundMusicPlayer.Instance.GetDuckFrequency();

            BGMCommand musicCommand = new BGMCommand
            {
                ControlType = EffectControlType,
                Clip = null,
                Volume = SetVolume,
                CrossFadeTime = CrossfadeDuration,
                LowPassFrequency = SetLowPassFrequency
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
            BGMCommand musicCommand = new BGMCommand
            {
                ControlType = EffectControlType,
                Clip = currentClip,
                Volume = restoreVolume,
                CrossFadeTime = CrossfadeDuration,
                LowPassFrequency = restoreFrequency
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
        }
    }
}