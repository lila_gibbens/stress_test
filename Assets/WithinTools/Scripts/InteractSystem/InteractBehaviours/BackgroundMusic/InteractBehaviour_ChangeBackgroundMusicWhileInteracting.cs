﻿using System.Collections;
using UnityEngine;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_ChangeBackgroundMusicWhileInteracting : SBInteractBehaviour
    {
        [Tooltip("The music audio clip to be played.")]
        public AudioClip MusicClip;

        [Tooltip("Apply crossfade effect")]
        public bool CrossfadeMusic = true;
        
        [Tooltip("Duration of the crossfade")]
        public float CrossfadeDuration = BackgroundMusicPlayer.DEFAULT_FADE_DURATION;
        
        [Tooltip("The target volume of the music after the crossfade")]
        [Range(0, 1)] 
        public float SetVolume = 1f;

        private float restoreVolume;
        private AudioClip currentClip;

        public override void OnBehaviourStarted(Hashtable args)
        {
            restoreVolume = BackgroundMusicPlayer.Instance.GetBackgroundMusicVolume();
            currentClip = BackgroundMusicPlayer.Instance.GetBackgroundMusicClip();

            BGMCommand musicCommand = new BGMCommand
            {
                ControlType = MusicEffectControlType.PlayMusic,
                Clip = MusicClip,
                Volume = SetVolume,
                CrossFadeTime = CrossfadeDuration
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
            BGMCommand musicCommand = new BGMCommand
            {
                ControlType = MusicEffectControlType.ResumeMusic,
                Clip = currentClip,
                Volume = restoreVolume,
                CrossFadeTime = CrossfadeDuration
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
        }
    }
}