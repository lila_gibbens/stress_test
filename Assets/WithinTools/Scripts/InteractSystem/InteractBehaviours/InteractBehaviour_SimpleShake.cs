﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_SimpleShake : SBInteractBehaviour
    {
        [Tooltip("The target object to be shaken by the behaviour.")]
        public Transform transformToShake;

        [Tooltip("Duration of the shake in seconds.")]
        public float Duration = 1;
        
        [Tooltip("The strength of the effect. You might want to tweak this value depending on your desired effect.")]
        public float Power = 0.25f;
        
        [Tooltip("How random is the deformation of the effect. You might want to tweak this value depending on your desired effect.")]
        public int Randomess = 35;
        
        [Tooltip("How much it vibrates. You might want to tweak this value depending on your desired effect.")]
        public float Vibrato = 34;

        private Vector3 originalScale;
        private float maxPower;

        private void Start()
        {
            originalScale = new Vector3(transformToShake.localScale.x, transformToShake.localScale.y, transformToShake.localScale.z);
        }

        public override void OnBehaviourStarted(Hashtable args)
        {
            //-- if it's a drag, etc interaction, leave the shake for the end
            if (args.ContainsKey(EInteractParamType.OriginalPosition))
            {
                return;
            }

            if (args.ContainsKey(EInteractParamType.Direction) && args.ContainsKey(EInteractParamType.Power))
            {
                float swipePower = (float)args[EInteractParamType.Power];
                DoPowerShake(swipePower);
                return;
            }

            if (args.ContainsKey(EInteractParamType.Power))
            {
                maxPower = (float)args[EInteractParamType.Power];
                return;
            }

            transformToShake.DOShakeScale(Duration, Power, Randomess, Vibrato, false);
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
            if (args.ContainsKey(EInteractParamType.CurrentPosition))
            {
                Vector2 originalDragPos = (Vector2)args[EInteractParamType.OriginalPosition];
                Vector2 currentDragPos = (Vector2)args[EInteractParamType.CurrentPosition];

                float dragDistance = Vector2.Distance(originalDragPos, currentDragPos);
                float maxDistance = Screen.height / 2; //-- just a guessed max drag power

                float lerpAlpha = dragDistance / maxDistance;

                Vector3 minScale = originalScale / 3; //--- just a guessed minimal scale for dragging

                Vector3 currentScale = Vector3.Lerp(originalScale, minScale, lerpAlpha);
                transformToShake.localScale = currentScale;
            }
            else if (args.ContainsKey(EInteractParamType.Power))
            {
                maxPower = Mathf.Max(maxPower, (float)args[EInteractParamType.Power]);
            }
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
            if (args.ContainsKey(EInteractParamType.OriginalPosition))
            {
                Vector2 originalDragPos = (Vector2)args[EInteractParamType.OriginalPosition];
                Vector2 currentDragPos = (Vector2)args[EInteractParamType.CurrentPosition];
                float dragDistance = Vector2.Distance(originalDragPos, currentDragPos);
                float maxDistance = Screen.height / 2; //-- just a guessed max drag power

                float lerpDrag = dragDistance / maxDistance;

                float dragPower = lerpDrag * Power;

                DoPowerShake(dragPower);
            }
            else if (args.ContainsKey(EInteractParamType.Power))
            {
                DoPowerShake(maxPower);
            }
        }

        private void DoPowerShake(float power)
        {
            transformToShake.localScale = originalScale;
            transformToShake.DOKill();
            transformToShake.DOShakeScale(Duration, Power * power, Randomess, Vibrato, true);
        }
    }
}