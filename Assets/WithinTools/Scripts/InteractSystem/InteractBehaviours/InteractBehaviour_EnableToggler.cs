﻿using System.Collections;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_EnableToggler : SBInteractBehaviour
    {
        public override void OnBehaviourStarted(Hashtable args)
        {
            gameObject.SetActive(!gameObject.activeSelf);
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
        }
    }
}