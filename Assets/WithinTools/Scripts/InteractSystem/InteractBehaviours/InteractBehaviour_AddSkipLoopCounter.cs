﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_AddSkipLoopCounter : SBInteractBehaviour
    {
        [SerializeField]
        private int skipCountersToAdd = 1;
        
        public override void OnBehaviourStarted(Hashtable args)
        {
            TimelineFlowControl.Instance.AddSkipCounters(skipCountersToAdd);
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
        }
    }
}