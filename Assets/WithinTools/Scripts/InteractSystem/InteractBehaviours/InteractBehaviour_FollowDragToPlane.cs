﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_FollowDragToPlane : SBInteractBehaviour
    {
        [Tooltip("From 0 to 1. It's the speed of the following object behind the drag. 0 is no movement, 1 is instant follow (like a standard drag).")]
        public float LerpSpeed;

        [Tooltip("The plane that limits the movement.")]
        public GameObject planeObject;

        private float originalY;

        public override void OnBehaviourStarted(Hashtable args)
        {
            originalY = transform.position.y;
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
            if (planeObject == null)
            {
                return;
            }

            Vector2 currentDragPos = (Vector2)args[EInteractParamType.CurrentPosition];
            
            Ray ray = Camera.main.ScreenPointToRay(currentDragPos);
            
            RaycastHit[] hits;
            hits = Physics.RaycastAll(ray.origin, ray.direction, 10000);

            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].collider.gameObject != planeObject)
                {
                    continue;
                }
                
                RaycastHit hitInfo = hits[i];
                
                Vector3 targetPosition = new Vector3(hitInfo.point.x, originalY, hitInfo.point.z);
                Vector3 newPosition = Vector3.Slerp(transform.position, targetPosition, LerpSpeed);
                newPosition.y = originalY;

                transform.position = newPosition;
            }
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
        }
    }
}