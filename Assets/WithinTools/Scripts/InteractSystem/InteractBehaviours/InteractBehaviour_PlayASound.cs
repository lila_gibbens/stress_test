﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_PlayASound : SBInteractBehaviour
    {
        [Tooltip("OPTIONAL: A unique id for this sound to be used by the AudioPlayer. It can be left empty.")]
        public string InstanceID;
        
        [Tooltip("The AudioClip you want to play. If it's null, it will throw an Exception in runtime. ")]
        public AudioClip ClipToPlay;

        [Tooltip("The volume to play the clip at.")]
        public float Volume = 1;

        [Tooltip(" Delay before playing the sound. This is particulary useful if you are using Music Duck to allow the music to lower before playing this sound.")]
        public float DelayOnPlaying = 0;
        
        [Tooltip("If true, it will enable the Unity spatial audio system for this sound when played. The AudioSource will be added as a child GameObject of the GameObject containing this InteractBehaviour, and will be removed once the sound finishes playing.")]
        public bool Spatialize = false;
        
        [Tooltip("If using Spatialize, the distance from the camera to the object in which the volume will be the max.")]
        public float SpatializeMinDistance = 20;

        [Tooltip("If using Spatialize, the distance from the camera to the object in which the volume will be the min (no sound).")]
        public float SpatializeMaxDistance = 100;
        
        [Tooltip("If true, will play when the triggering Interaction Starts.")]
        public bool PlayOnStartInteraction = true;
        
        [Tooltip("If true, will play when the triggering Interaction Ends.")]
        public bool PlayOnEndsInteraction = false;

        [Tooltip("If true, this can be triggered multiple times and the sounds will overlap. If false, this can be played again only once the current playing sound finishes.")]
        public bool AllowMultiple = false;

        [Tooltip("If true, this will call duck any Background Music that is currently playing. When all the sounds that made the BGM to duck finish, the music will go back to the original volume.")]
        public bool DoMusicDuck = false;

        [Tooltip("Time that the Music Duck Transition should take to be done.")]
        public float MusicDuckTime = 1f;
        
        public override void OnBehaviourStarted(Hashtable args)
        {
            if (PlayOnStartInteraction)
            {
                PlaySound();
            }
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
            if (PlayOnEndsInteraction)
            {
                PlaySound();
            }
        }

        private void PlaySound()
        {
            AudioSourceOptions options = new AudioSourceOptions();
            options.AllowMultiple = AllowMultiple;
            options.Spatialize = Spatialize;
            options.Volume = Volume;
            options.ClipToPlay = ClipToPlay;
            options.SpatializeMaxDistance = SpatializeMaxDistance;
            options.SpatializeMinDistance = SpatializeMinDistance;
            options.DelayOnPlaying = DelayOnPlaying;
            options.DoMusicDuck = DoMusicDuck;
            options.MusicDuckTime = MusicDuckTime;
            options.TargetSpatialGameObject = gameObject;
            
            InstanceID = String.IsNullOrEmpty(InstanceID) ? ClipToPlay.name : InstanceID;
            
            GlobalAudioPlayer.Instance.PlayAudio(InstanceID, options);
        }
    }
}