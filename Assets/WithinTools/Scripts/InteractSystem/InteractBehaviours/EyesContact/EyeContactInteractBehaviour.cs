﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom
{
	public class EyeContactInteractBehaviour : SBInteractBehaviour
	{
		[Tooltip("The EyeContactCharacter we want to affect.")]
		[SerializeField] 
		private EyeContactCharacter eyeCharacter;
		
		[Tooltip("if true, the character will start looking at target, if false, the charcter will stop looking and his head/eyes will go back to the center position.")] 
		public bool ShouldLook;

		[Tooltip("if true, the character will follow the camera with his/her sight. If false, will look at the TargetLookObject instead.")]
		[SerializeField] 
		private bool lookToCamera = true;
		
		[Tooltip("If LookToCamera is false, the character will follow this referenced gameobject.")]
		[SerializeField] 
		private GameObject targetLookObject;

		[Tooltip("Behaviour will be triggered when the interaction calls OnStart")]
		public bool TriggerOnStartInteraction;
		
		[Tooltip("Behaviour will be triggered when the interaction calls OnFinish")]
		public bool TriggerOnFinishInteraction;
		
		private void LookTo()
		{
			GameObject targetGameObject = lookToCamera ? Camera.main.gameObject : targetLookObject;
			eyeCharacter.SetLooking(ShouldLook, targetGameObject);
		}

		public override void OnBehaviourStarted(Hashtable args)
		{
			if (TriggerOnStartInteraction)
			{
				LookTo();	
			}
		}

		public override void OnBehaviourUpdate(Hashtable args)
		{
		}

		public override void OnBehaviourFinished(Hashtable args)
		{
			if (TriggerOnFinishInteraction)
			{
				LookTo();	
			}
		}
	}
}