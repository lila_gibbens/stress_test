﻿using System;
using CrazyMinnow.SALSA;
using UnityEngine;

namespace Within.SuperBloom
{
	public class EyeContactCharacter : MonoBehaviour
	{
		[Tooltip("For the eyes movement we are using RandomEyes, wich is included in SALSA. Drag the randomeyes3D reference in here.")]
		[SerializeField]
		private RandomEyes3D randomEyes;

		[Tooltip("This is the parent object of the character. Since we added the script to the character's game object, it would be itself in this case.")]
		[SerializeField] 
		private Transform rootTransform;
		
		[Tooltip("This is a reference to the character's Mecanim Animator. Just drag the character with the Animator in here.")]
		[SerializeField] 
		private Animator animatorController;
		
		[HideInInspector]
		public GameObject targetLookObject;

		[Tooltip("Drag here the transform of the head bone in your character. The script will rotate that transform when moving the head.")]
		[SerializeField] 
		private Transform headBoneTransform;

		[Tooltip("How fast the head will turn when following an object. 150 is the default value.")]
		[SerializeField]
		private float headTurningSpeed = 150;

		[Tooltip("Use body rotation when head max angle is reached.")]
		[SerializeField] 
		private bool useBodyRotation;
		
		[Tooltip("How fast the body will turn when following an object. 50 is the default value. You might want it to be slower than the head, but also you might need to tune up with value if your turning animation doesn't look properly with the actual movement.")]
		[SerializeField]
		private float bodyTurningSpeed = 50;

		[Tooltip("How far the head joint can turn in degrees from looking straight.")]
		[SerializeField] 
		private float maxNeckAppertureAngle = 75;
		
		[NonSerialized]
		public bool isFollowingTarget = false;

		private Quaternion originalHeadRotation;
		
		private Quaternion previousHeadRotation;

		private Quaternion previousBodyRotation;

		private bool isBodyRotating;

		private bool hasLooked;
		
		private void OnEnable()
		{
			isBodyRotating = false;
			
			previousHeadRotation = new Quaternion(headBoneTransform.rotation.x, headBoneTransform.rotation.y, headBoneTransform.rotation.z, headBoneTransform.rotation.w);

			previousBodyRotation = new Quaternion(rootTransform.rotation.x, rootTransform.rotation.y, rootTransform.rotation.z, rootTransform.rotation.w);
		}

		public void SetLooking(bool isLooking, GameObject targetGameObject)
		{
			isFollowingTarget = isLooking;

			if (isLooking)
			{
				hasLooked = true;
				targetLookObject = targetGameObject;
				originalHeadRotation = new Quaternion(headBoneTransform.localRotation.x, headBoneTransform.localRotation.y, headBoneTransform.localRotation.z, headBoneTransform.localRotation.w);
				previousBodyRotation = new Quaternion(rootTransform.rotation.x, rootTransform.rotation.y, rootTransform.rotation.z, rootTransform.rotation.w);
			}
		}

		private void LateUpdate()
		{
			bool isLookingToSomething = isFollowingTarget && targetLookObject != null;
			
			if (isLookingToSomething)
			{
				Vector3 lookDirection = headBoneTransform.InverseTransformPoint(targetLookObject.transform.position - headBoneTransform.localPosition).normalized;
				Quaternion currentRotation = Quaternion.RotateTowards(previousHeadRotation, Quaternion.LookRotation(lookDirection), Time.deltaTime * headTurningSpeed);
				headBoneTransform.localRotation = currentRotation;
				previousHeadRotation = new Quaternion(headBoneTransform.localRotation.x, headBoneTransform.localRotation.y, headBoneTransform.localRotation.z, headBoneTransform.localRotation.w);
			}
			else if (hasLooked)
			{
				Quaternion currentRotation = Quaternion.RotateTowards(previousHeadRotation, originalHeadRotation, Time.deltaTime * headTurningSpeed);
				headBoneTransform.localRotation = currentRotation;
				previousHeadRotation = new Quaternion(headBoneTransform.localRotation.x, headBoneTransform.localRotation.y, headBoneTransform.localRotation.z, headBoneTransform.localRotation.w);
			}
			else
			{
				return;
			}

			Vector3 localNeckEuler = headBoneTransform.localEulerAngles;

			if (localNeckEuler.y > maxNeckAppertureAngle && localNeckEuler.y < 180)
			{
				isBodyRotating = true;
				localNeckEuler.y = maxNeckAppertureAngle;
			}
			else if (localNeckEuler.y < 360 - maxNeckAppertureAngle && localNeckEuler.y >= 180)
			{
				isBodyRotating = true;
				localNeckEuler.y = 360 - maxNeckAppertureAngle;
			}
			
			isBodyRotating = isBodyRotating && isLookingToSomething && useBodyRotation;			

			if (isBodyRotating)
			{
				Vector3 lookDirection = (targetLookObject.transform.position - rootTransform.position).normalized;
				
				rootTransform.rotation = Quaternion.RotateTowards(previousBodyRotation, Quaternion.LookRotation(lookDirection), Time.deltaTime * bodyTurningSpeed);
				rootTransform.eulerAngles = new Vector3(0, rootTransform.eulerAngles.y, 0);

				float angleBodyObject = Quaternion.Angle(previousBodyRotation, rootTransform.rotation);

				isBodyRotating = angleBodyObject > 0;

				bool rotatingRight = Vector3.Dot(lookDirection, rootTransform.right) > 0; 
				
				animatorController.SetFloat("TurningValue", rotatingRight ? 1 : -1);
			}
			else
			{
				animatorController.SetFloat("TurningValue", 0);
			}
			
			headBoneTransform.localEulerAngles = localNeckEuler;
			previousHeadRotation = new Quaternion(headBoneTransform.localRotation.x, headBoneTransform.localRotation.y, headBoneTransform.localRotation.z, headBoneTransform.localRotation.w);
			previousBodyRotation = new Quaternion(rootTransform.rotation.x, rootTransform.rotation.y, rootTransform.rotation.z, rootTransform.rotation.w);
		}
	}
}