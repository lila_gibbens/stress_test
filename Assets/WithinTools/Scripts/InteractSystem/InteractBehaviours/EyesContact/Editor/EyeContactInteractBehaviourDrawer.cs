﻿using UnityEditor;
using UnityEngine;

namespace Within.SuperBloom
{
	[CustomPropertyDrawer(typeof(EyeContactInteractBehaviour))]
	public class EyeContactInteractBehaviourDrawer : PropertyDrawer
	{
		private int fieldCount = 1;
        
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return fieldCount * EditorGUIUtility.singleLineHeight;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			SerializedProperty doLook = property.FindPropertyRelative("ShouldLook");
			SerializedProperty lookToCamera = property.FindPropertyRelative("LookToCamera");

			Rect singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);

			fieldCount = 1;
            
			EditorGUI.PropertyField(singleFieldRect, doLook);

			if (!doLook.boolValue)
			{
				return;
			}

			fieldCount++;
            
			singleFieldRect.y += EditorGUIUtility.singleLineHeight;
            
			EditorGUI.PropertyField(singleFieldRect, lookToCamera);
		}
	}
}