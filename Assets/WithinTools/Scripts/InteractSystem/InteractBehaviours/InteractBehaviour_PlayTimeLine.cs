﻿using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_PlayTimeLine : SBInteractBehaviour
    {
        public enum EPlayType
        {
            WaitUntilFinish, AllowRestart
        }

        [Tooltip("he target timeline to play when triggered.")]
        public PlayableDirector timelineDirector;

        [Tooltip("* Wait Until Finish: Block this timeline to being played from start until the current execution finishes.\n" +  
                 "Allow Restart: Allows this to play from start the timeline even if the current execution hasn't finished yet.")]
        public EPlayType playType;

        [Tooltip("An array of InteractBehaviours that are triggered when the timeline finishes.")]
        public SBInteractBehaviour[] BehavioursToTriggerOnFinish;

        private bool isPlaying;

        private Hashtable receivingArgs;

        public override void OnBehaviourStarted(Hashtable args)
        {
            if (playType == EPlayType.WaitUntilFinish && isPlaying)
            {
                return;
            }

            isPlaying = true;

            receivingArgs = args;

            timelineDirector.Stop();
            timelineDirector.Play();

            Invoke("OnFinish", (float)timelineDirector.duration);
        }

        private void OnFinish()
        {
            foreach (var behav in BehavioursToTriggerOnFinish)
            {
                behav.OnBehaviourStarted(receivingArgs);
            }

            isPlaying = false;
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
        }
    }
}