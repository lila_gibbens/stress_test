﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_AnimatorState : SBInteractBehaviour
    {
        // TODO: Option to pass through power to float parameter
        // TODO: Custom inspector to select parameters
        [SerializeField] private bool onStartInteraction = true;
        [SerializeField] private bool onEndsInteraction = false;
        
        [Tooltip("The Animator Controller you want to modify the state by triggering a state or changing a parameter value.")]
        [SerializeField] private Animator animator;
        
        [Tooltip("The parameters you want to change and their new values. For triggering a Trigger Parameter, just leave the Value empty.")]
        [SerializeField] private AnimatorStateController.AnimatorParameter[] parametersToSet;

        private AnimatorStateController stateController;

        private void OnValidate()
        {
            Assert.IsNotNull(animator);
        }

        private void Awake()
        {
            if (stateController == null)
            {
                Initialize();
            }
        }

        private void Initialize()
        {
            stateController = new AnimatorStateController(animator, parametersToSet);
        }
        
        public override void OnBehaviourStarted(Hashtable args)
        {
            if (onStartInteraction)
            {
                stateController.Run(true);
            }
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
            if (onEndsInteraction)
            {
                stateController.Run(true);
            }
        }
    }
}