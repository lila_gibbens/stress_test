﻿using System;
using System.Linq;
using UnityEngine;

namespace Within.SuperBloom
{
    [Serializable]
    public class AnimatorStateController
    {
        [Serializable]
        public struct AnimatorParameter
        {
            public string name;
            public string value;
        }
        
        [SerializeField] private Animator animator;
        [SerializeField] private AnimatorParameter[] parametersToSet;
        
        private AnimatorControllerParameter[] parameters;
        private int[] animationHashes;
        public bool Initialized { get; private set; }
        
        public AnimatorStateController(Animator animator, AnimatorParameter[] parametersToSet)
        {
            if (parametersToSet == null || animator == null)
            {
                return;
            }
            
            this.animator = animator;
            this.parametersToSet = parametersToSet;

            parameters = new AnimatorControllerParameter[parametersToSet.Length];
            animationHashes = new int[parametersToSet.Length];
            for (int i = 0; i < parametersToSet.Length; ++i)
            {
                string paramName = parametersToSet[i].name;
                parameters[i] = animator.parameters.First(p => p.name == paramName);
                animationHashes[i] = Animator.StringToHash(paramName);
            }

            Initialized = true;
        }

        public void Run(bool runTriggers)
        {
            if (parametersToSet == null || animator == null)
            {
                return;
            }
            
            for (int i = 0; i < parametersToSet.Length; ++i)
            {
                AnimatorControllerParameter param = parameters[i];
                if (param != null)
                {
                    string value = parametersToSet[i].value;
                    switch (param.type)
                    {
                        case AnimatorControllerParameterType.Bool:
                            animator.SetBool(animationHashes[i], bool.Parse(value));
                            break;
                        case AnimatorControllerParameterType.Float:
                            animator.SetFloat(animationHashes[i], float.Parse(value));
                            break;
                        case AnimatorControllerParameterType.Int:
                            animator.SetInteger(animationHashes[i], int.Parse(value));
                            break;
                        case AnimatorControllerParameterType.Trigger:
                            if (runTriggers)
                            {
                                animator.SetTrigger(animationHashes[i]);
                            }
                            break;
                    }
                }
            }
        }
    }
}