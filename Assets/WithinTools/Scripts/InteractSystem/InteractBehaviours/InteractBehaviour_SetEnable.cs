﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_SetEnable : SBInteractBehaviour
    {
        [Tooltip("The new enable/disable state of the gameobject")]
        public bool NewEnableState;

        private bool actionRequested;
        
        public override void OnBehaviourStarted(Hashtable args)
        {
            if (actionRequested)
            {
                return;
            }
            actionRequested = true;
            StartCoroutine(this.ActionDelayer());
        }

        private IEnumerator ActionDelayer()
        {
            yield return new WaitForEndOfFrame();
            gameObject.SetActive(NewEnableState);
            actionRequested = false;
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
        }
    }
}