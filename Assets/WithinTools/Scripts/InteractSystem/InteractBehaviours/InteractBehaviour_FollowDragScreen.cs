﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_FollowDragScreen : SBInteractBehaviour
    {
        [Tooltip("From 0 to 1. It's the speed of the following object behind the drag. 0 is no movement, 1 is instant follow (like a standard drag).")]
        public float LerpSpeed;

        private Vector3 originalPosition;

        private float distanceToCamera;

        public override void OnBehaviourStarted(Hashtable args)
        {
            originalPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);

            distanceToCamera = Vector3.Distance(originalPosition, Camera.main.transform.position);
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
            if (!args.Contains(EInteractParamType.CurrentPosition))
            {
                return;
            }

            Vector2 currentDragPos = (Vector2)args[EInteractParamType.CurrentPosition];

            Vector3 newWorldDragPosition = Camera.main.ScreenToWorldPoint(
                new Vector3(currentDragPos.x, currentDragPos.y, distanceToCamera));

            Vector3 newPosition = Vector3.Slerp(transform.position, newWorldDragPosition, LerpSpeed);
            transform.position = newPosition;
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
        }
    }
}