﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_LogText : SBInteractBehaviour
    {
        [Tooltip("Text to show in the console.")]
        public string TextToLog;

        [Tooltip("It shows all the parameters received from the Interactable that triggers this Behaviour. Useful to know what's calling this and for development purposes.")]
        public bool LogAllParameters = true;

        public override void OnBehaviourStarted(Hashtable args)
        {
            LogStage("<STARTED>", args);
        }

        public override void OnBehaviourUpdate(Hashtable args)
        {
            LogStage("<UPDATE>", args);
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
            LogStage("<FINISHED>", args);
        }

        private void LogStage(string stage, Hashtable args)
        {
            Debug.Log(TextToLog);

            if (LogAllParameters)
            {
                string argsToLog = stage + " Args: ";

                foreach (var key in args.Keys)
                {
                    argsToLog += string.Format("[{0}]: {1} -", key, args[key].ToString());
                }
                Debug.Log(argsToLog);
            }
        }
    }
}