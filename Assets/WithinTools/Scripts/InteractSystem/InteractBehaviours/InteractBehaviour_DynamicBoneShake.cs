﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Within.SuperBloom.InteractBehaviours
{
    public class InteractBehaviour_DynamicBoneShake : SBInteractBehaviour
    {
        [Tooltip("The target object to be shaken by the behaviour.")]
        [SerializeField]
        private Rigidbody rigibodyToShake;

        [Tooltip("Reference of the DynamicBone script that will vibrate")]
        [SerializeField]
        private DynamicBone dynamicBone;
        
        [Tooltip("The strength of the effect. You might want to tweak this value depending on your desired effect.")]
        public float Power = 15f;

        [Tooltip("How much time it takes to go back to rest position")]
        public float RestingTime = 5;

        private float restTimer = 0;
        
        public override void OnBehaviourUpdate(Hashtable args)
        {
            if (args.ContainsKey(EInteractParamType.HitInfo))
            {
                RaycastHit hitInfo = (RaycastHit)args[EInteractParamType.HitInfo];
                Vector2 screenTouchPos = (Vector2)args[EInteractParamType.ScreenPosition];
                Vector2 swipeSpeed = (Vector2)args[EInteractParamType.Speed];
                
                rigibodyToShake.isKinematic = false;

                Vector3 currentWorldPosition = hitInfo.point;
                
                Vector3 currentCameraProjection = Camera.main.WorldToScreenPoint(currentWorldPosition);

                Vector2 previousTouchPos = screenTouchPos - swipeSpeed;  
                    
                Vector3 previousWorldPosition = Camera.main.ScreenToWorldPoint(new Vector3(previousTouchPos.x, previousTouchPos.y, currentCameraProjection.z));

                Vector3 worldSpeedForce = currentWorldPosition - previousWorldPosition;

                rigibodyToShake.AddForceAtPosition(worldSpeedForce * Power * Time.deltaTime, currentWorldPosition, ForceMode.Impulse);
                
                dynamicBone.SetWeight(1);

                rigibodyToShake.angularDrag = 0;
                
                restTimer = RestingTime;
            }
        }

        public void Update()
        {
            if (restTimer <= 0)
            {
                return;
            }

            restTimer -= Time.deltaTime;
            
            rigibodyToShake.angularDrag += Time.deltaTime * (40 / RestingTime);

            dynamicBone.SetWeight(restTimer / RestingTime);
            
            if (restTimer <= 0)
            {
                rigibodyToShake.isKinematic = true;
            }
        }

        public override void OnBehaviourStarted(Hashtable args)
        {
        }

        public override void OnBehaviourFinished(Hashtable args)
        {
        }
    }
}