﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom
{
    public abstract class SBInteractBehaviour : MonoBehaviour
    {
        public abstract void OnBehaviourStarted(Hashtable args);
        public abstract void OnBehaviourUpdate(Hashtable args);
        public abstract void OnBehaviourFinished(Hashtable args);

        public void OnBehaviourStarted()
        {
            OnBehaviourStarted(new Hashtable());
        }

        public void OnBehaviourUpdate()
        {
            OnBehaviourUpdate(new Hashtable());
        }

        public void OnBehaviourFinished()
        {
            OnBehaviourFinished(new Hashtable());
        }
    }
}