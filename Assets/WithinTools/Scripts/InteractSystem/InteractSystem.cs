﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom
{
    public enum EInteractionDirection
    {
        Up, Down, Left, Right
    }

    public enum EInteractParamType
    {
        Direction, 
        Power,                  // Value between 0 and 1
        OriginalPosition, 
        CurrentPosition, 
        HitInfo,
        ScreenPosition,
        Speed
    }
    
    public static class InteractSystem
    {
        public static Hashtable ParamsHash(params object[] args)
        {
            Hashtable hashTable = new Hashtable(args.Length / 2);
            if (args.Length % 2 != 0)
            {
                Debug.LogError("[Hash Error] Hash requires an even number of arguments!");
                return null;
            }
            else
            {
                int i = 0;
                while (i < args.Length - 1)
                {
                    hashTable.Add(args[i], args[i + 1]);
                    i += 2;
                }
                return hashTable;
            }
        }
    }
}