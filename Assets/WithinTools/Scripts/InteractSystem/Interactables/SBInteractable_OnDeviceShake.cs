﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom
{
    public class SBInteractable_OnDeviceShake : SBInteractable
    {
        [Tooltip("The acceleration magnitude required to register a shake action")]
        [SerializeField] private float shakeMagnitude = 2;

        [Tooltip("The larger this value, the smaller the power output will be")]
        [SerializeField] private float magnitudePowerSoftener = 15.0f;

        private float shakeMagnitudeSqr;
        private bool shaking = false;

        protected override void OnAwake()
        {
            shakeMagnitudeSqr = shakeMagnitude * shakeMagnitude;
        }

        private void OnValidate()
        {
            shakeMagnitudeSqr = shakeMagnitude * shakeMagnitude;
        }

        private void OnEnable()
        {
            shaking = false;
        }

        private void Update()
        {
            Hashtable args = new Hashtable()
            {
                { EInteractParamType.Power, Mathf.Clamp01(Input.acceleration.sqrMagnitude / magnitudePowerSoftener) }
            };

            if (Input.acceleration.sqrMagnitude > shakeMagnitudeSqr)
            {
                if (!shaking)
                {
                    OnInteractionStarted(args);
                    shaking = true;
                }
                else
                {
                    OnInteractionUpdate(args);
                }
            }
            else if (shaking)
            {
                OnInteractionFinished(args);
                shaking = false;
            }
        }
    }
}
