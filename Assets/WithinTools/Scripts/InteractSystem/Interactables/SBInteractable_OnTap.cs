﻿namespace Within.SuperBloom
{
    public class SBInteractable_OnTap : SBInteractable
    {
        protected override void OnAwake()
        {
        }

        private void OnMouseDown()
        {
            OnInteractionStarted();
        }

        private void OnMouseUpAsButton()
        {
            OnInteractionFinished();
        }
    }
}