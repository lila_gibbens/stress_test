﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom
{
    public class SBInteractable_OnDrag : SBInteractable
    {
        private Vector2 originalDragPoint;
        private Vector2 currentDragPoint;
        private bool isDraggin;

        protected override void OnAwake()
        {
        }

        private void BeginDrag()
        {
            isDraggin = true;
            originalDragPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            currentDragPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            Hashtable args = new Hashtable
            {
                { EInteractParamType.OriginalPosition, currentDragPoint },
                { EInteractParamType.CurrentPosition, currentDragPoint }
            };

            OnInteractionStarted(args);
        }

        private void StopDrag()
        {
            isDraggin = false;

            Hashtable args = new Hashtable
            {
                { EInteractParamType.OriginalPosition, originalDragPoint },
                { EInteractParamType.CurrentPosition, currentDragPoint }
            };

            OnInteractionFinished(args);
        }

        private void Update()
        {
#if UNITY_EDITOR
            UpdateOnDesktop();
#else
            UpdateOnMobile();
#endif

            if (isDraggin)
            {
                Hashtable args = new Hashtable
                {
                    { EInteractParamType.OriginalPosition, originalDragPoint },
                    { EInteractParamType.CurrentPosition, currentDragPoint }
                };

                OnInteractionUpdate(args);
            }
        }

        private void UpdateOnMobile()
        {
            if (Input.touchCount == 0)
            {
                return;
            }

            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                RaycastHit hitInfo;
                GameObject target = null;
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

                if (Physics.Raycast(ray.origin, ray.direction * 10000, out hitInfo))
                {
                    target = hitInfo.collider.gameObject;
                }

                if (gameObject == target)
                {
                    BeginDrag();
                }
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                if (isDraggin)
                {
                    StopDrag();
                }
            }

            if (isDraggin)
            {
                UpdateDrag();
            }
        }

        private void UpdateOnDesktop()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hitInfo;
                GameObject target = null;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray.origin, ray.direction * 10000, out hitInfo))
                {
                    target = hitInfo.collider.gameObject;
                }

                if (gameObject == target)
                {
                    BeginDrag();
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (isDraggin)
                {
                    StopDrag();
                }
            }

            if (isDraggin)
            {
                UpdateDrag();
            }
        }

        private void UpdateDrag()
        {
            currentDragPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
    }
}