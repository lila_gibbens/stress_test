﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom
{
    public class SBInteractable_EnterCameraView : SBInteractable
    {
        public float DelayToActivate;

        public bool CancelDelayIfExit = true;

        private FollowingObject[] followingObjects;

        private bool inited = false;
        
        private class FollowingObject
        {
            private SBInteractable_EnterCameraView parentInteractable;
            private SBInteractBehaviour[] behaviours;
            private Renderer[] renderers;
            private bool isVisible;
            private bool wasVisible;

            public void Init(GameObject go, SBInteractable_EnterCameraView parent)
            {
                parentInteractable = parent;
                wasVisible = false;
                isVisible = false;
                renderers = go.GetComponentsInChildren<Renderer>();
                behaviours = go.GetComponents<SBInteractBehaviour>();
            }

            public void CheckVisibility(Plane[] planes, bool initialize = false)
            {
                isVisible = false;

                for (int i = 0; i < renderers.Length; i++)
                {
                    Renderer rend = renderers[i];

                    if (rend == null)
                    {
                        continue;
                    }

                    isVisible = isVisible || GeometryUtility.TestPlanesAABB(planes, rend.bounds);
                    if (isVisible)
                    {
                        break;
                    }
                }

                if (!initialize)
                {
                    if (!wasVisible && isVisible)
                    {
                        OnObjectEnteredView();
                    }    
                }

                wasVisible = isVisible;
            }

            private void OnObjectEnteredView()
            {
                parentInteractable.StartCoroutine(TriggerBehaviours());
            }

            private IEnumerator TriggerBehaviours()
            {
                float remainingTime = parentInteractable.DelayToActivate;

                while (true)
                {
                    yield return new WaitForEndOfFrame();

                    remainingTime -= Time.deltaTime;
                    if (!isVisible && parentInteractable.CancelDelayIfExit)
                    {
                        yield break;
                    }
                    if (remainingTime <= 0)
                    {
                        break;
                    }
                }

                foreach (var behav in behaviours)
                {
                    Hashtable args = new Hashtable();
                    behav.OnBehaviourStarted(args);
                }
            }
        }

        protected override void OnAwake()
        {
        }

        private void OnEnable()
        {
            StartCoroutine(Initialize());
        }
        
        private void OnDisable()
        {
            inited = false;
        }

        private IEnumerator Initialize()
        {
            yield return new WaitUntil(() => StorySequencer.Instance != null);
            yield return new WaitUntil(() => StorySequencer.Instance.IsStoryInProgress);
            
            followingObjects = new FollowingObject[TargetObjects.Length];

            for (int i = 0; i < followingObjects.Length; i++)
            {
                followingObjects[i] = new FollowingObject();
                followingObjects[i].Init(TargetObjects[i], this);
                
                Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
                followingObjects[i].CheckVisibility(planes, true);
            }

            inited = true;
        }

        private void Update()
        {
            if (!inited)
            {
                return;
            }

            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

            foreach (FollowingObject obj in followingObjects)
            {
                obj.CheckVisibility(planes);
            }
        }
    }
}