﻿using UnityEngine;

namespace Within.SuperBloom
{
    public class SBInteractable_OnSwipe : SBInteractable
    {
        public float SWIPE_TIME_LIMIT = 0.25f;
        
        private Vector2 fingerDown;
        private Vector2 fingerUp;
        public bool detectSwipeOnlyAfterRelease = false;

        public float SWIPE_THRESHOLD = 20f;

        private float initialSwapTime;
        
        protected override void OnAwake()
        {
        }

        private void Update()
        {
#if UNITY_EDITOR
            UpdateOnDesktop();
#else
            UpdateOnMobile();
#endif
        }

        internal void UpdateOnDesktop()
        {
            if (Input.GetMouseButtonDown(0))
            {
                fingerUp = Input.mousePosition;
                fingerDown = Input.mousePosition;
                initialSwapTime = Time.time;
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (!detectSwipeOnlyAfterRelease)
                {
                    fingerDown = Input.mousePosition;
                    CheckSwipe();
                }
            }

            // Detects swipe after finger is released
            if (Input.GetMouseButtonUp(0))
            {
                fingerDown = Input.mousePosition;
                CheckSwipe();
            }
        }

        internal void UpdateOnMobile()
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    fingerUp = touch.position;
                    fingerDown = touch.position;
                    initialSwapTime = Time.time;
                }

                // Detects Swipe while finger is still moving
                if (touch.phase == TouchPhase.Moved)
                {
                    if (!detectSwipeOnlyAfterRelease)
                    {
                        fingerDown = touch.position;
                        CheckSwipe();
                    }
                }

                // Detects swipe after finger is released
                if (touch.phase == TouchPhase.Ended)
                {
                    fingerDown = touch.position;
                    CheckSwipe();
                }
            }
        }

        internal void CheckSwipe()
        {
            if (Time.time - initialSwapTime > SWIPE_TIME_LIMIT)
            {
                return;
            }

            if (VerticalMove() > SWIPE_THRESHOLD && VerticalMove() > HorizontalValMove())
            {
                if (fingerDown.y - fingerUp.y > 0) // up swipe
                {
                    OnSwipe(EInteractionDirection.Up);
                }
                else if (fingerDown.y - fingerUp.y < 0) // Down swipe
                {
                    OnSwipe(EInteractionDirection.Down);
                }
                fingerUp = fingerDown;
            }
            else if (HorizontalValMove() > SWIPE_THRESHOLD && HorizontalValMove() > VerticalMove())
            {
                if (fingerDown.x - fingerUp.x > 0) // Right swipe
                {
                    OnSwipe(EInteractionDirection.Right);
                }
                else if (fingerDown.x - fingerUp.x < 0) // Left swipe
                {
                    OnSwipe(EInteractionDirection.Left);
                }
                fingerUp = fingerDown;
            }
        }

        internal float VerticalMove()
        {
            return Mathf.Abs(fingerDown.y - fingerUp.y);
        }

        internal float HorizontalValMove()
        {
            return Mathf.Abs(fingerDown.x - fingerUp.x);
        }

        internal void OnSwipe(EInteractionDirection interactionDirection)
        {
            float power = (fingerUp - fingerDown).magnitude / 1000.0f;
            power = Mathf.Clamp01(power);

            OnInteractionStarted(InteractSystem.ParamsHash(
                EInteractParamType.Direction, interactionDirection,
                EInteractParamType.Power, power));
        }
    }
}