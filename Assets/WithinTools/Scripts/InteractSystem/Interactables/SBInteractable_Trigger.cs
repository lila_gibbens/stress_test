﻿using UnityEngine;

namespace Within.SuperBloom
{
    public class SBInteractable_Trigger : SBInteractable
    {
        [SerializeField] private Collider trigger;
        
        protected override void OnAwake()
        {
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other == trigger)
            {
                OnInteractionStarted();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other == trigger)
            {
                OnInteractionFinished();
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (other == trigger)
            {
                OnInteractionUpdate();
            }
        }
    }
}