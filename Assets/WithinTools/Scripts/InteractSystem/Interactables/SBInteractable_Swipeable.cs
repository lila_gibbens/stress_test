﻿using System.Collections;
using UnityEngine;

namespace Within.SuperBloom
{
    public class SBInteractable_Swipeable : SBInteractable
    {
        protected override void OnAwake()
        {
        }

        public void SetSwipeInfo(Vector2 screenPosition, RaycastHit hitInfo, Vector2 swipeSpeed)
        {
            Hashtable args = new Hashtable();
            args.Add(EInteractParamType.ScreenPosition, screenPosition);
            args.Add(EInteractParamType.HitInfo, hitInfo);
            args.Add(EInteractParamType.Speed, swipeSpeed);
            args.Add(EInteractParamType.Power, Mathf.Clamp01(swipeSpeed.magnitude));
            OnInteractionUpdate(args);
        }
    }
}