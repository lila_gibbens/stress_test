﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Within.SuperBloom
{
    public class SBInteractable_CameraDistance : SBInteractable
    {
        public float DistanceToEnterRange;

        public bool TriggerOnEnterRange = true;
        public bool TriggerOnExitRange = false;

        [Tooltip("If enabled, behaviours will be triggered when the interactable is enabled if the camera is in range (or out of range)")]
        [SerializeField] private bool triggerOnEnable = true;

        private Transform cameraTransform;

        private List<Transform> transformsOutside;
        private List<Transform> transformsInside;

        private Queue<Transform> removeFromOutside;
        private Queue<Transform> removeFromInside;

        private bool inited;
        
        private float distanceToEnterRangeSrq;
        
        protected override void OnAwake()
        {
        }

        private void OnEnable()
        {
            StartCoroutine(InitializeBehaviour());
        }
        
        private void OnDisable()
        {
            inited = false;
        }
        
        private IEnumerator InitializeBehaviour()
        {
            yield return new WaitUntil(() => StorySequencer.Instance != null);
            yield return new WaitUntil(() => StorySequencer.Instance.IsStoryInProgress);
            
            cameraTransform = Camera.main.transform;

            transformsOutside = new List<Transform>();
            transformsInside = new List<Transform>();

            removeFromOutside = new Queue<Transform>();
            removeFromInside = new Queue<Transform>();
            
            UpdateSqrDistance();
            InitDistance();
            
            inited = true;
        }
        
        private void OnValidate()
        {
            // This allows the user to edit values while in play mode in the editor.
            UpdateSqrDistance();
        }

        private void UpdateSqrDistance()
        {
            distanceToEnterRangeSrq = Mathf.Pow(DistanceToEnterRange, 2);
        }

        private void InitDistance()
        {
            SequencerFlowEvents.onStoryStarted.RemoveListener(InitDistance);

            foreach (GameObject go in TargetObjects)
            {
                float tDistance = (go.transform.position - cameraTransform.position).sqrMagnitude;

                if (tDistance >= distanceToEnterRangeSrq)
                {
                    transformsOutside.Add(go.transform);
                    if (triggerOnEnable && TriggerOnExitRange)
                    {
                        TriggerForTransform(go.transform, EInteractionDirection.Up);
                    }
                }
                else
                {
                    transformsInside.Add(go.transform);
                    if (triggerOnEnable && TriggerOnEnterRange)
                    {
                        TriggerForTransform(go.transform, EInteractionDirection.Down);
                    }
                }
            }
        }

        private void Update()
        {
            if (!inited)
            {
                return;
            }
            
            foreach (Transform t in transformsOutside)
            {
                float tDistance = (t.position - cameraTransform.position).sqrMagnitude;

                if (tDistance < distanceToEnterRangeSrq)
                {
                    removeFromOutside.Enqueue(t);
                    if (TriggerOnEnterRange)
                    {
                        TriggerForTransform(t, EInteractionDirection.Down);
                    }
                }
            }

            foreach (Transform t in transformsInside)
            {
                float tDistance = (t.position - cameraTransform.position).sqrMagnitude;

                if (tDistance > distanceToEnterRangeSrq)
                {
                    removeFromInside.Enqueue(t);
                    if (TriggerOnExitRange)
                    {
                        TriggerForTransform(t, EInteractionDirection.Up);
                    }
                }
            }

            while (removeFromOutside.Count > 0)
            {
                Transform t = removeFromOutside.Dequeue();
                transformsOutside.Remove(t);
                transformsInside.Add(t);
            }

            while (removeFromInside.Count > 0)
            {
                Transform t = removeFromInside.Dequeue();
                transformsInside.Remove(t);
                transformsOutside.Add(t);
            }
        }

        private void TriggerForTransform(Transform t, EInteractionDirection direction)
        {
            SBInteractBehaviour[] behavs = t.GetComponents<SBInteractBehaviour>();

            foreach (var beh in behavs)
            {
                Hashtable args = new Hashtable
                {
                    { EInteractParamType.Direction, direction }
                };

                beh.OnBehaviourStarted(args);
            }
        }
    }
}