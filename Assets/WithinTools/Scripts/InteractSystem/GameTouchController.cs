﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Within.SuperBloom
{
	public class GameTouchController : MonoBehaviour
	{
		private Vector2 lastTouchPosition;
		private Vector2 currentTouchPosition;
		
		private bool pressing;

		private void Awake()
		{
			pressing = false;
			lastTouchPosition = Vector2.zero;
			currentTouchPosition = Vector2.zero;
		}

		private void Update()
		{
#if UNITY_EDITOR
			UpdateOnDesktop();
#else
            UpdateOnMobile();
#endif
			CheckSwipe();
		}

		private void UpdateOnDesktop()
		{
			pressing = false;
			
			if (Input.GetMouseButton(0))
			{
				lastTouchPosition = new Vector2(currentTouchPosition.x, currentTouchPosition.y);
				currentTouchPosition = Input.mousePosition;
				pressing = true;
			}
		}

		private void UpdateOnMobile()
		{
			pressing = false;
			
			if (Input.touches.Length > 0)
			{
				lastTouchPosition = new Vector2(currentTouchPosition.x, currentTouchPosition.y);
				currentTouchPosition = Input.touches[0].position;
				pressing = true;
			}
		}

		private void CheckSwipe()
		{
			if (!pressing)
			{
				return;
			}
			
			Ray ray = Camera.main.ScreenPointToRay(currentTouchPosition);

			RaycastHit[] hitInfos = Physics.RaycastAll(ray.origin, ray.direction, 10000);

			Vector2 swipeSpeed = currentTouchPosition - lastTouchPosition;
			
			foreach (var hitInfo in hitInfos)
			{
				GameObject target = hitInfo.collider.gameObject;
				
				if (target != null)
				{
					SBInteractable_Swipeable[] allSwipeables = target.GetComponents<SBInteractable_Swipeable>();
					
					foreach (var swipeable in allSwipeables)
					{
						swipeable.SetSwipeInfo(currentTouchPosition, hitInfo, swipeSpeed);	
					}
				}
			}
		}
	}
}
