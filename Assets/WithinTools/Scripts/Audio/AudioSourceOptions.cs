﻿using UnityEngine;

namespace Within.SuperBloom.Audio
{
    public class AudioSourceOptions
    {
        public AudioClip ClipToPlay;

        public float Volume;

        public float DelayOnPlaying;
        
        public bool Spatialize;

        public GameObject TargetSpatialGameObject;
        
        public float SpatializeMinDistance;

        public float SpatializeMaxDistance;

        public bool AllowMultiple;

        public bool DoMusicDuck;

        public float MusicDuckTime;

        public AudioSourceOptions()
        {
            ClipToPlay = null;
            Volume = 1;
            DelayOnPlaying = 0;
            Spatialize = false;
            SpatializeMinDistance = 20;
            SpatializeMaxDistance = 100;
            AllowMultiple = false;
            DoMusicDuck = false;
            MusicDuckTime = 1;
        }
    }
}