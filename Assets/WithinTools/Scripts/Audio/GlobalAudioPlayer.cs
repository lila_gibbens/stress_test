﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom
{
	public class GlobalAudioPlayer : MonoBehaviour 
	{
		private static GlobalAudioPlayer instance;
	
		public static GlobalAudioPlayer Instance
		{
			get
			{
				if (instance == null)
				{
					GameObject audioContainer = new GameObject("GlobalAudioPlayer");
	
					instance = audioContainer.AddComponent<GlobalAudioPlayer>();
						
					instance.audiosByID = new Dictionary<string, List<AudioObject>>();
				}
					
				return instance;
			}
		}
	
		private Dictionary<string, List<AudioObject>> audiosByID;
		
		public void PlayAudio(string audioID, AudioSourceOptions optionsToPlay)
		{
			if (!optionsToPlay.AllowMultiple && audiosByID.ContainsKey(audioID) && audiosByID[audioID].Count > 0)
			{
				return;
			}
			
			AudioObject audioObject = new AudioObject(audioID, optionsToPlay);
			
			if (!audiosByID.ContainsKey(audioID))
			{
				audiosByID.Add(audioID, new List<AudioObject>());
			}
			
			audiosByID[audioID].Add(audioObject);
			
			if (audioObject.Options.DoMusicDuck)
			{
				BGMCommand musicCommand = new BGMCommand()
				{
					ControlType = MusicEffectControlType.StartMusicDuck,
					Volume = 0.5f,
					CrossFadeTime = audioObject.Options.MusicDuckTime,
					LowPassFrequency = 4000
				};
				BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
			}
	
			StartCoroutine(PlaySoundRoutine(audioObject));
		}
	
		private IEnumerator PlaySoundRoutine(AudioObject audioToPlay)
		{
			yield return new WaitForSeconds(audioToPlay.Options.DelayOnPlaying);
	
			audioToPlay.PlayingAudioSource.Play();
			audioToPlay.HasStarted = true;
		}
	
		private void Update()
		{
			if (audiosByID.Keys.Count == 0)
			{
				return;
			}
	
			bool stopDuckingMusic = true;
	
			foreach (var kvp in audiosByID)
			{
				for (int i = kvp.Value.Count - 1; i >= 0; i--)
				{
					AudioObject audioObject = kvp.Value[i];
	
					if (!audioObject.PlayingAudioSource.isPlaying && audioObject.HasStarted)
					{
						Destroy(audioObject.ContainerGameObject);
						kvp.Value.RemoveAt(i);
						audioObject = null; //-- skip GC
					}
					else
					{
						if (audioObject.Options.DoMusicDuck)
						{
							stopDuckingMusic = false;
						}
					}
				}
			}
			
			audiosByID = audiosByID
				.Where(f => f.Value.Count > 0)
				.ToDictionary(x => x.Key, x => x.Value);
	
			if (stopDuckingMusic)
			{
				BGMCommand musicCommand = new BGMCommand()
				{
					ControlType = MusicEffectControlType.EndMusicDuck,
					Volume = 1,
					CrossFadeTime = 1f,
					LowPassFrequency = 5000
				};
				BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
			}
		}
	}
}