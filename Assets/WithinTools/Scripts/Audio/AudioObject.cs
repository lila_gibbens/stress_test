﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Within.SuperBloom.Audio
{
    public class AudioObject
    {
        public AudioSourceOptions Options;
        public AudioSource PlayingAudioSource;
        public GameObject ContainerGameObject;
        public bool HasStarted;
        
        public AudioObject(string audioID, AudioSourceOptions opts)
        {
            HasStarted = false;
            
            Options = opts;
            
            ContainerGameObject = new GameObject("AudioContainer_" + audioID);
            
            PlayingAudioSource = ContainerGameObject.AddComponent<AudioSource>();
            
            PlayingAudioSource.clip = Options.ClipToPlay;
            PlayingAudioSource.volume = Options.Volume;
		
            if (Options.Spatialize)
            {
                Assert.IsNotNull(Options.TargetSpatialGameObject);
                ContainerGameObject.transform.SetParent(Options.TargetSpatialGameObject.transform);
                ContainerGameObject.transform.localPosition = Vector3.zero;

                PlayingAudioSource.spatialize = true;
                PlayingAudioSource.spatialBlend = 1;
                PlayingAudioSource.minDistance = Options.SpatializeMinDistance;
                PlayingAudioSource.maxDistance = Options.SpatializeMaxDistance;
            }
            else
            {
                ContainerGameObject.transform.SetParent(GlobalAudioPlayer.Instance.transform);
                ContainerGameObject.transform.localPosition = Vector3.zero;
            }
        }
    }
}