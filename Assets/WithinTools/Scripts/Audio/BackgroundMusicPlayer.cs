﻿ using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;

namespace Within.SuperBloom.Audio
{
    public enum MusicEffectControlType
    {
        SetVolume,
        PauseMusic,
        ResumeMusic,
        StartMusicDuck,
        EndMusicDuck,
        PlayMusic,
        StopMusic
    }

    public class BGMCommand
    {
        public MusicEffectControlType ControlType;

        public AudioClip Clip;
        public float Volume = 1f;
        public float CrossFadeTime = 0;
        public float LowPassFrequency = 0;
    }

    public class BackgroundMusicPlayer : MonoBehaviour
    {
        public const float DEFAULT_FADE_DURATION = 0.2f;
        public const float DEFAULT_LOWPASS_FREQUENCY = 2400f;
        public const float MAX_LOWPASS_FREQUENCY = 22000f;
        private const int SAFETY_DELAY_FRAMES = 5;
        
        private static BackgroundMusicPlayer instance;
        public static BackgroundMusicPlayer Instance
        {
            get
            {
                if (instance == null)
                {
                    CreateInstance();
                }
                return instance;
            }
        }
        
        private AudioSource bgmSource;
        private AudioSource bgmCrossfadeSource;
        private AudioLowPassFilter lowPassFilter;

        private List<BGMCommand> commandBuffer;

        // We have a delay between a command is requested and it's executed to allow running coroutines to break before executing the new command
        private int commandDelayFrames;

        private bool IsNewCommandInBuffer
        {
            get { return commandDelayFrames > 0 && commandBuffer.Count > 0; }
        }
        
        private static void CreateInstance()
        {
            GameObject objectToRemove = GameObject.Find("BackgroundMusicPlayer");
            if (objectToRemove != null)
            {
                GameObject.DestroyImmediate(objectToRemove);
            }

            instance = new GameObject("BackgroundMusicPlayer").AddComponent<BackgroundMusicPlayer>();
            instance.Initialize();
        }

        private void Initialize()
        {
            if (bgmSource == null)
            {
                bgmSource = gameObject.AddComponent<AudioSource>();
                bgmSource.loop = true;
                bgmSource.playOnAwake = false;
            }

            if (bgmCrossfadeSource == null)
            {
                bgmCrossfadeSource = gameObject.AddComponent<AudioSource>();
                bgmCrossfadeSource.loop = true;
                bgmCrossfadeSource.playOnAwake = false;
                bgmCrossfadeSource.volume = 0f;
            }
            
            commandBuffer = new List<BGMCommand>();
            
            InitLowPassFilter();
            
            DontDestroyOnLoad(gameObject);
        }

        private void InitLowPassFilter()
        {
            lowPassFilter = bgmSource.gameObject.GetComponent<AudioLowPassFilter>();
            if (lowPassFilter == null)
            {
                lowPassFilter = bgmSource.gameObject.AddComponent<AudioLowPassFilter>();
            }
            
            lowPassFilter.cutoffFrequency = MAX_LOWPASS_FREQUENCY;
        }

        private void OnDestroy()
        {
            if (instance.gameObject == this.gameObject)
            {
                instance = null;
            }
        }

        public void DoCommand(BGMCommand bgmCommand)
        {
            commandBuffer.Add(bgmCommand);
        }

        private void LateUpdate()
        {
            if (commandBuffer.Count == 0)
            {
                return;
            }

            if (commandDelayFrames > 0)
            {
                commandDelayFrames--;
                return;
            }
            
            commandDelayFrames = SAFETY_DELAY_FRAMES;

            bool musicChanged = false;
            bool volumeChanged = false;
            bool freqChanged = false;
            
            AudioClip resultingClip = null;
            float resultingVolume = 1f;
            float resultingFrequency = MAX_LOWPASS_FREQUENCY;
            float resultingFadeTimePlay = 0f;
            float resultingFadeTimeVolume = 0f;
            float resultingFadeTimeDuck = 0f;
            bool musicStopped = false;
            
            for (int i = 0; i < commandBuffer.Count; i++)
            {
                BGMCommand commandToDo = commandBuffer[i];

                if (commandToDo.ControlType == MusicEffectControlType.PlayMusic)
                {
                    resultingClip = commandToDo.Clip;
                    resultingVolume = commandToDo.Volume;
                    resultingFadeTimePlay = commandToDo.CrossFadeTime;
                    resultingFrequency = MAX_LOWPASS_FREQUENCY;
                    musicChanged = true;
                    volumeChanged = false;
                    freqChanged = false;
                    musicStopped = false;
                }
                else if (commandToDo.ControlType == MusicEffectControlType.StopMusic)
                {
                    resultingClip = null;
                    musicChanged = true;
                    musicStopped = true;
                }
                else if (commandToDo.ControlType == MusicEffectControlType.SetVolume)
                {
                    resultingVolume = commandToDo.Volume;
                    resultingFadeTimeVolume = commandToDo.CrossFadeTime;
                    volumeChanged = true;
                }
                else if (commandToDo.ControlType == MusicEffectControlType.StartMusicDuck)
                {
                    resultingFrequency = commandToDo.LowPassFrequency;
                    resultingFadeTimeDuck = commandToDo.CrossFadeTime;
                    freqChanged = true;
                }
                else if (commandToDo.ControlType == MusicEffectControlType.EndMusicDuck)
                {
                    resultingFrequency = MAX_LOWPASS_FREQUENCY;
                    resultingFadeTimeDuck = commandToDo.CrossFadeTime;
                    freqChanged = true;
                }
            }
            
            commandBuffer.Clear();
            
            if (musicStopped)
            {
                ForceStopBackgroundMusic();
            }
            else
            {
                float totalFadeTime = 0;

                if (musicChanged)
                {
                    totalFadeTime = resultingFadeTimePlay;
                }
                if (volumeChanged)
                {
                    totalFadeTime = Mathf.Max(totalFadeTime, resultingFadeTimeVolume);
                }
                if (freqChanged)
                {
                    totalFadeTime = Mathf.Max(totalFadeTime, resultingFadeTimeDuck);
                }

                PlayBackgroundMusic(resultingClip, resultingVolume, totalFadeTime);
                SetBackgroundMusicVolume(resultingVolume, totalFadeTime);
                StartDuckBackgroundMusic(resultingFrequency, totalFadeTime);
            }
        }

        private IEnumerator FadeBackgroundMusicVolume(float fadeToVolume, float fadeDuration, bool playFromBeginning = false)
        {
            float fadeFromVolume = bgmSource.volume;
            float elapsed = 0f;
            float t = 0f;

            if (playFromBeginning)
            {
                bgmSource.Stop();
                bgmSource.Play();
            }

            if (fadeDuration > 0)
            {
                while (t < 1f)
                {
                    elapsed += Time.deltaTime;
                    t = Mathf.Clamp01(elapsed / fadeDuration);
                    
                    if (IsNewCommandInBuffer)
                    {
                        yield break;
                    }
                    
                    bgmSource.volume = Mathf.Lerp(fadeFromVolume, fadeToVolume, t);
                    
                    yield return null;
                }
            }
        }
        
        private IEnumerator LerpToFilterFrequency(AudioLowPassFilter filter, float frequency, float fadeDuration)
        {
            float startFrequency = filter.cutoffFrequency;
            float elapsed = 0f;
            float t = 0f;

            if (fadeDuration > 0)
            {
                while (t <= 1f)
                {
                    elapsed += Time.deltaTime;
                    t = Mathf.Clamp01(elapsed / fadeDuration);
                    if (IsNewCommandInBuffer)
                    {
                        yield break;
                    }
                    
                    filter.cutoffFrequency = Mathf.Lerp(startFrequency, frequency, t);
                    
                    yield return null;
                }
            }
        }

        private IEnumerator CrossfadeBackgroundMusicClips(AudioClip clipIn, float fadeToVolume, float fadeDuration, bool playFromBeginning)
        {
            // initialize the track to crossfade to
            bgmCrossfadeSource.clip = clipIn;
            bgmCrossfadeSource.volume = 0f;

            if (!bgmCrossfadeSource.isPlaying)
            {
                bgmCrossfadeSource.Play();
            }
            if (playFromBeginning)
            {
                bgmCrossfadeSource.time = 0f;
            }

            // swap source references (this ensures that the new clip immediately becomes the 'bgmSource')
            AudioSource newBgmSource = bgmCrossfadeSource;
            AudioSource newCrossfadeSource = bgmSource;
            bgmSource = newBgmSource;
            bgmCrossfadeSource = newCrossfadeSource;

            float fadeFromVolume = bgmCrossfadeSource.volume;
            float elapsed = 0f;
            float t = 0f;

            // fade new crossfade source to 0 and new bgm source to desired volume
            if (fadeDuration > 0)
            {
                while (t <= 1f)
                {
                    elapsed += Time.deltaTime;
                    t = Mathf.Clamp01(elapsed / fadeDuration);
                    
                    if (IsNewCommandInBuffer)
                    {
                        yield break;
                    }
                    
                    bgmSource.volume = Mathf.Lerp(0f, fadeToVolume, t);
                    bgmCrossfadeSource.volume = Mathf.Lerp(fadeFromVolume, 0f, t);
                    
                    yield return null;
                }
            }
            bgmCrossfadeSource.Pause();
        }

        private void PlayBackgroundMusic(AudioClip clip, float volume, float fadeDuration = DEFAULT_FADE_DURATION)
        {
            if (clip != null && clip != bgmSource.clip)
            {
                StartCoroutine(CrossfadeBackgroundMusicClips(clip, volume, fadeDuration, true));
                
                if (!bgmSource.isPlaying)
                {
                    bgmSource.Play();
                }
            }
        }
        
        public AudioClip GetBackgroundMusicClip()
        {
            return bgmSource.clip;
        }

        private void ForceStopBackgroundMusic()
        {
            bgmSource.clip = null;
            bgmSource.Stop();
            bgmCrossfadeSource.clip = null;
            bgmCrossfadeSource.Stop();
        }
        
        private void SetBackgroundMusicVolume(float volume, float fadeDuration = DEFAULT_FADE_DURATION)
        {
            StartCoroutine(FadeBackgroundMusicVolume(volume, fadeDuration));
        }
        
        public float GetBackgroundMusicVolume()
        {
            return bgmSource.volume;
        }

        private void StartDuckBackgroundMusic(float lowPassFrequency, float fadeDuration = DEFAULT_FADE_DURATION)
        {
            StartCoroutine(LerpToFilterFrequency(lowPassFilter, lowPassFrequency, fadeDuration));
        }

        public float GetDuckFrequency()
        {
            return lowPassFilter.cutoffFrequency;
        }
    }
}