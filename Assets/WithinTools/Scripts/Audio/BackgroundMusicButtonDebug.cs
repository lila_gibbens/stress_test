﻿using System.Collections.Generic;
using UnityEngine;

namespace Within.SuperBloom.Audio
{
    public class BackgroundMusicButtonDebug : MonoBehaviour
    {
        [SerializeField] private List<AudioClip> bgmTracks;

        [SerializeField] private int selectedTrackIndex = 0;

        public void PlayBgmTrack()
        {
            BGMCommand musicCommand = new BGMCommand()
            {
                ControlType = MusicEffectControlType.PlayMusic,
                Clip = bgmTracks[selectedTrackIndex],
                Volume = 0.5f
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
        }
        
        public void ToggleDuckBgmTrack(float freq)
        {
            if (!(BackgroundMusicPlayer.Instance.GetDuckFrequency() < 22000f))
            {
                BGMCommand musicCommand = new BGMCommand()
                {
                    ControlType = MusicEffectControlType.StartMusicDuck,
                    LowPassFrequency = freq
                };
                BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
            }
            else
            {
                BGMCommand musicCommand = new BGMCommand()
                {
                    ControlType = MusicEffectControlType.StopMusic
                };
                BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
            }
        }

        public void SelectAdjacentTrack(bool previous)
        {
            selectedTrackIndex += previous ? (bgmTracks.Count - 1) : 1;
            selectedTrackIndex %= bgmTracks.Count;
        }
    }
}