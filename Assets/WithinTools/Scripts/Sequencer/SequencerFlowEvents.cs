﻿using UnityEngine.Events;

namespace Within.SuperBloom
{	
	public static class SequencerFlowEvents
	{
		public static readonly UnityEvent onStoryStarted = new UnityEvent();
		public static readonly UnityEvent onStoryFinished = new UnityEvent();
		
		public static readonly UnityIntEvent onWordsRead = new UnityIntEvent();
		
		public static readonly UnityEvent pageStarted = new UnityEvent();
		public static readonly UnityEvent pageCompleted = new UnityEvent();
		
		public static readonly UnityEvent passageStarted = new UnityEvent();
		
		public static readonly UnityStringEvent passagePartStarted = new UnityStringEvent();
		public static readonly UnityEvent passagePartCompleted = new UnityEvent();
		
		public static readonly UnityEvent onPassageTextFadedOut = new UnityEvent();
		
		public static readonly UnityEvent passageCompleted = new UnityEvent();
		
		public static readonly UnityIntEvent onTriggerWordRead = new UnityIntEvent();
	
		public static readonly UnityEvent sequenceStarted = new UnityEvent();
		public static readonly UnityEvent sequenceCompleted = new UnityEvent();

		public static readonly UnityBoolEvent storyVisibilityChanged = new UnityBoolEvent();
	}
	
	public class UnityBoolEvent : UnityEvent<bool> { }
	public class UnityIntEvent : UnityEvent<int> { }
	public class UnityStringEvent : UnityEvent<string> { }
}
