﻿using CrazyMinnow.SALSA;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

namespace Within.SuperBloom
{
    public class StoryVoiceNarrator : MonoBehaviour
    {
        private static StoryVoiceNarrator instance;

        public static StoryVoiceNarrator Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject go = new GameObject("StoryVoiceNarrator");
                    instance = go.AddComponent<StoryVoiceNarrator>();
                    instance.ac_narrator = go.AddComponent<AudioSource>();
                    instance.ac_narrator.playOnAwake = false;
                    instance.ac_narrator.spatialize = false;
                    instance.ac_narrator.Stop();
                }

                return instance;
            }
        }

        private AudioSource ac_narrator;

        private CM_MicInput mcInput;
        
        public AudioSource NarratorAudioSource
        {
            get { return ac_narrator; }
        }

        public UnityEvent OnNarrationComplete = new UnityEvent();

        private void OnDestroy()
        {
            instance = null;
        }

        public void PlayNarrationClip(AudioClip clip)
        {
            StopNarration();

            ac_narrator.clip = clip;
            ac_narrator.Play();
        }

        public void StopNarration()
        {
            if (ac_narrator.isPlaying)
            {
                ac_narrator.Stop();
            }
        }

        public void InitializeMic()
        {
            if (StoryModeOptions.playMode == EStoryPlayMode.ListenPrerecorded)
            {
                return;
            }

            if (mcInput == null)
            {
                AudioMixer mixer = Resources.Load<AudioMixer>("SalsaMicAudioMixer") as AudioMixer;
                ac_narrator.outputAudioMixerGroup = mixer.FindMatchingGroups("SalsaMic")[0];
            
                mcInput = gameObject.AddComponent<CM_MicInput>();
                mcInput.isAutoStart = false;
                mcInput.audioSrc = ac_narrator;    
            }

            if (Microphone.devices.Length == 0)
            {
                Debug.LogWarning("[StoryVoiceNarrator] Not Microphone device found to use in Lipsync");
                return;
            }

            string micName = Microphone.devices[0];
            mcInput.StartMicrophone(micName);
        }

        public void StartMicNarration()
        {
            // Syncing the audio source time to the Microphone time so that
            // lip syncing is not delayed
            ac_narrator.timeSamples = Microphone.GetPosition(mcInput.selectedMic);
            ac_narrator.Play();
        }

        public void StopMicNarration()
        {
            ac_narrator.Stop();
        }
	}
}