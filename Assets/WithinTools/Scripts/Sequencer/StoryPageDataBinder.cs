﻿using UnityEngine;

namespace Within.SuperBloom
{
    [DisallowMultipleComponent]
    public class StoryPageDataBinder : MonoBehaviour
    {
        [SerializeField]
        private StoryPageData pageData;

        /// <summary>
        /// Use This only in Editor Tools! 
        /// </summary>
        /// <param name="newData"></param>
        public void SetData(StoryPageData newData)
        {
              pageData = newData;
        }

        public StoryPageData CloneData()
        {
            return pageData.GetClone();
        }
    }
}