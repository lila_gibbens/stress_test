﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Within.SuperBloom
{
    public class ExperienceSequencerUI : MonoBehaviour
    {
        [SerializeField]
        private Button btn_ReadOneWord;
        
        [SerializeField]
        private Button btn_SkipPassage;

        [SerializeField]
        private Button btn_FinishPassage;

        [SerializeField]
        private Button btn_NextPage;

        [SerializeField]
        private Button btn_PrevPage;

        public GameObject container_Passage;

        public PassageText passage_Text;

        public GameObject container_PrePlayOptions;
        
        public GameObject container_StoryFinished;

        private bool showPassage = false;
        private bool storyVisible = false;
        
        private void OnEnable()
        {
            showPassage = false;
            UpdatePassageVisibility();
            container_PrePlayOptions.SetActive(false);
            container_StoryFinished.SetActive(false);

            SequencerFlowEvents.passageStarted.AddListener(OnPassageStart);
            SequencerFlowEvents.passagePartStarted.AddListener(OnPassagePartStart);
            SequencerFlowEvents.passagePartCompleted.AddListener(OnPassagePartCompleted);
            SequencerFlowEvents.passageCompleted.AddListener(OnPassageCompleted);
            SequencerFlowEvents.storyVisibilityChanged.AddListener(OnStoryVisibilityChanged);
            SequencerFlowEvents.onStoryFinished.AddListener(OnStoryFinished);

            btn_ReadOneWord.onClick.AddListener(OnClick_ReadOneWord);
            btn_SkipPassage.onClick.AddListener(OnClick_SkipPassage);
            btn_FinishPassage.onClick.AddListener(OnClick_FinishPassage);
            btn_NextPage.onClick.AddListener(OnClick_NextPage);
            btn_PrevPage.onClick.AddListener(OnClick_PreviousPage);

            SequencerControlDispatcher.OnReadyToStart.AddListener(OnStoryReadyToPlay);
        }

        private void OnStoryVisibilityChanged(bool visible)
        {
            storyVisible = visible;
            UpdatePassageVisibility();
        }

        private void OnDisable()
        {
            SequencerFlowEvents.passageStarted.RemoveListener(OnPassageStart);
            SequencerFlowEvents.passagePartStarted.RemoveListener(OnPassagePartStart);
            SequencerFlowEvents.passageCompleted.RemoveListener(OnPassageCompleted);
            SequencerFlowEvents.storyVisibilityChanged.RemoveListener(OnStoryVisibilityChanged);

            btn_ReadOneWord.onClick.RemoveAllListeners();
            btn_SkipPassage.onClick.RemoveAllListeners();
            btn_FinishPassage.onClick.RemoveListener(OnClick_FinishPassage);
            btn_NextPage.onClick.RemoveAllListeners();
            btn_PrevPage.onClick.RemoveAllListeners();

            SequencerControlDispatcher.OnReadyToStart.RemoveListener(OnStoryReadyToPlay);
        }

        private void OnStoryReadyToPlay()
        {
            if (!StorySequencer.Instance.IsStoryInProgress && StoryModeOptions.playMode == EStoryPlayMode.None)
            {
                container_PrePlayOptions.SetActive(true);
            }
            else
            {
                SequencerControlDispatcher.StartStory_Request.Invoke();
            }
        }

        private void OnPassageStart()
        {
            showPassage = true;
            UpdatePassageVisibility();
            
            btn_SkipPassage.gameObject.SetActive(false);

            //--- activate the Skip button for Read mode until we have a proper "on read" trigger		
            if (StoryModeOptions.playMode == EStoryPlayMode.ReadTheLines)
            {
#if UNITY_EDITOR                
                btn_ReadOneWord.gameObject.SetActive(true);
#else
                btn_ReadOneWord.gameObject.SetActive(false);
#endif
                btn_SkipPassage.gameObject.SetActive(true);
                btn_FinishPassage.gameObject.SetActive(false);
            }
            else if (StoryModeOptions.playMode == EStoryPlayMode.ListenPrerecorded)
            {
                btn_SkipPassage.gameObject.SetActive(false);
                btn_FinishPassage.gameObject.SetActive(false);
                btn_ReadOneWord.gameObject.SetActive(false);
                StoryVoiceNarrator.Instance.OnNarrationComplete.AddListener(OnNarrationCompleted);
            }
        }

        private void OnPassagePartStart(string passageText)
        {
            btn_ReadOneWord.gameObject.SetActive(true);
            passage_Text.Text = string.Format(passageText, (StorySequencer.Instance.PageIndex + 1));
        }

        private void OnPassagePartCompleted()
        {
            btn_ReadOneWord.gameObject.SetActive(false);
        }
        
        private void UpdatePassageVisibility()
        {
            container_Passage.SetActive(showPassage && storyVisible);
        }

        private void OnNarrationCompleted()
        {
            StoryVoiceNarrator.Instance.OnNarrationComplete.RemoveListener(OnNarrationCompleted);
            btn_FinishPassage.gameObject.SetActive(true);
        }
        
        /// <summary>
        /// When passage is complete, the passage ui must be hidden, but we need to wait for the Text Fade Out to Finish before 
        /// hidding it
        /// </summary>
        private void OnPassageCompleted()
        {
            btn_ReadOneWord.gameObject.SetActive(false);
            SequencerFlowEvents.onPassageTextFadedOut.RemoveListener(OnTextFadedOut);
            SequencerFlowEvents.onPassageTextFadedOut.AddListener(OnTextFadedOut);
        }

        private void OnTextFadedOut()
        {
            if (StoryModeOptions.playMode == EStoryPlayMode.ReadTheLines)
            {
                showPassage = false;
                UpdatePassageVisibility();
            }
        }

        private void OnStoryFinished()
        {
            container_StoryFinished.SetActive(true);
        }

        private void OnClick_ReadOneWord()
        {
            SequencerControlDispatcher.ReadOneWord_Request.Invoke();
        }

        private void OnClick_SkipPassage()
        {
            SequencerFlowEvents.passageCompleted.Invoke();
        }

        private void OnClick_FinishPassage()
        {
            showPassage = false;
            UpdatePassageVisibility();

            SequencerFlowEvents.passageCompleted.Invoke();
        }

        public void OnClick_NextPage()
        {
            SequencerControlDispatcher.NextPage.Invoke();
        }

        public void OnClick_PreviousPage()
        {
            SequencerControlDispatcher.PrevPage.Invoke();
        }
    }
}