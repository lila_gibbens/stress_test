﻿using UnityEngine;
using UnityEngine.UI;

namespace Within.SuperBloom
{
    public class SubMenuDebug_PlayModeUI : MonoBehaviour
    {
        [SerializeField]
        private Button btnPlayNormal;

        [SerializeField]
        private Button btnPlayPreRecorded;

        private void OnEnable()
        {
            btnPlayNormal.onClick.AddListener(OnClick_PlayNormal);
            btnPlayPreRecorded.onClick.AddListener(OnClick_PlayPreRecorded);
        }

        private void OnDisable()
        {
            btnPlayNormal.onClick.RemoveListener(OnClick_PlayNormal);
            btnPlayPreRecorded.onClick.RemoveListener(OnClick_PlayPreRecorded);
        }

        private void OnClick_PlayNormal()
        {
            gameObject.SetActive(false);

            StoryModeOptions.playMode = EStoryPlayMode.ReadTheLines;

            SequencerControlDispatcher.StartStory_Request.Invoke();
        }

        private void OnClick_PlayPreRecorded()
        {
            gameObject.SetActive(false);

            StoryModeOptions.playMode = EStoryPlayMode.ListenPrerecorded;

            SequencerControlDispatcher.StartStory_Request.Invoke();
        }
    }
}