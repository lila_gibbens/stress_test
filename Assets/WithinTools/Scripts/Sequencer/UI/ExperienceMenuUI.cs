﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Within.SuperBloom
{
	public class ExperienceMenuUI : MonoBehaviour
	{
		[SerializeField] 
		private Button btn_Menu;

		[SerializeField] 
		private Button btn_JumpForward;
		
		[SerializeField] 
		private Button btn_JumpBackward;
		
		[SerializeField] 
		private Button btn_ShowSoundPanel;
		
		[SerializeField] 
		private Button btn_ShowInfo;
		
		[SerializeField] 
		private Button btn_BackHome;

		[SerializeField] 
		private RectTransform panel_SubMenu;

		[SerializeField] 
		private RectTransform panel_SoundOptions;

#region Development

		[SerializeField] 
		private GameObject panel_Dev;
		[SerializeField] 
		private Button btn_ARRemote;

#endregion Development

		private bool subMenuTweening;

		private void OnEnable()
		{		
			subMenuTweening = false;
			panel_SubMenu.gameObject.SetActive(false);
			panel_Dev.gameObject.SetActive(false);
			
			btn_Menu.onClick.AddListener(OnClick_Menu);
			
			btn_JumpForward.onClick.AddListener(OnClick_JumpForward);
			btn_JumpBackward.onClick.AddListener(OnClick_JumpBackward);
			btn_ShowSoundPanel.onClick.AddListener(OnClick_SoundMenu);
			btn_ShowInfo.onClick.AddListener(OnClick_Info);
			btn_BackHome.onClick.AddListener(OnClick_BackHome);
			btn_ARRemote.onClick.AddListener(OnClick_ARRemote);
		}
		
		private void OnDisable()
		{		
			btn_Menu.onClick.RemoveListener(OnClick_Menu);
			
			btn_JumpForward.onClick.RemoveListener(OnClick_JumpForward);
			btn_JumpBackward.onClick.RemoveListener(OnClick_JumpBackward);
			btn_ShowSoundPanel.onClick.RemoveListener(OnClick_SoundMenu);
			btn_ShowInfo.onClick.RemoveListener(OnClick_Info);
			btn_BackHome.onClick.RemoveListener(OnClick_BackHome);
			btn_ARRemote.onClick.RemoveListener(OnClick_ARRemote);
		}
		
		private void OnClick_Menu()
		{
			if (subMenuTweening)
			{
				return;
			}

			bool isOpened = panel_SubMenu.gameObject.activeSelf;

			subMenuTweening = true;
			
			panel_SoundOptions.gameObject.SetActive(false);
			
			if (!isOpened)
			{		
				panel_SubMenu.gameObject.SetActive(true);
				panel_SubMenu.DOScaleY(1, 0.25f)
					.SetEase(Ease.OutCubic)
					.OnComplete(() => 
						{
							subMenuTweening = false;

							// Debug.isDebugBuild (development build) is always true in the editor and we don't want this button in the editor
							// AR Editor Remote only works on a device.
							if (Debug.isDebugBuild && !Application.isEditor)
							{
								panel_Dev.gameObject.SetActive(true);
							}
						});
			}
			else
			{
				panel_SubMenu.DOScaleY(0, 0.1f)
					.SetEase(Ease.OutCubic)
					.OnComplete(() => 
						{ 
							subMenuTweening = false;
							panel_SubMenu.gameObject.SetActive(false);

							if (Debug.isDebugBuild && !Application.isEditor)
							{
								panel_Dev.gameObject.SetActive(false);
							}
						});
			}
		}

		private void OnClick_JumpForward() { }

		private void OnClick_JumpBackward() { }
		
		private void OnClick_SoundMenu()
		{
			panel_SoundOptions.gameObject.SetActive(!panel_SoundOptions.gameObject.activeSelf);
		}

		private void OnClick_Info() { }

		private void OnClick_BackHome() { }

		private void OnClick_ARRemote() 
		{ 
			SceneManager.LoadScene(SBToolsPaths.AR_REMOTE_SCENE_NAME);
		}	
	}
}
