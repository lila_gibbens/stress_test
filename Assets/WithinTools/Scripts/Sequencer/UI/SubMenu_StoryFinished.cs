﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Within.SuperBloom
{
    public class SubMenu_StoryFinished : MonoBehaviour
    {
        [SerializeField]
        private Button btnPlayAgain;

        [SerializeField]
        private Button btnBackToHome;

        private void OnEnable()
        {
            btnPlayAgain.onClick.AddListener(OnClick_PlayAgain);
            btnBackToHome.onClick.AddListener(OnClick_BackToHome);

            btnBackToHome.gameObject.SetActive(StoryModeOptions.IsRunningFromBundle);
        }

        private void OnDisable()
        {
            btnPlayAgain.onClick.RemoveListener(OnClick_PlayAgain);
            btnBackToHome.onClick.RemoveListener(OnClick_BackToHome);
        }

        private void OnClick_PlayAgain()
        {
            gameObject.SetActive(false);

            SequencerControlDispatcher.RestartStory.Invoke();
        }

        private void OnClick_BackToHome()
        {
            gameObject.SetActive(false);

            StoryScenesLoader.Instance.LoadHomeScreen();
        }
    }
}