﻿using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using Within.SuperBloom;

namespace Within.SuperBloom
{
	public class UIPassageTextFader : MonoBehaviour
	{
		[SerializeField] private Transform mask_Transform;

		[SerializeField] private TextMeshProUGUI txtPassage;

		[SerializeField] private ParticleSystem prefabParticle;

		[SerializeField] private Transform leftOfMaskIndicator;
		
		[SerializeField] private Transform rightOfMaskIndicator;

		[SerializeField] private Camera uiCamera;

		[SerializeField] private float fadeTime = 1.0f;

		[SerializeField] private float fadeInFactor = 0.8f;

		private bool initialized = false;

		private bool isFadingOut;

		private ParticleSystem particlesObject;

		private Vector3 textOriginalPos;
		private Vector3 maskOriginalPos;

		private Vector3 textMeshLeftLimit;
		private Vector3 textMeshRightLimit;

		private void OnEnable()
		{
			if (!initialized)
			{
				textOriginalPos = new Vector3(txtPassage.transform.position.x, txtPassage.transform.position.y, txtPassage.transform.position.z);
				maskOriginalPos = new Vector3(mask_Transform.position.x, mask_Transform.position.y, mask_Transform.position.z);

				particlesObject = Instantiate(prefabParticle);
				particlesObject.transform.position = txtPassage.transform.position;
				particlesObject.Stop();

				initialized = true;
			}

			FadeInText();

			SequencerFlowEvents.passagePartStarted.AddListener(FadeInText);
			SequencerFlowEvents.passagePartCompleted.AddListener(DoFadeOutTween);
		}

		private void OnDisable()
		{
			SequencerFlowEvents.passagePartStarted.RemoveListener(FadeInText);
			SequencerFlowEvents.passagePartCompleted.RemoveListener(DoFadeOutTween);
		}

		private void FadeInText(string text = "")
		{
			mask_Transform.transform.position = new Vector3(maskOriginalPos.x, maskOriginalPos.y, maskOriginalPos.z);

			Color color = txtPassage.color;
			color.a = 0f;
			txtPassage.color = color;

			StartCoroutine(FadeInTextRoutine());
		}

		private IEnumerator FadeInTextRoutine()
		{
			while (txtPassage.color.a < 1)
			{
				Color color = txtPassage.color;

				float alpha = Mathf.Clamp01(color.a + fadeInFactor * Time.deltaTime);
				color.a = alpha;

				txtPassage.color = color;
				yield return new WaitForEndOfFrame();
			}

			Vector3 textMeshInCameraPos = uiCamera.WorldToScreenPoint(txtPassage.transform.position);

			textMeshInCameraPos.x -= txtPassage.mesh.bounds.extents.x;
			textMeshInCameraPos.y = txtPassage.mesh.bounds.extents.y;
			
			RectTransform maskRectTransform = mask_Transform.GetComponent<RectTransform>();
			Vector3[] maskWorldCorners = new Vector3[4];
			maskRectTransform.GetWorldCorners(maskWorldCorners);
			
			leftOfMaskIndicator.transform.position = maskWorldCorners[0];
			rightOfMaskIndicator.transform.position = maskWorldCorners[2];
			
			textMeshLeftLimit = uiCamera.ScreenToWorldPoint(textMeshInCameraPos);
			textMeshLeftLimit.y = maskWorldCorners[0].y + 0.5f;
			
			textMeshInCameraPos.x += txtPassage.mesh.bounds.extents.x * 2;
	
			textMeshRightLimit = uiCamera.ScreenToWorldPoint(textMeshInCameraPos);
			textMeshRightLimit.y = maskWorldCorners[0].y;
			
			particlesObject.transform.position = textMeshLeftLimit;

			particlesObject.Stop();
		}

		private void DoFadeOutTween()
		{
			RectTransform maskRectTransform = mask_Transform.GetComponent<RectTransform>();
			
			maskRectTransform.position = textMeshLeftLimit;

			float movementDiff = textMeshRightLimit.x - textMeshLeftLimit.x;
			float averageDistance = 8f;

			float fadeTimeProportional = (movementDiff / averageDistance) * fadeTime;
			
			maskRectTransform
				.DOMoveX(textMeshRightLimit.x, fadeTimeProportional)
				.OnComplete(OnComplete);

			particlesObject.transform.position = textMeshLeftLimit;

			isFadingOut = true;
		}

		private void LateUpdate()
		{
			txtPassage.transform.position = textOriginalPos;

			if (isFadingOut)
			{
				if (leftOfMaskIndicator.position.x >= textMeshLeftLimit.x && leftOfMaskIndicator.position.x < textMeshRightLimit.x)
				{
					if (!particlesObject.isPlaying)
					{
						particlesObject.Play();
					}

					particlesObject.transform.position = new Vector3(leftOfMaskIndicator.position.x, particlesObject.transform.position.y, particlesObject.transform.position.z);
				}
				else
				{
					if (particlesObject.isPlaying)
					{
						particlesObject.Stop();
					}
				}
			}
		}

		private void OnComplete()
		{
			isFadingOut = false;
			particlesObject.Stop();
			SequencerFlowEvents.onPassageTextFadedOut.Invoke();
		}
	}
}