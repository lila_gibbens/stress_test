﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Within.SuperBloom
{
    public class PassageText : MonoBehaviour
    {
        public class UnityStringEvent : UnityEvent<string> { }
        public readonly UnityStringEvent textChanged = new UnityStringEvent();

        [SerializeField] private TextMeshProUGUI textComponent;

        private string text;
        public string Text
        {
            get
            {
                return text;
            }

            set
            {
                text = value;
                textComponent.text = text;
                textChanged.Invoke(text);
            }
        }
    }
}