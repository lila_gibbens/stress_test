﻿using System;
using System.Collections;
using System.Collections.Generic;
using CrazyMinnow.SALSA;
using UnityEngine;

namespace Within.SuperBloom
{
    public class StoryPageWordsReader
    {
        private int passagePartIndex;
        
        private readonly List<string> passageParts;
        private readonly List<int> triggerWordIndexes;

        private int passageWordsRead;
        private int wordsRead;

        private string currentReadingPartTxt;

        private string passageText;

        private bool readingFromAudio;

        private bool isReading;

        private bool textCanBeRead;

        private StoryPageData pageData;

        private Salsa3D salsaCharacter;
        
        public StoryPageWordsReader(StoryPageData _pageData, Salsa3D _salsaCharacter)
        {
            pageData = _pageData;
            salsaCharacter = _salsaCharacter;
            
            string fullPassageText = pageData.Passage.Text;
            
            passageText = fullPassageText;
            passageParts = new List<string>(fullPassageText.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries));

            triggerWordIndexes = new List<int>();
            string[] words = fullPassageText.Split(new[] { ' ', '|' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < words.Length; ++i)
            {
                if (words[i].EndsWith("*"))
                {
                    triggerWordIndexes.Add(i);
                }
            }

            string passage = fullPassageText.Replace("*", string.Empty);
            passageParts = new List<string>(passage.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries));
            
            SequencerFlowEvents.pageStarted.AddListener(StopReading);
        }

        public void StartReadingFromPrerecorded()
        {
            StoryVoiceNarrator.Instance.OnNarrationComplete.AddListener(OnNarrationComplete);
            
            StartReading(true);
            CoroutinesRunner.Instance.StartCoroutine(PlayAudioParts());
        }

        private IEnumerator PlayAudioParts()
        {
            for (int partIndex = 0; partIndex < passageParts.Count; partIndex++)
            {
                yield return new WaitUntil(() => textCanBeRead);
                
                AudioClip partAudio = pageData.PreRecordedClips[partIndex];

                string[] partWords = passageParts[partIndex].Split();
                int wordsLength = string.Join(string.Empty, partWords).Length;

                float clipDuration = 0;

                if (partAudio != null)
                {
                    clipDuration = partAudio.length;
                    StoryVoiceNarrator.Instance.PlayNarrationClip(partAudio);
                }
                else
                {
                    //-- hardcoded reading time of 1 character. This case only happens if the audio is null and to facility the test of the story
                    clipDuration = wordsLength * 0.1f; 
                    Debug.LogError("No Audio setup for Prerecorded mode in this Page");
                }
                
                for (int i = 0; i < partWords.Length; i++)
                {
                    float wordProportion = partWords[i].Length / (float)wordsLength;
                    
                    float timeProportion = clipDuration * wordProportion;
                    
                    yield return new WaitForSeconds(timeProportion);

                    if (!isReading)
                    {
                        yield break;
                    }
                    
                    OnOneWordReadedFromUI();
                }
            }
        }
        
        /// <summary>
        /// We need to put null in the salsa charater audio source when the narration finishes, 
        /// otherwise it will play with the next page even if we don't want it 
        /// </summary>
        private void OnNarrationComplete()
        {
            StoryVoiceNarrator.Instance.OnNarrationComplete.RemoveListener(OnNarrationComplete); 
        }

        public void StartReading(bool fromAudio = false)
        {
            readingFromAudio = fromAudio;
            passagePartIndex = -1;
            passageWordsRead = 0;

            isReading = true;
            
            ROGERPlugin.onRogerWordForWordResult += OnRogerHits;
            SequencerControlDispatcher.ReadOneWord_Request.AddListener(OnOneWordReadedFromUI);
            
            //-- Remove all text transitions events when start reading a new passage, because some events could be still there 
            // if Chapter Skipping was used
            // TODO Ariel Investigate better solutions to manage chapter skipping with this situations
            SequencerFlowEvents.onPassageTextFadedOut.RemoveAllListeners(); 
            
            SequencerFlowEvents.passageStarted.Invoke();
            
            StartNextPart();
        }

        public void StopReading()
        {
            isReading = false;
            
            ROGERPlugin.onRogerWordForWordResult -= OnRogerHits;
            SequencerControlDispatcher.ReadOneWord_Request.RemoveListener(OnOneWordReadedFromUI);
        }
        
        private void StartNextPart()
        {
            // Passages can be skipped by using an empty passage text
            if (passageText.Trim().Length == 0)
            {
                SequencerFlowEvents.passageCompleted.Invoke();   
                return;
            }

            passagePartIndex++;

            if (passagePartIndex == 0)
            {
                wordsRead = 0;    
            }
            else
            {
                wordsRead -= currentReadingPartTxt.Split().Length;
            }
            
            currentReadingPartTxt = passageParts[passagePartIndex];

            textCanBeRead = true;
            
            SequencerFlowEvents.passagePartStarted.Invoke(currentReadingPartTxt);
        }

        private void OnPassagePartReaded()
        {
            textCanBeRead = false;

            SequencerFlowEvents.passagePartCompleted.Invoke();
            
            if (passagePartIndex >= passageParts.Count - 1)
            {
                // only invoke this if not reading from pre recorded audio. If reading from audio, when it finishes, a button will appear on screen
                // and that button will trigger this event when clicked
                if (!readingFromAudio)
                {
                    SequencerFlowEvents.passageCompleted.Invoke();    
                }
                else
                {
                    if (salsaCharacter != null)
                    {
                        salsaCharacter.audioClip = null;
                        salsaCharacter.audioSrc = null;
                    }
                    
                    StoryVoiceNarrator.Instance.OnNarrationComplete.Invoke();
                }
            }
            
            SequencerFlowEvents.onPassageTextFadedOut.AddListener(OnTextFadedOut);
        }

        private void OnTextFadedOut()
        {
            textCanBeRead = true;  
            
            SequencerFlowEvents.onPassageTextFadedOut.RemoveAllListeners();
            
            if (passagePartIndex < passageParts.Count - 1)
            {
                StartNextPart();
            }
        }

        private void OnOneWordReadedFromUI()
        {
            if (textCanBeRead)
            {
                OnRogerHits(passageWordsRead + 1);    
            }
        }
        
        private void OnRogerHits(int numHits)
        {
            int newWords = numHits - passageWordsRead;

            if (newWords <= 0)
            {
                return;
            }
            
            // Trigger words
            for (int i = 0; i < triggerWordIndexes.Count; ++i)
            {
                int triggerWord = triggerWordIndexes[i];
                if (numHits > triggerWord && wordsRead <= triggerWord)
                {
                    SequencerFlowEvents.onTriggerWordRead.Invoke(i - 1);
                }
            }
            
            wordsRead += newWords;
            passageWordsRead += newWords;
            
            SequencerFlowEvents.onWordsRead.Invoke(wordsRead);

            int totalWordsThisPart = currentReadingPartTxt.Split().Length;
            if (wordsRead >= totalWordsThisPart)
            {
                OnPassagePartReaded();
            }
        }
    }
}