﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Within.SuperBloom.StateManagement;

namespace Within.SuperBloom
{
    public enum EStoryPlayMode
    {
        ListenPrerecorded, ListenCustomRecorded, ReadTheLines, None
    }

    public class StorySequencer : MonoBehaviour
    {
        private static StorySequencer instance;
     
        /// <summary>
        /// If the containing scene is closing/closed this can return null
        /// </summary>
        /// <returns></returns>
        public static StorySequencer Instance
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }

                StorySequencer[] scriptInstance = GameObject.FindObjectsOfType<StorySequencer>();

                if (scriptInstance.Length > 1)
                {
                    throw new Exception("[StorySequencer] Multiple instances found of this singleton. It should be there only one");
                }
                else if (scriptInstance.Length == 0)
                {
                    // This will occur in the editor due to the scene reload that happens when the scene is played.
                }
                else
                {
                    instance = scriptInstance[0];
                }

                return instance;
            }
        }

        public bool IsReady { get; private set; }        
        [SerializeField] private List<StoryPage> storyPages;
        private StoryPage currentPage;
        private StoryStateManager stateManager;

        /// <summary>
        /// Assigned in Editor field by the StoryEditor
        /// </summary>
        [SerializeField] 
        private StoryData storydata;
        public StoryData StoryData
        {
            get { return storydata; }
            set { storydata = value; }
        }

        public bool IsStoryInProgress 
        { 
            get { return currentPage != null; } 
        }

        private float initialFixedDeltaTime;

        private int pageIndex;
        public int PageIndex
        {
            get { return pageIndex; }
        }

        public int TotalPages
        {
            get { return storyPages.Count; }
        }

        private void SetupPages()
        {
            storyPages = new List<StoryPage>();

            List<StoryPage> scenePages = SBSceneLocator.PagesContainer.transform.GetComponentsInChildren<StoryPage>().ToList();
            
            for (int i = 0; i < storydata.DataPages.Count; i++)
            {
                StoryPage pageToAdd = scenePages.Find(pi => pi.name == storydata.DataPages[i].name); 
                
                pageToAdd.Initialize();

                storyPages.Add(pageToAdd);
            }
        }

        private void Awake()
        {
            SetupPages();
            stateManager = new StoryStateManager(storyPages);
        }
        
        private void OnEnable()
        {
            initialFixedDeltaTime = Time.fixedDeltaTime;
            SequencerFlowEvents.passageCompleted.AddListener(OnPassageComplete);
            SequencerFlowEvents.sequenceCompleted.AddListener(OnPageEndReached);

            SequencerControlDispatcher.Play += PlayCurrentPage;
            SequencerControlDispatcher.Stop += Stop;
            SequencerControlDispatcher.Pause += Pause;
            SequencerControlDispatcher.NextPage += SkipPage;
            SequencerControlDispatcher.PrevPage += PrevPage;
            SequencerControlDispatcher.JumpToPage += JumpToPage;
            // SequencerControlDispatcher.ExitStory += PlayCurrentPage;
            SequencerControlDispatcher.RestartStory += RestartStory;

            SequencerControlDispatcher.StartStory_Request.AddListener(ResumeOrStartStory);
            SequencerControlDispatcher.StartPassage_Request.AddListener(ShowPassage);

            IsReady = true;
        }

        private void OnDisable()
        {
            IsReady = false;

            SequencerFlowEvents.passageCompleted.RemoveListener(OnPassageComplete);
            SequencerFlowEvents.sequenceCompleted.RemoveListener(OnPageEndReached);

            SequencerControlDispatcher.Play -= PlayCurrentPage;
            SequencerControlDispatcher.Stop -= Stop;
            SequencerControlDispatcher.Pause -= Pause;
            SequencerControlDispatcher.NextPage -= SkipPage;
            SequencerControlDispatcher.PrevPage -= PrevPage;
            SequencerControlDispatcher.JumpToPage -= JumpToPage;
            // SequencerControlDispatcher.ExitStory -= PlayCurrentPage;
            SequencerControlDispatcher.RestartStory -= RestartStory;

            SequencerControlDispatcher.StartStory_Request.RemoveListener(ResumeOrStartStory);
            SequencerControlDispatcher.StartPassage_Request.RemoveListener(ShowPassage);
        }

        private void OnDestroy()
        {
            // Pausing is global, make sure we don't affect other scenes
            Unpause();
        }
        
        private void RestartStory()
        {
            JumpToPage(0);
            StartPage(0);
        }

        private void StartStory()
        {
            // Salsa mic MUST be initialized before ROGER
            StoryVoiceNarrator.Instance.InitializeMic();
            ROGERPlugin.initializeMicrophone();
            ROGERPlugin.initializeRoger();

            stateManager.SaveState();
            
            StartPage(0);
            
            SequencerFlowEvents.onStoryStarted.Invoke();
        }

        private void ResumeOrStartStory()
        {
            if (currentPage != null) // Story has already been started
            {
                Unpause();
            }
            else
            {
                StartStory();
            }
        }

        private void JumpToPage(int index)
        {
            stateManager.RestoreStateAtIndex(index);
            StartPage(index);
        }

        private void PlayCurrentPage()
        {
            if (currentPage != null)
            {
                currentPage.PlaySequence();
            }
            else
            {
                Debug.LogWarning("[StorySequencer] Attempted to PlayCurrentPage without having started the story");
            }
        }

        private void ShowPassage()
        {
            if (currentPage != null)
            {
                currentPage.ShowPassage();
            }
            else
            {
                Debug.LogWarning("[StorySequencer] Attempted to show passge without having started the story");
            }
        }
        
        private void OnPassageComplete()
        {
        }

        private void StartPage(int index)
        {
            pageIndex = index;
            
            currentPage = storyPages[index];

            SequencerControlDispatcher.StartPageSequence_Request.RemoveAllListeners();
            SequencerControlDispatcher.StartPageSequence_Request.AddListener(currentPage.PlaySequence);
            
            SequencerFlowEvents.pageStarted.Invoke();
            
            TimelineFlowControl.Instance.SetSkipCounter(0);
            
            PlayCurrentPage();
        }

        private void Stop()
        {
            if (currentPage != null)
            {
                currentPage.Stop();
            }
        }

        public void Pause()
        {
            Time.timeScale = 0f;
            Time.fixedDeltaTime = 0f;
        }

        public void Unpause()
        {
            Time.timeScale = 1f;
            Time.fixedDeltaTime = initialFixedDeltaTime;
        }

        private void SkipPage()
        {
            if (PageIndex >= storyPages.Count - 1)
            {
                FinishStory();
                return;
            }

            JumpToPage(PageIndex + 1);
        }
        
        private void NextPage()
        {
            if (PageIndex >= storyPages.Count - 1)
            {
                FinishStory();
                return;
            }

            StartPage(PageIndex + 1);
        }

        private void PrevPage()
        {
            if (PageIndex <= 0)
            {
                return;
            }

            JumpToPage(PageIndex - 1);
        }

        private void OnPageEndReached()
        {
            NextPage();
        }

        private void FinishStory()
        {
            SequencerFlowEvents.onStoryFinished.Invoke();   
        }
    }
}
