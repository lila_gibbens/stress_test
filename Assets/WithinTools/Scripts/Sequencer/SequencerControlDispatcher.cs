﻿using UnityEngine.Events;

namespace Within.SuperBloom
{
    public class SequencerControlDispatcher
    {
        public static readonly UnityEvent OnReadyToStart = new UnityEvent();

        public static readonly UnityEvent StartStory_Request = new UnityEvent();

        public static readonly UnityEvent StartPageSequence_Request = new UnityEvent();
        public static readonly UnityEvent StartPassage_Request = new UnityEvent();

        public static readonly UnityEvent ReadOneWord_Request = new UnityEvent();
        
        public delegate void Play_Delegate();
        public static Play_Delegate Play = delegate { };

        public delegate void Stop_Delegate();
        public static Stop_Delegate Stop = delegate { };

        public delegate void Pause_Delegate();
        public static Pause_Delegate Pause = delegate { };

        public delegate void JumpToPage_Delegate(int pageIndex);
        public static JumpToPage_Delegate JumpToPage = delegate { };

        public delegate void NextPage_Delegate();
        public static NextPage_Delegate NextPage = delegate { };

        public delegate void PrevPage_Delegate();
        public static PrevPage_Delegate PrevPage = delegate { };

        public delegate void RestartStory_Delegate();
        public static RestartStory_Delegate RestartStory = delegate { };

        public delegate void ExitStory_Delegate();
        public static ExitStory_Delegate ExitStory = delegate { };
    }
}