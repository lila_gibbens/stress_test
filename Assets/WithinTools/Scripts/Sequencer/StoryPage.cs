using CrazyMinnow.SALSA;
using System.IO;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Playables;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Within.SuperBloom
{
	[RequireComponent(typeof(StoryPageDataBinder))]
	public class StoryPage : MonoBehaviour
	{
        private static readonly string pageObjectPrefix = "StoryPage";

		/// <summary>
		/// This is a duplicate of the StoryPageData asset to be used in runtime
		/// </summary>
		private StoryPageData data;

		public StoryPageData Data
		{
			get
			{
				Assert.IsNotNull(data, "[StoryPage] this.data is null (cloned data for runtime access)");
				return data;
			}
		}
		
		public PlayableDirector TimelineDirector;

		/// <summary>
		/// Timeline pause state
		/// </summary>
		/// <returns></returns>
		private bool IsTimelinePaused { get { return TimelineDirector.state == PlayState.Paused; } }

		/// <summary>
		/// ShowPassage has been called but PlaySequence hasn't been called yet.
		/// </summary>
		private bool isPassageBeingDisplayed;

        private bool rogerInitialized;

		private StoryPageWordsReader wordsReader;
		
        [SerializeField] private Salsa3D salsaCharacter;
        public Salsa3D SalsaCharacter
        {
            get
            {
                return salsaCharacter;
            }
            set
            {
                if (salsaCharacter != value)
                {
#if UNITY_EDITOR
                    Undo.RecordObject(this, "Set Lipsync Character");
#endif
                    salsaCharacter = value;
                }
            }
        }

		/// <summary>
		/// Call this only in EDITOR time (from whitin tools)
		/// This binds this StoryPage with its DataBinder (it has to be created before calling this) 
		/// </summary>
		public void SetDataFromBinder()
		{
			data = ScriptableObject.CreateInstance<StoryPageData>();
			
			StoryPageDataBinder storyPageDataBinder = gameObject.GetComponent<StoryPageDataBinder>();
			data = storyPageDataBinder.CloneData();
		}

		public void Initialize()
		{
			TimelineDirector = GetComponent<PlayableDirector>();
			
			SetDataFromBinder();
			
			wordsReader = new StoryPageWordsReader(data, salsaCharacter);
		}

        public void ShowPassage()
        {
	        if (salsaCharacter != null)
	        {
		        salsaCharacter.audioSrc = StoryVoiceNarrator.Instance.NarratorAudioSource;
	        }
	        
            if (StoryModeOptions.playMode == EStoryPlayMode.ListenPrerecorded)
            {
	            wordsReader.StartReadingFromPrerecorded();  
            }
	        else if (StoryModeOptions.playMode == EStoryPlayMode.ReadTheLines)
	        {
                // Initialize ROGER
                string storyName = StoryData.GetTitleFilename(StorySequencer.Instance.StoryData.Title);
                // TODO: Add id to passage once we are automatically generating models
                string passageName = storyName; // + GetID();
                string modelFile = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf("/")) + "/" + passageName + ".fst";
                FileInfo info = new FileInfo(modelFile);
                if (info.Exists)
                {
                    string passage = data.Passage.Text.TrimEnd(' ');
                    ROGERPlugin.setupRoger(passageName, passage);
                    ROGERPlugin.startRoger(0.2f);
                    rogerInitialized = true;
                }

		        if (salsaCharacter != null)
		        {
			        StoryVoiceNarrator.Instance.StartMicNarration();
		        }
		        
		        SequencerFlowEvents.passageCompleted.AddListener(OnPassageComplete);
		        wordsReader.StartReading();
	        }
	        
	        isPassageBeingDisplayed = true;
        }

		private void OnPassageComplete()
		{
			wordsReader.StopReading();
			
            if (rogerInitialized)
            {
                ROGERPlugin.stopRoger();
                rogerInitialized = false;
            }

			SequencerFlowEvents.passageCompleted.RemoveListener(OnPassageComplete);

            if (salsaCharacter != null)
            {
                StoryVoiceNarrator.Instance.StopMicNarration();
            }
		}

		public void PlaySequence()
        {
            StoryVoiceNarrator.Instance.OnNarrationComplete.RemoveAllListeners();
            SequencerControlDispatcher.StartPageSequence_Request.RemoveAllListeners();

            StoryVoiceNarrator.Instance.StopNarration();

            TimelineDirector.Play();
            SequencerFlowEvents.sequenceStarted.Invoke();
        }

        public void Stop()
        {
            TimelineDirector.time = 0;
            TimelineDirector.Stop();
            isPassageBeingDisplayed = false;
        }

        private string GetID()
        {
            return gameObject.name.Substring(pageObjectPrefix.Length);
        }
    }
}