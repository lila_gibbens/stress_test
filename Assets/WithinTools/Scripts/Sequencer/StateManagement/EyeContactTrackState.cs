﻿using UnityEngine;

namespace Within.SuperBloom.StateManagement
{
    public class EyeContactTrackState : TrackState<EyeContactCharacter>
    {
        private GameObject targetLookObject;
        private bool isFollowingTarget;
        
        public EyeContactTrackState(EyeContactCharacter eyeContactCharacter) : base(eyeContactCharacter)
        {
        }
        
        public override void SaveState()
        {
            targetLookObject = stateObject.targetLookObject;
            isFollowingTarget = stateObject.isFollowingTarget;
        }

        public override void RestoreInitialState()
        {
            if (stateObject == null)
            {
                return;
            }

            stateObject.targetLookObject = targetLookObject;
            stateObject.isFollowingTarget = isFollowingTarget;
        }
        
        public override void RestoreFinalState()
        {
        }
    }
}