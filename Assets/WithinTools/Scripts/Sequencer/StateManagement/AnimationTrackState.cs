﻿using System;
using UnityEngine;

namespace Within.SuperBloom.StateManagement
{
    public class AnimationTrackState : TrackState<Animator>
    {
        private Vector3 position;
        private Quaternion rotation;
     
        private Vector3 localPosition;
        private Vector3 localScale;
        private Quaternion localRotation;

        public AnimationTrackState(Animator animator) : base(animator)
        {
        }
        
        public override void SaveState()
        {
            if (stateObject == null)
            {
                return;
            }
            Transform transform = stateObject.transform;
            
            position = transform.position;
            localPosition = transform.localPosition;
         
            rotation = transform.rotation;
            localRotation = transform.localRotation;
         
            localScale = transform.localScale;
        }

        public override void RestoreInitialState()
        {
            if (stateObject == null)
            {
                return;
            }
            Transform transform = stateObject.transform;
            
            transform.position = position;
            transform.localPosition = localPosition;
         
            transform.rotation = rotation;
            transform.localRotation = localRotation;
         
            transform.localScale = localScale;
        }

        public override void RestoreFinalState()
        {
        }
    }
}