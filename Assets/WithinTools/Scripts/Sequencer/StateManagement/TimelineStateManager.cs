﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom.StateManagement
{
    public class TimelineStateManager : CompositeStateManager
    {
        private PlayableDirector playableDirector;

        private double endPageEndTime;
        
        public TimelineStateManager(PlayableDirector director)
        {
            playableDirector = director;
            director.RebuildGraph();
            PlayableGraph graph = director.playableGraph;
            int trackCount = graph.GetOutputCount();
            for (int i = 0; i < trackCount; i++)
            {
                PlayableOutput track = graph.GetOutput(i);
                Object trackAsset = track.GetReferenceObject();
                Object binding = director.GetGenericBinding(trackAsset);
                IStateManager state = TrackStateFactory.GetTrackState(director, trackAsset, binding);
                // TODO: Don't store duplicate states
                if (state != null)
                {
                    states.Add(state);
                }
                
                if (trackAsset.GetType() == typeof(PageManagementPlayableTrack))
                {
                    TrackAsset trackObject = trackAsset as TrackAsset;
                    foreach (TimelineClip clip in trackObject.GetClips())
                    {
                        if (clip.asset.GetType() == typeof(EndOfPagePlayableClip))
                        {
                            endPageEndTime = clip.end;
                        }
                    }
                }
            }
        }

        public override void RestoreInitialState()
        {
            playableDirector.Stop();
            base.RestoreInitialState();
        }

        public override void RestoreFinalState()
        {
            base.RestoreFinalState();
            playableDirector.Play();
            playableDirector.time = endPageEndTime + 2; // 2 seconds ofsset (just in case..?)
            playableDirector.Evaluate();
        }
    }
}