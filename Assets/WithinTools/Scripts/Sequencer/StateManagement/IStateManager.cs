﻿namespace Within.SuperBloom.StateManagement
{
    public interface IStateManager
    {
        void SaveState();
        void RestoreInitialState();
        void RestoreFinalState();
    }
}