﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom.StateManagement
{
    public class ControlTrackState : CompositeStateManager
    {
        public ControlTrackState(PlayableDirector director, TrackAsset track)
        {
            foreach (TimelineClip clip in track.GetClips())
            {
                ControlPlayableAsset asset = clip.asset as ControlPlayableAsset;
                GameObject directorObject = asset.sourceGameObject.Resolve(director.playableGraph.GetResolver());
                if (directorObject == null)
                {
                    continue;
                }
                PlayableDirector controlTrackDirector = directorObject.GetComponent<PlayableDirector>();
                if (controlTrackDirector != null)
                {
                    states.Add(new TimelineStateManager(controlTrackDirector));
                }
            }
        }
    }
}