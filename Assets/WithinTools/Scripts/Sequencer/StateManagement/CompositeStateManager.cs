﻿using System.Collections.Generic;

namespace Within.SuperBloom.StateManagement
{
    public abstract class CompositeStateManager : IStateManager
    {
        protected readonly List<IStateManager> states = new List<IStateManager>();
        
        public virtual void SaveState()
        {
            foreach (IStateManager state in states)
            {
                state.SaveState();
            }
        }

        public virtual void RestoreInitialState()
        {
            foreach (IStateManager state in states)
            {
                state.RestoreInitialState();
            }
        }

        public virtual void RestoreFinalState()
        {
            foreach (IStateManager state in states)
            {
                state.RestoreFinalState();
            }
        }

        public void RestoreStateAtIndex(int index)
        {
            RestoreInitialState();
            for (int i = 0; i < index; ++i)
            {
                states[i].RestoreFinalState();
            }
        }
    }
}