﻿namespace Within.SuperBloom.StateManagement
{
    public abstract class TrackState<T> : IStateManager
    {
        protected T stateObject;

        protected TrackState(T stateObject)
        {
            this.stateObject = stateObject;
        }

        public abstract void SaveState();
        public abstract void RestoreInitialState();
        public abstract void RestoreFinalState();
    }
}