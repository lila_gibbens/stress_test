﻿using UnityEngine;

namespace Within.SuperBloom.StateManagement
{
    public class AnimatorStateTrackState : TrackState<Animator>
    {
        public AnimatorStateTrackState(Animator animator) : base(animator)
        {
        }
        
        public override void SaveState()
        {
            // No state to save, we will just revert all parameters to defaults
        }

        public override void RestoreInitialState()
        {
            if (stateObject == null)
            {
                return;
            }

            foreach (AnimatorControllerParameter param in stateObject.parameters)
            {
                switch (param.type)
                {
                    case AnimatorControllerParameterType.Bool:
                        stateObject.SetBool(param.nameHash, param.defaultBool);
                        break;
                    case AnimatorControllerParameterType.Float:
                        stateObject.SetFloat(param.nameHash, param.defaultFloat);
                        break;
                    case AnimatorControllerParameterType.Int:
                        stateObject.SetInteger(param.nameHash, param.defaultInt);
                        break;
                    case AnimatorControllerParameterType.Trigger:
                        stateObject.ResetTrigger(param.nameHash);
                        break;
                }
            }
            
            stateObject.Rebind();
        }
        
        public override void RestoreFinalState()
        {
        }
    }
}