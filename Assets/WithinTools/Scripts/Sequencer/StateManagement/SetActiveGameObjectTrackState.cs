﻿using UnityEngine;

namespace Within.SuperBloom.StateManagement
{
    public class SetActiveGameObjectTrackState : TrackState<GameObject>
    {
        private bool isActive;
        
        public SetActiveGameObjectTrackState(GameObject gameObject) : base(gameObject)
        {
        }
        
        public override void SaveState()
        {
            isActive = stateObject.activeSelf;
        }

        public override void RestoreInitialState()
        {
            if (stateObject == null)
            {
                return;
            }
            
            stateObject.SetActive(isActive);
        }
        
        public override void RestoreFinalState()
        {
        }
    }
}