﻿using UnityEngine;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom.StateManagement
{
    public class BGMTrackState : TrackState<Animator>
    {
        private AudioClip currentClip;
        
        public BGMTrackState() : base(null)
        {
            currentClip = null;
        }
        
        public override void SaveState()
        {
        }

        public override void RestoreInitialState()
        {
            BGMCommand musicCommand = new BGMCommand
            {
                ControlType = MusicEffectControlType.StopMusic,
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
        }
        
        public override void RestoreFinalState()
        {
        }
    }
}