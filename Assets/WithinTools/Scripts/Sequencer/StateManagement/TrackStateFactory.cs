﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom.StateManagement
{
    public static class TrackStateFactory
    {
        public static IStateManager GetTrackState(PlayableDirector director, UnityEngine.Object track, UnityEngine.Object binding)
        {
            // If it's a Within track, we can get the state directly
            IStateTracked stateTracked = track as IStateTracked;
            if (stateTracked != null)
            {
                return stateTracked.GetTrackState(binding);
            }
            
            // Internal track types
            Type trackType = track.GetType();
            if (trackType == typeof(AnimationTrack))
            {
                GameObject bindingGameObject = binding as GameObject;
                if (bindingGameObject != null)
                {
                    return new AnimationTrackState(bindingGameObject.GetComponent<Animator>());
                }
            }

            if (trackType == typeof(ControlTrack))
            {
                return new ControlTrackState(director, track as TrackAsset);
            }
            return null;
        }
    }
}