﻿using System.Collections.Generic;

namespace Within.SuperBloom.StateManagement
{
    public class StoryStateManager : CompositeStateManager
    {
        public StoryStateManager(List<StoryPage> storyPages)
        {
            foreach (StoryPage page in storyPages)
            {
                states.Add(new TimelineStateManager(page.TimelineDirector));
            }
        }
    }
}