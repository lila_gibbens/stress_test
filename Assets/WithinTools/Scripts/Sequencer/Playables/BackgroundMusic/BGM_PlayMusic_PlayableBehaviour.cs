﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom
{
    [Serializable]
    public class BGM_PlayMusic_PlayableBehaviour : TimelinePlayableBehaviour
    {
        [Tooltip("The music audio clip to be played.")]
        public AudioClip MusicClip;
        
        [Tooltip("The target volume of the music after the crossfade.")]
        [Range(0, 1)]
        public float Volume = 1f;

        [Tooltip("Apply crossfade effect.")]
        public bool CrossfadeClips = true;
        
        [Tooltip("Duration of the crossfade.")]
        public float CrossfadeDuration = BackgroundMusicPlayer.DEFAULT_FADE_DURATION;

        protected override void OnClipEntered(Playable playable, object playerData)
        {
            if (!Application.isPlaying)
            {
                return;
            }

            BGMCommand musicCommand = new BGMCommand()
            {
                ControlType = MusicEffectControlType.PlayMusic,
                Clip = MusicClip,
                Volume = this.Volume,
                CrossFadeTime = CrossfadeDuration
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
        }
    }
}