﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    [Serializable]
    public class BGM_EffectControl_PlayableClip : PlayableAsset, ITimelineClipAsset
    {
        public BGM_EffectControl_PlayableBehaviour properties = new BGM_EffectControl_PlayableBehaviour();

        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter",
            Justification = "Unity Timeline library specifies the name via ITimelineClipAsset")]
        public ClipCaps clipCaps
        {
            get { return ClipCaps.None; }
        }

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<BGM_EffectControl_PlayableBehaviour>.Create(graph, properties);
            return playable;
        }
    }
}