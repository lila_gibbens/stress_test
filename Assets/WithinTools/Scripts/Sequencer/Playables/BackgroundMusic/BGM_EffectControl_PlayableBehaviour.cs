﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom
{
    [Serializable]
    public class BGM_EffectControl_PlayableBehaviour : TimelinePlayableBehaviour
    {
        [Tooltip("The target volume of the music after the crossfade")]
        [Range(0, 1)]
        public float SetNewVolume = 1f;

        [Tooltip("Target low pass frequency.")]
        [Range(900f, BackgroundMusicPlayer.MAX_LOWPASS_FREQUENCY)]
        public float SetLowPassFrequency = BackgroundMusicPlayer.DEFAULT_LOWPASS_FREQUENCY;

        [Tooltip("Either SetVolume, Pause, Start Ducking Music or Stop Ducking Music.")]
        public MusicEffectControlType EffectControlType;

        [Tooltip("Apply crossfade effect.")]
        public bool CrossfadeEffect = true;
        
        [Tooltip("Duration of the crossfade.")]
        public float CrossfadeDuration = BackgroundMusicPlayer.DEFAULT_FADE_DURATION;

        protected override void OnClipEntered(Playable playable, object playerData)
        {
            BGMCommand musicCommand = new BGMCommand
            {
                ControlType = EffectControlType,
                Clip = null,
                Volume = SetNewVolume,
                CrossFadeTime = CrossfadeDuration,
                LowPassFrequency = SetLowPassFrequency
            };
            BackgroundMusicPlayer.Instance.DoCommand(musicCommand);
        }
    }
}