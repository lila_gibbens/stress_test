﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Within.SuperBloom.StateManagement;

namespace Within.SuperBloom
{
    [TrackColor(0.847f, 0.533f, 0.047f)]
    [TrackClipType(typeof(BGM_EffectControl_PlayableClip))]
    [TrackClipType(typeof(BGM_PlayMusic_PlayableClip))]
    public class BGM_PlayableTrack : TrackAsset, IStateTracked
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            var playable = ScriptPlayable<DefaultPlayableMixer>.Create(graph, inputCount);
            playable.GetBehaviour().SetTrackDetails(go.GetComponent<PlayableDirector>(), GetClips());
            return playable;
        }

        public IStateManager GetTrackState(Object binding)
        {
            return new BGMTrackState();
        }
    }
}