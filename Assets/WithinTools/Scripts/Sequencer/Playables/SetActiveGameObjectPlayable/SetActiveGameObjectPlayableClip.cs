using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    [Serializable]
    public class SetActiveGameObjectPlayableClip : PlayableAsset, ITimelineClipAsset
    {
        public SetActiveGameObjectPlayableBehaviour properties = new SetActiveGameObjectPlayableBehaviour();

        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Unity Timeline library specifies the name via ITimelineClipAsset")]
        public ClipCaps clipCaps
        {
            get { return ClipCaps.None; }
        }

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<SetActiveGameObjectPlayableBehaviour>.Create(graph, properties);
            return playable;
        }
    }
}