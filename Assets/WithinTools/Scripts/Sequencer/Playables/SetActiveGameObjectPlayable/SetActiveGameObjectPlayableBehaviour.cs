using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Within.SuperBloom
{
    [Serializable]
    public class SetActiveGameObjectPlayableBehaviour : TimelinePlayableBehaviour
    {
        [Tooltip("The new enable/disable state of the gameobject")]
        [SerializeField]
        private bool newActive;

        protected override void OnClipEntered(Playable playable, object playerData)
        {
            base.OnClipEntered(playable, playerData);

            GameObject targetGameObject = playerData as GameObject;
            
            targetGameObject.SetActive(newActive);
        }
    }
}