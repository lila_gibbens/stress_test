using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Within.SuperBloom.StateManagement;

namespace Within.SuperBloom
{
    [TrackColor(0.855f, 0.8623f, 0.87f)]
    [TrackClipType(typeof(SetActiveGameObjectPlayableClip))]
    [TrackBindingType(typeof(GameObject))]
    public class SetActiveGameObjectPlayableTrack : TrackAsset, IStateTracked
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            var playable = ScriptPlayable<DefaultPlayableMixer>.Create(graph, inputCount); 
            playable.GetBehaviour().SetTrackDetails(go.GetComponent<PlayableDirector>(), GetClips());
            return playable;
        }

        public IStateManager GetTrackState(Object binding)
        {
            GameObject gameObject = binding as GameObject;
           
            if (gameObject != null)
            {
                return new SetActiveGameObjectTrackState(gameObject);
            }
            
            return null;
        }
    }
}