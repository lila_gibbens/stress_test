﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    [Serializable]
    public class TriggerWordPlayableClip : PlayableAsset, ITimelineClipAsset
    {
        public TriggerWordPlayableBehaviour template = new TriggerWordPlayableBehaviour();

        [NonSerialized] public int ClipIndex;

        [HideInInspector]
        public double StartsAt;
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Unity Timeline library specifies the name via ITimelineClipAsset")]
        public ClipCaps clipCaps
        {
            get { return ClipCaps.None; }
        }

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<TriggerWordPlayableBehaviour>.Create(graph, template);
            var behaviour = playable.GetBehaviour();
            behaviour.ClipIndex = ClipIndex;
            behaviour.Clip = this;
            return playable;
        }
    }
}