using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    [TrackColor(0.5514706f, 0f, 0f)]
    [TrackClipType(typeof(StartPagePlayableClip))]
    public class StartPagePlayableTrack : TrackAsset
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            var playable = ScriptPlayable<DefaultPlayableMixer>.Create(graph, inputCount);
            playable.GetBehaviour().SetTrackDetails(go.GetComponent<PlayableDirector>(), GetClips(), false);
            
            foreach (TimelineClip clip in GetClips())
            {
                clip.displayName = "Start Passage & Wait";
            }

            return playable;
        }
    }
}