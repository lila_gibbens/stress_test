using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    [TrackColor(0.5514706f, 0f, 0f)]
    [TrackClipType(typeof(StartPagePlayableClip))]
    [TrackClipType(typeof(EndOfPagePlayableClip))]
    [TrackClipType(typeof(TriggerWordPlayableClip))]
    public class PageManagementPlayableTrack : TrackAsset
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            IEnumerable<TimelineClip> clips = GetClips();
            var playable = ScriptPlayable<DefaultPlayableMixer>.Create(graph, inputCount); 
            playable.GetBehaviour().SetTrackDetails(go.GetComponent<PlayableDirector>(), GetClips(), false);

            int index = 0;
            foreach (TimelineClip clip in clips)
            {
                TriggerWordPlayableClip triggerWordClip = clip.asset as TriggerWordPlayableClip;
                if (triggerWordClip != null)
                {
                    triggerWordClip.ClipIndex = index++;
                    triggerWordClip.StartsAt = clip.start;
                }
                StartPagePlayableClip startPagePlayableClip = clip.asset as StartPagePlayableClip;
                if (startPagePlayableClip != null)
                {
                    startPagePlayableClip.StartsAt = clip.start;
                }
            }
            
            return playable;
        }
    }
}