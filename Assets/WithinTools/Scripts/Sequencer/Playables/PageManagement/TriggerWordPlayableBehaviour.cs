﻿using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Within.SuperBloom
{
    [Serializable]
    public class TriggerWordPlayableBehaviour : TimelinePlayableBehaviour
    {
        [NonSerialized] public int ClipIndex;

        [HideInInspector]
        public TriggerWordPlayableClip Clip;

        private bool listening = false;
        private bool triggerWordRead = false;

        public override void OnTimelinePlay()
        {
            SequencerFlowEvents.onTriggerWordRead.AddListener(OnTriggerWordRead);
            SequencerFlowEvents.passageCompleted.AddListener(OnPassageRead);
            listening = true;
        }

        public override void OnGraphStop(Playable playable)
        {
            RemoveListeners();
        }

        private void RemoveListeners()
        {
            if (listening)
            {
                SequencerFlowEvents.onTriggerWordRead.RemoveListener(OnTriggerWordRead);
                SequencerFlowEvents.passageCompleted.RemoveListener(OnPassageRead);
                listening = false;
            }
        }

        private void OnPassageRead()
        {
            RemoveListeners();
            triggerWordRead = true;
        }
        
        private void OnTriggerWordRead(int id)
        {
            if (id >= ClipIndex)
            {
                OnPassageRead();
            }
        }

        protected override void OnClipExited(Playable playable, object playerData)
        {
            if (!Application.isPlaying || triggerWordRead)
            {
                return;
            }

            PlayableDirector director = playable.GetGraph().GetResolver() as PlayableDirector;

            director.time = Clip.StartsAt;
        }
    }
}