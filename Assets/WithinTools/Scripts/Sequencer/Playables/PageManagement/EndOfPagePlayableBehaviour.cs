using System;
using UnityEngine.Playables;

namespace Within.SuperBloom
{
    [Serializable]
    public class EndOfPagePlayableBehaviour : TimelinePlayableBehaviour
    {
        protected override void OnClipEntered(Playable playable, object playerData)
        {
            SequencerFlowEvents.sequenceCompleted.Invoke();
        }
    }
}