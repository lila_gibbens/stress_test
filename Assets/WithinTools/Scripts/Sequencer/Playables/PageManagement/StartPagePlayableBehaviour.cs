using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Within.SuperBloom
{
    [Serializable]
    public class StartPagePlayableBehaviour : TimelinePlayableBehaviour
    {
        [HideInInspector]
        public StartPagePlayableClip Clip;

        private bool finishReceived = false;
        private bool passageShown = false;

        protected override void OnClipEntered(Playable playable, object playerData)
        {
            SequencerFlowEvents.passageCompleted.AddListener(OnPassageRead);
            SequencerFlowEvents.onTriggerWordRead.AddListener(OnTriggerWordRead);
            
            if (passageShown)
            {
                return;
            }
        
            SequencerControlDispatcher.StartPassage_Request.Invoke();
            passageShown = true;
        }
        
        private void OnPassageRead()
        {
            SequencerFlowEvents.passageCompleted.RemoveListener(OnPassageRead);
            SequencerFlowEvents.onTriggerWordRead.RemoveListener(OnTriggerWordRead);
            finishReceived = true;
        }

        private void OnTriggerWordRead(int id)
        {
            OnPassageRead();
        }

        protected override void OnClipExited(Playable playable, object playerData)
        {
            if (!Application.isPlaying || finishReceived)
            {
                passageShown = false;
                return;
            }

            PlayableDirector director = playable.GetGraph().GetResolver() as PlayableDirector;
            
            double timeToJump = director.time - 1f;
            director.time = Clip.StartsAt;
        }
    }
}