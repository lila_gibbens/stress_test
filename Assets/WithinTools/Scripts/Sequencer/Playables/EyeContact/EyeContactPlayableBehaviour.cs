using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Within.SuperBloom
{
    [Serializable]
    public class EyeContactPlayableBehaviour : PlayableBehaviour
    {
        [Tooltip("if true, will start looking to target, if false will stop following target")]
        public bool ShouldLook;
        
        [Tooltip("if true, the target will be the Camera")]
        public bool LookToCamera;
        
        [Tooltip("if LookToCamera was false, the ye will look this object")]
        public GameObject TargetGameObject;

        public void DoLookAt(EyeContactCharacter eyeContactCharacter)
        {
            GameObject targetGameObject = LookToCamera ? Camera.main.gameObject : TargetGameObject;

            eyeContactCharacter.SetLooking(ShouldLook, targetGameObject);
        }
    }
}