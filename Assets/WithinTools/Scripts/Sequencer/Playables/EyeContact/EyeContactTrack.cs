using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Within.SuperBloom.StateManagement;

namespace Within.SuperBloom
{
    [TrackColor(0.855f, 0.8623f, 0f)]
    [TrackClipType(typeof(EyeContactClip))]
    [TrackBindingType(typeof(EyeContactCharacter))]
    public class EyeContactTrack : TrackAsset, IStateTracked
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            var playable = ScriptPlayable<EyeContactMixer>.Create(graph, inputCount); 
            playable.GetBehaviour().SetTrackDetails(go.GetComponent<PlayableDirector>(), GetClips());
            return playable;
        }

        public IStateManager GetTrackState(Object binding)
        {
            return new EyeContactTrackState(binding as EyeContactCharacter);
        }
    }
}