using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    [Serializable]
    public class EyeContactClip : PlayableAsset, ITimelineClipAsset
    {
        public EyeContactPlayableBehaviour template = new EyeContactPlayableBehaviour();
        public ExposedReference<GameObject> TargetGameObject;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Unity Timeline library specifies the name via ITimelineClipAsset")]
        public ClipCaps clipCaps
        {
            get { return ClipCaps.None; }
        }

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<EyeContactPlayableBehaviour>.Create(graph, template);
            playable.GetBehaviour().TargetGameObject = TargetGameObject.Resolve(graph.GetResolver());
            return playable;
        }
    }
}