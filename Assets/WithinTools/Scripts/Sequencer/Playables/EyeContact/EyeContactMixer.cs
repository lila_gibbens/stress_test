using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    public class EyeContactMixer : PlayableBehaviour
    {
        private PlayableDirector director;
        private IEnumerable<TimelineClip> clips;

        private double previousTime = 0;

        public void SetTrackDetails(PlayableDirector director, IEnumerable<TimelineClip> clips)
        {
            this.director = director;
            this.clips = clips;
        }
        
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            int input = 0;
            foreach (TimelineClip clip in clips)
            {
                ScriptPlayable<EyeContactPlayableBehaviour> inputPlayable = (ScriptPlayable<EyeContactPlayableBehaviour>)playable.GetInput(input);
                EyeContactPlayableBehaviour behaviour = inputPlayable.GetBehaviour();
                
                if (behaviour != null)
                {
                    if (director.time >= clip.start && previousTime < clip.start)
                    {
                        behaviour.DoLookAt(playerData as EyeContactCharacter);
                    }
                }
                ++input;
            }

            previousTime = director.time;
        }
    }
}