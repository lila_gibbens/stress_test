using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    [Serializable]
    public class AnimatorStatePlayableClip : PlayableAsset, ITimelineClipAsset
    {
        [SerializeField] private AnimatorStatePlayableBehaviour properties = new AnimatorStatePlayableBehaviour();

        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Unity Timeline library specifies the name via ITimelineClipAsset")]
        public ClipCaps clipCaps
        {
            get { return ClipCaps.None; }
        }

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            return ScriptPlayable<AnimatorStatePlayableBehaviour>.Create(graph, properties);
        }
    }
}