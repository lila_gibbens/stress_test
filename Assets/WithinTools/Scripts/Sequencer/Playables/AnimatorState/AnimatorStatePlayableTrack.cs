using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Within.SuperBloom.StateManagement;

namespace Within.SuperBloom
{
    [TrackColor(0.2f, 0.2f, 0.8f)]
    [TrackClipType(typeof(AnimatorStatePlayableClip))]
    [TrackBindingType(typeof(Animator))]
    public class AnimatorStatePlayableTrack : TrackAsset, IStateTracked
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            var playable = ScriptPlayable<AnimatorStatePlayableMixer>.Create(graph, inputCount);
            playable.GetBehaviour().SetTrackDetails(go.GetComponent<PlayableDirector>(), GetClips());
            return playable;
        }

        public IStateManager GetTrackState(Object binding)
        {
            Animator animator = binding as Animator;
            if (animator != null)
            {
                return new AnimatorStateTrackState(animator);
            }
            
            return null;
        }
    }
}