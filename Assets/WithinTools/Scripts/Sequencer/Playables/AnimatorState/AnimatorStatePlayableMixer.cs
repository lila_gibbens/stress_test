﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    [Serializable]
    public class AnimatorStatePlayableMixer : PlayableBehaviour
    {
        private PlayableDirector director;
        private IEnumerable<TimelineClip> clips;

        private double previousTime = 0;

        public void SetTrackDetails(PlayableDirector director, IEnumerable<TimelineClip> clips)
        {
            this.director = director;
            this.clips = clips;
        }
        
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (!Application.isPlaying)
            {
                return;
            }

            Animator animator = playerData as Animator;

            if (!animator)
            {
                return;
            }

            int input = 0;
            foreach (TimelineClip clip in clips)
            {
                ScriptPlayable<AnimatorStatePlayableBehaviour> inputPlayable = (ScriptPlayable<AnimatorStatePlayableBehaviour>)playable.GetInput(input);
                AnimatorStatePlayableBehaviour behaviour = inputPlayable.GetBehaviour();
                
                if (behaviour != null)
                {
                    if (director.time >= clip.start && previousTime < clip.start)
                    {
                        if (behaviour.stateController == null || !behaviour.stateController.Initialized)
                        {
                            behaviour.Initialize(animator);
                        }

                        bool runTriggers = director.time < clip.end;
                        behaviour.stateController.Run(runTriggers);
                    }
                }
                ++input;
            }

            previousTime = director.time;
        }
    }
}