using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Within.SuperBloom
{
    [Serializable]
    public class AnimatorStatePlayableBehaviour : PlayableBehaviour
    {
        [Tooltip("The parameters you want to change and their new values. For triggering a Trigger Parameter, just leave the Value empty.")]
        [SerializeField] private AnimatorStateController.AnimatorParameter[] parametersToSet;
        
        [NonSerialized] public AnimatorStateController stateController = null;
        
        public void Initialize(Animator animator)
        {
            stateController = new AnimatorStateController(animator, parametersToSet);
        }
    }
}