using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;
using Within.SuperBloom.Audio;

namespace Within.SuperBloom
{
    [Serializable]
    public class PlayASound_PlayableBehaviour : PlayableBehaviour
    {
        [Tooltip("The AudioClip you want to play. If it's null, it will throw an Exception in runtime. ")]
        public AudioClip ClipToPlay;

        [Tooltip("The volume to play the sound at. ")]
        public float Volume = 1;

        [Tooltip("Delay before playing the sound. This is particularly useful if you are using Music Duck to allow the music to lower before playing this sound.")]
        public float DelayOnPlaying = 0;

        [Tooltip("If true, this will duck any Background Music that is currently playing. When all the sounds that made the BGM to duck finish, the music will go back to the original volume.")]
        public bool DoMusicDuck = false;

        [Tooltip("Duration of the Duck Effect. ")]
        public float MusicDuckTime = 1f;

        [Tooltip("If true, it will ignore multiple calls of this PlayableClip that can happen, for instance, when using FlowControl Loops.")]
        public bool IgnoreTimelineLoops = true;

        [HideInInspector]
        public bool AlreadyPlayed = false;

        [HideInInspector]
        public float PreviousWeight = 0;

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (!Application.isPlaying)
            {
                return;
            }

            int inputCount = playable.GetInputCount();

            for (int i = 0; i < inputCount; i++)
            {
                float inputWeight = playable.GetInputWeight(i);

                ScriptPlayable<PlayASound_PlayableBehaviour> inputPlayable = (ScriptPlayable<PlayASound_PlayableBehaviour>)playable.GetInput(i);
                PlayASound_PlayableBehaviour behaviour = inputPlayable.GetBehaviour();
                
                if (behaviour.PreviousWeight < 0.5f && inputWeight >= 0.5f)
                {
                    OnEnterClip(behaviour);
                }

                behaviour.PreviousWeight = inputWeight;
            }
        }

        private void OnEnterClip(PlayASound_PlayableBehaviour behaviour)
        {
            if (behaviour.AlreadyPlayed && behaviour.IgnoreTimelineLoops)
            {
                return;
            }

            if (behaviour.ClipToPlay == null)
            {
                 throw new NullReferenceException("[PlayASound PlayableBehaviour] Please fill the <Clip to Play> field");
            }

            AudioSourceOptions options = new AudioSourceOptions();
            options.AllowMultiple = true;
            options.Spatialize = false;
            options.Volume = behaviour.Volume;
            options.ClipToPlay = behaviour.ClipToPlay;
            options.DelayOnPlaying = behaviour.DelayOnPlaying;
            options.DoMusicDuck = behaviour.DoMusicDuck;
            options.MusicDuckTime = behaviour.MusicDuckTime;
            
            string instanceID = behaviour.ClipToPlay.name;
            
            GlobalAudioPlayer.Instance.PlayAudio(instanceID, options);
        }
    }
}