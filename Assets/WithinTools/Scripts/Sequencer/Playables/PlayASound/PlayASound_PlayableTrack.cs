using System.Linq;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    [TrackColor(0.5514706f, 0f, 0f)]
    [TrackClipType(typeof(PlayASound_PlayableClip))]
    public class PlayASound_PlayableTrack : TrackAsset
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            var playable = ScriptPlayable<PlayASound_PlayableBehaviour>.Create(graph, inputCount);
            return playable;
        }
    }
}