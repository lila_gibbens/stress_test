﻿public class TimelineFlowControl
{
	private static TimelineFlowControl instance;
	public static TimelineFlowControl Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new TimelineFlowControl();
			}
			return instance;
		}
	}
	
	/// <summary>
	/// Skip counters are reset to 0 each time a new page starts.
	/// </summary>
	private int skipCounters;
	public int SkipCounters
	{
		get { return skipCounters; }
	}

	public void SetSkipCounter(int qty)
	{
		skipCounters = qty;
	}

	public void AddSkipCounters(int qty)
	{
		skipCounters += qty;
	}

	public void RemoveSkipCounters(int qty)
	{
		skipCounters -= qty;

		if (skipCounters < 0)
		{
			skipCounters = 0;
		}
	}
}
