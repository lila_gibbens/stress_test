using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    [TrackColor(0.8161765f, 0.03600777f, 0.5363919f)]
    [TrackClipType(typeof(FlowControlPlayableClip))]
    public class FlowControlPlayableTrack : TrackAsset
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            var playable = ScriptPlayable<DefaultPlayableMixer>.Create(graph, inputCount);
            playable.GetBehaviour().SetTrackDetails(go.GetComponent<PlayableDirector>(), GetClips(), false);

            foreach (var clip in GetClips())
            {
                FlowControlPlayableClip playableClip = clip.asset as FlowControlPlayableClip;
                playableClip.StartsAt = clip.start;
                playableClip.EndsAt = clip.end;

                clip.displayName = "Loop Clip";
            }
            return playable;
        }
    }
}