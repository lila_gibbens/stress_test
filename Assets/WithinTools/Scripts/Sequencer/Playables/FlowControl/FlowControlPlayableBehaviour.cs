using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Within.SuperBloom
{
    [Serializable]
    public class FlowControlPlayableBehaviour : TimelinePlayableBehaviour
    {
        [HideInInspector]
        public FlowControlPlayableClip Clip;

        [SerializeField]
        private int countersRequired = 1;

        protected override void OnClipExited(Playable playable, object playerData)
        {
            base.OnClipExited(playable, playerData);
            
            // Don't loop if the SkipCounters are equal-less than 0
            if (TimelineFlowControl.Instance.SkipCounters >= countersRequired)
            {
                // consume required skipcounter and exit the loop
                TimelineFlowControl.Instance.RemoveSkipCounters(countersRequired);
                return;
            }

            PlayableDirector director = (playable.GetGraph().GetResolver() as PlayableDirector);
            director.time = Clip.StartsAt;
        }
    }
}