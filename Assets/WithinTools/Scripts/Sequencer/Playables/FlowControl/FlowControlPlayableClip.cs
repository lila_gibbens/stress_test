using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    [Serializable]
    public class FlowControlPlayableClip : PlayableAsset, ITimelineClipAsset
    {
        public FlowControlPlayableBehaviour properties = new FlowControlPlayableBehaviour();
        
        [HideInInspector]
        public double StartsAt;
        
        [HideInInspector]
        public double EndsAt;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Unity Timeline library specifies the name via ITimelineClipAsset")]
        public ClipCaps clipCaps
        {
            get { return ClipCaps.None; }
        }

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<FlowControlPlayableBehaviour>.Create(graph, properties);
            FlowControlPlayableBehaviour clone = playable.GetBehaviour();
            clone.Clip = this;
            return playable;
        }
    }
}