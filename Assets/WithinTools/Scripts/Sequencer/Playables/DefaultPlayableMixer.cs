﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    public class TimelinePlayableBehaviour : PlayableBehaviour
    {
        [NonSerialized]
        public bool Entered = false;

        public void ClipEntered(Playable playable, object playerData)
        {
            OnClipEntered(playable, playerData);
            Entered = true;
        }

        public void ClipExited(Playable playable, object playerData)
        {
            OnClipExited(playable, playerData);
            Entered = false;
        }

        public virtual void OnTimelinePlay()
        {
        }
        
        protected virtual void OnClipEntered(Playable playable, object playerData)
        {
        }

        protected virtual void OnClipExited(Playable playable, object playerData)
        {
        }
    }
    
    public class DefaultPlayableMixer : PlayableBehaviour
    {
        private PlayableDirector director;
        private IEnumerable<TimelineClip> clips;

        private double previousTime = -1;
        private bool stateTracked = true;
        private bool playing = false;

        public void SetTrackDetails(PlayableDirector director, IEnumerable<TimelineClip> clips, bool stateTracked = true)
        {
            this.director = director;
            this.clips = clips;
            this.stateTracked = stateTracked;
        }
        
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (!Application.isPlaying)
            {
                return;
            }
            
            bool started = false;
            if (playable.GetPlayState() == PlayState.Playing)
            {
                if (!playing)
                {
                    playing = true;
                    started = true;
                }
            }
            else
            {
                playing = false;
            }
            int input = 0;
            foreach (TimelineClip clip in clips)
            {
                ScriptPlayable<TimelinePlayableBehaviour> inputPlayable = (ScriptPlayable<TimelinePlayableBehaviour>)playable.GetInput(input++);
                TimelinePlayableBehaviour behaviour = inputPlayable.GetBehaviour();

                if (behaviour == null)
                {
                    continue;
                }

                if (started)
                {
                    behaviour.OnTimelinePlay();
                }

                // Logically, we want to enter the clip if we've come from before the clip and are now inside the clip.
                // However, there are a couple of other cases we need to cover:
                // 1. Chapter Skip: To support chapter skipping, we need any clips that change state to perform any behaviour that 
                //    they would normally do if we skip PAST the clip. Most clips should behave that way but there are some exceptions
                //    (page management clips for example) where stateTracked is set to false.
                // 2. Loops/Flow Control: When a loop happens, the time effectively goes backwards for a frame. In this case we want
                //    to 'enter' the clip if we were outside the clip and are now inside the clip. If we were inside the clip when we
                //    looped though, we don't need to enter.
                bool loopedFromOutsideToInsideClip = previousTime >= director.time && !behaviour.Entered && director.time < clip.end;
                if (director.time > clip.start && (previousTime <= clip.start || loopedFromOutsideToInsideClip))
                {
                    if (stateTracked || director.time < clip.end)
                    {
                        // TODO: Kris - I think there is an issue where this is called twice in some cases.
                        behaviour.ClipEntered(playable, playerData);
                    }
                }

                if (director.time > clip.end && previousTime <= clip.end && behaviour.Entered)
                {
                    behaviour.ClipExited(playable, playerData);
                }
            }

            previousTime = director.time;
        }
    }
}