﻿using Within.SuperBloom.StateManagement;

namespace Within.SuperBloom
{
    public interface IStateTracked
    {
        IStateManager GetTrackState(UnityEngine.Object binding);
    }
}