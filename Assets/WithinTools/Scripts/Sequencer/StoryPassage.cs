﻿using System;
using UnityEngine;

namespace Within.SuperBloom
{
    [Serializable]
    public class StoryPassage
    {
        [SerializeField]
        private string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public StoryPassage(string _text)
        {
            text = _text;
        }
    }
}