﻿using UnityEngine.Events;
using UnityEngine.UI;

namespace Within.SuperBloom
{
    public class TimelineGraphEvents
    {
        public class TimelineEvent_OnLoopClipEnded : UnityEvent<FlowControlPlayableBehaviour> { }
        public class TimelineEvent_OnLoopClipStarted : UnityEvent<FlowControlPlayableBehaviour> { }

        public static TimelineEvent_OnLoopClipEnded OnLoopClipEnded = new TimelineEvent_OnLoopClipEnded();
        public static TimelineEvent_OnLoopClipStarted OnLoopClipStarted = new TimelineEvent_OnLoopClipStarted();

        public static UnityEvent OnFinishLoopRequested = new Button.ButtonClickedEvent();
    }
}