﻿using System;

namespace Within.SuperBloom
{
    public static class StoryModeOptions
    {
        public static string storyBundleName = String.Empty;

        public static EStoryPlayMode playMode = EStoryPlayMode.None;

        public static StoryDataMetaInfo metaInfo = null;

        public static void Reset()
        {
            storyBundleName = String.Empty;
            playMode = EStoryPlayMode.None;
            metaInfo = null;
        }

        public static bool IsRunningFromBundle
        {
            get
            {
                return storyBundleName != String.Empty;
            }
        }
    }
}