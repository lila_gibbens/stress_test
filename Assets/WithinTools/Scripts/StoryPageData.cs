﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

namespace Within.SuperBloom
{
    /// <summary>
    /// Story Pages collectively make up a story
    /// each StoryPageData asset created defines a Story Page
    /// </summary>
    
    [Serializable]
    public class StoryPageData : ScriptableObject
    {
        public TimelineAsset Sequence;
        public StoryPassage Passage;
        public string PageName;
        public int Index;
        
        [SerializeField]
        private List<AudioClip> preRecordedClips;

        public List<AudioClip> PreRecordedClips
        {
            get
            {
                if (preRecordedClips == null)
                {
                    preRecordedClips = new List<AudioClip>();
                }

                return preRecordedClips;
            }
        }

        public void UpdateClips()
        {
            int count = Passage.Text.Split(new char[] { '|' }, StringSplitOptions.None).Length;

            while (preRecordedClips.Count < count)
            {
                preRecordedClips.Add(null);
            }

            while (preRecordedClips.Count > count)
            {
                preRecordedClips.RemoveAt(preRecordedClips.Count - 1);
            }
        }

        public int PartsQty
        {
            get
            {
                return preRecordedClips.Count; 
            }
        }

        public string GetAssetPath()
        {
            return string.Format("{0}{1}.asset", SBToolsPaths.STORY_PAGES_DATA_PATH, PageName); 
        }
        
        public string GetTimelineAssetPath()
        {
            return string.Format("{0}{1}_Timeline.asset", SBToolsPaths.STORY_PAGES_DATA_PATH, PageName); 
        }

        public StoryPageData GetClone()
        {
            StoryPageData clone = ScriptableObject.CreateInstance<StoryPageData>();
            
            clone.Index    = this.Index;
            clone.Passage = this.Passage;
            clone.Sequence = this.Sequence;
            clone.PageName = this.PageName;

            clone.preRecordedClips = new List<AudioClip>(this.PreRecordedClips);
            
            return clone;
        }
    }
}