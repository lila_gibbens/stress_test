﻿using System.Collections.Generic;
using System.Linq;
using UnityARInterface;
using UnityEngine;
using UnityEngine.Assertions;

namespace Within.SuperBloom
{
    /// <summary>
    /// The PlaneManager is responsible for tracking AR planes as reported by the ARInterface
    /// and mapping them to ARPlanes in the Unity scene.
    /// </summary>
    public class ARPlaneManager : MonoBehaviour, IAnchorManager
    {
        // TODO: Zenject?
        private ARController arController;
        [SerializeField] private GameObject arPlanePrefab;

        public int FeatureCount { get { return planes.Count; } }

        // PlaneID, ARPlane
        private Dictionary<string, ARPlane> planes = new Dictionary<string, ARPlane>();
        private bool isShowingAnchors = true;

        private void Awake()
        {
            arController = GetComponentInParent<ARController>();
            Assert.IsNotNull(arController, "AR controller must be on parent");
            Assert.IsNotNull(arPlanePrefab, "AR plane prefab has not been set");
        }

        private void OnEnable()
        {
            ARInterface.planeAdded += AddPlane;
            ARInterface.planeRemoved += RemovePlane;
            ARInterface.planeUpdated += UpdatePlane;
            Debug.Log("ARPlaneManager: Initialized");
        }

        private void OnDisable()
        {
            ARInterface.planeAdded -= AddPlane;
            ARInterface.planeRemoved -= RemovePlane;
            ARInterface.planeUpdated -= UpdatePlane;
        }

        private void AddPlane(BoundedPlane nativePlane)
        {
            CreateOrUpdate(nativePlane);            
        }

        private void RemovePlane(BoundedPlane nativePlane)
        {
            RemovePlaneWithId(nativePlane.id);
        }

        private void RemovePlaneWithId(string id)
        {
            if (planes.ContainsKey(id))
            {
                ARPlane arPlaneToDestroy = planes[id];
                planes.Remove(id);
                Destroy(arPlaneToDestroy.gameObject);
            }
        }

        private void UpdatePlane(BoundedPlane nativePlane)
        {
            CreateOrUpdate(nativePlane);
        }

        private void CreateOrUpdate(BoundedPlane nativePlane)
        {
            if (!planes.ContainsKey(nativePlane.id))
            {
                CreateARPlane(nativePlane);
            }
            else
            {
                ARPlane arPlaneToUpdate = planes[nativePlane.id];
                arPlaneToUpdate.NativePlaneData = nativePlane;
            }
        }

        private void CreateARPlane(BoundedPlane nativePlane)
        {
            GameObject newARPlaneGO = Instantiate<GameObject>(arPlanePrefab);
            newARPlaneGO.transform.SetParent(gameObject.transform);

            ARPlane newPlane = newARPlaneGO.GetComponent<ARPlane>();
            newPlane.NativePlaneData = nativePlane;

            planes.Add(newPlane.NativePlaneData.id, newPlane);
            newARPlaneGO.SetActive(isShowingAnchors);
        }

        /// <summary>
        /// Show/Hide the known planes.
        /// </summary>
        /// <param name="enable"></param>
        public void ShowAnchors(bool enable)
        {
            isShowingAnchors = enable;

            foreach (ARPlane plane in planes.Values)
            {
                plane.gameObject.SetActive(enable);
            }
        }

        public void Reset()
        {
            string[] trackedPlaneIds = planes.Keys.ToArray();
            foreach (string nativePlaneId in trackedPlaneIds)
            {
                RemovePlaneWithId(nativePlaneId);
            }
        }
    }
}
