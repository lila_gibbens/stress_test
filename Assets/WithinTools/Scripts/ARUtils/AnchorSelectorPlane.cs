﻿using UnityEngine;

namespace Within.SuperBloom
{
    /// <summary>
    /// Responsible for selecting an ARPlane to use as the play area.
    /// </summary>
    public class AnchorSelectorPlane : AnchorSelector
    {
        private const float MAX_RAYCAST_DISTANCE = 1000000;
        
        public override Anchor TrySelectAnchor()
        {
            Anchor selectedPlane = null;

            if (Input.touchSupported)
            {
                for (int i = 0; i < Input.touchCount; ++i)
                {
                    Touch touch = Input.GetTouch(i);
                    if (touch.phase == TouchPhase.Began)
                    {
                        selectedPlane = CastRayForPlane(touch.position);
                        if (selectedPlane != null)
                        {
                            break;
                        }
                    }
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    selectedPlane = CastRayForPlane(Input.mousePosition);
                }
            }

            return selectedPlane;
        }

        private Anchor CastRayForPlane(Vector2 screenLocation)
        {
            ARPlane selectedPlane = null;
            Ray ray = Camera.main.ScreenPointToRay(screenLocation);
            RaycastHit rayHit;
            if (Physics.Raycast(ray, out rayHit, MAX_RAYCAST_DISTANCE))
            {
                selectedPlane = rayHit.collider.GetComponentInParent<ARPlane>();
            }

            return selectedPlane;
        }

        protected override IAnchorManager FindAnchorManager()
        {
            return FindObjectOfType<ARPlaneManager>();
        }
    }
}
