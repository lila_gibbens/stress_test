﻿using UnityARInterface;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace Within.SuperBloom
{
    /// <summary>
    /// The ARPlane is responsible for providing a representation of a native ARPlane
    /// in the Unity scene and describing how to map StoryAreas to those planes.
    /// </summary>
    public class ARPlane : Anchor
    {
        public class ARPlaneUpdatedEvent : UnityEvent<ARPlane> { }
        public readonly ARPlaneUpdatedEvent PlaneUpdated = new ARPlaneUpdatedEvent();

        private MeshRenderer visualization;

        private BoundedPlane nativePlaneData;
        public BoundedPlane NativePlaneData
        {
            get
            {
                return nativePlaneData;
            }
            set
            {
                nativePlaneData = value;

                // Update visual for plane.
                gameObject.transform.localPosition = nativePlaneData.center;
                gameObject.transform.localRotation = nativePlaneData.rotation;
                gameObject.transform.localScale = new Vector3(nativePlaneData.extents.x, 1f, nativePlaneData.extents.y);

                // Event - plane has been updated
                PlaneUpdated.Invoke(this);
            }
        }

        public float AspectRatio { get { return NativePlaneData.width / NativePlaneData.height; } }
        
        protected override void Start()
        {
            base.Start();

            visualization = GetComponentInChildren<MeshRenderer>();
            Assert.IsNotNull(visualization, "MeshRenderer for visualizing feature is missing");
            Assert.IsFalse(string.IsNullOrEmpty(nativePlaneData.id), "Native plane data has not been set. NativePlaneData must be set immediately after creating an ARPlane");
        }

        /// <summary>
        /// Toggles visibility of visualization (does not disable colliders)
        /// </summary>
        /// <param name="show"></param>
        public void ShowVisualization(bool show)
        {
            visualization.enabled = show;
        }

        /// <summary>
        /// Fit the StoryAreaPlane to the ARPlane
        /// </summary>
        /// <param name="storyAreaPlane"></param>
        /// <param name="arCamera"></param>
        public override void Align(StoryAreaPlane storyAreaPlane, Camera arCamera)
        {
            // Reset scale of controller before attempting alignment. 
            // If this is not done then previous scale of controller may result in incorrect alignment 
            // and scaling. Scale can be non '1' if the controller has previously been aligned (for example, due
            // to reinitialising the AR system.
            arController.scale = 1;

            // Position plane and rotate so it is axis aligned
            arController.AlignWithPointOfInterest(transform.position);
            arController.rotation = transform.localRotation;
            
            Vector3 directionToCamera = Camera.main.transform.position - transform.position;

            // In the case of two valid alignment sides we need to make sure that long sides are aligned
            if ((storyAreaPlane.XAxisFrontSide != StoryAreaPlane.AxisDirection.None)
                && (storyAreaPlane.ZAxisFrontSide != StoryAreaPlane.AxisDirection.None))
            {
                float aspectRatioInWorld = AspectRatioInWorld();
                if ((aspectRatioInWorld < 1 && storyAreaPlane.AspectRatio > 1)
                    || (aspectRatioInWorld > 1 && storyAreaPlane.AspectRatio < 1))
                {
                    arController.rotation *= Quaternion.AngleAxis(90, Vector3.up);

                    // As we have rotated clockwise: z+ is now in the x+ direction and x+ is in the z- direction
                    // so we need to modify the directionToCamera so the axis flips in the next block of code work correctly
                    // Note: may still be 180 degrees off at this point, that is handled by RotateToAlign in the next code block.
                    float z = directionToCamera.z;
                    directionToCamera.z = directionToCamera.x;
                    directionToCamera.x = -z;
                }
            }

            // Now we can handle rotation to face the camera (as close as possible while remaining aligned with the play area (world) axis)
            if (Mathf.Abs(directionToCamera.x) > Mathf.Abs(directionToCamera.z))
            {
                // Camera on one of the x sides
                RotateToAlign(storyAreaPlane.XAxisFrontSide, storyAreaPlane.ZAxisFrontSide, directionToCamera.x, true);
            }
            else
            {
                // Camera on one of the z sides
                RotateToAlign(storyAreaPlane.ZAxisFrontSide, storyAreaPlane.XAxisFrontSide, directionToCamera.z, false);
            }

            // Scale plane to make best use of table space now that it is oriented within the side restrictions            
            if (AspectRatioInWorld() >= storyAreaPlane.AspectRatio)
            {
                // fit by height
                arController.scale = storyAreaPlane.Height / HeightInWorld(); 
            }
            else
            {
                // fit by width
                arController.scale = storyAreaPlane.Width / WidthInWorld();
            }            
        }

        /// <summary>
        /// Note: This value is only correct when ARPlane is axis aligned with the world
        /// </summary>
        /// <returns></returns>
        private float AspectRatioInWorld()
        {
            return visualization.bounds.size.x / visualization.bounds.size.z;
        }

        /// <summary>
        /// Note: This value is only correct when ARPlane is axis aligned with the world
        /// </summary>
        /// <returns></returns>
        private float WidthInWorld()
        {
            return visualization.bounds.size.x;
        }

        /// <summary>
        /// Note: This value is only correct when ARPlane is axis aligned with the world
        /// </summary>
        /// <returns></returns>
        private float HeightInWorld()
        {
            return visualization.bounds.size.z;
        }

        /// <summary>
        /// Rotates so that a valid side is facing towards the camera
        /// </summary>
        /// <param name="frontSideForCameraAxis"></param>
        /// <param name="frontSideForOtherAxis"></param>
        /// <param name="axisDirectionToCamera"></param>
        /// <param name="isCameraOnXAxis">if axisDirectionToCamera is on the x axis then set this to true. Otherwise (camera on the z
        /// axis) set this false </param>
        private void RotateToAlign(StoryAreaPlane.AxisDirection frontSideForCameraAxis, StoryAreaPlane.AxisDirection frontSideForOtherAxis, 
            float axisDirectionToCamera, bool isCameraOnXAxis)
        {
            // Camera on one of the sides
            // If we have an alignment side for that axis
            if (frontSideForCameraAxis != StoryAreaPlane.AxisDirection.None)
            {
                // We have an alignment for the side but the alignment is on the opposite side from the camera
                if (((frontSideForCameraAxis == StoryAreaPlane.AxisDirection.Positive) && axisDirectionToCamera < 0)
                    || ((frontSideForCameraAxis == StoryAreaPlane.AxisDirection.Negative) && axisDirectionToCamera >= 0))
                {
                    // spin 180 to match alignment
                    arController.rotation *= Quaternion.AngleAxis(180, Vector3.up);
                }
            }
            else // align the other axis front to the camera side
            {
                // So we rotate in the correct direction for the axis
                StoryAreaPlane.AxisDirection directionToCamLessThanZero = StoryAreaPlane.AxisDirection.Negative;
                StoryAreaPlane.AxisDirection directionToCamGreaterThanZero = StoryAreaPlane.AxisDirection.Positive;
                if (isCameraOnXAxis)
                {
                    directionToCamLessThanZero = StoryAreaPlane.AxisDirection.Positive;
                    directionToCamGreaterThanZero = StoryAreaPlane.AxisDirection.Negative;
                }
                
                // We are 90 degrees off axis clockwise
                if (((frontSideForOtherAxis == directionToCamLessThanZero) && axisDirectionToCamera < 0)
                    || ((frontSideForOtherAxis == directionToCamGreaterThanZero) && axisDirectionToCamera >= 0))
                {
                    // spin -90 to align
                    arController.rotation *= Quaternion.AngleAxis(-90, Vector3.up);
                }
                else
                {
                    arController.rotation *= Quaternion.AngleAxis(90, Vector3.up);
                }
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawLine(Camera.main.transform.position, transform.position);
        }
    }
}
