﻿using UnityARInterface;
using UnityEngine;
using UnityEngine.Assertions;

namespace Within.SuperBloom
{
    /// <summary>
    /// The root that contains the camera is the primary AR root. Any other AR root is a secondary
    /// </summary>
    public class SecondaryARRoot : MonoBehaviour
    {
        private ARController currentlyFollowing;

        public void FollowCameraRoot(ARController controller)
        {
            Assert.IsTrue(controller.arCamera.transform.parent.parent == null, "Root of camera is expected to be a top level object");
            Assert.IsTrue(controller.arCamera.transform.parent.parent == null, "This proxy root is expected to be a top level object");

            if (currentlyFollowing != null && currentlyFollowing != controller)
            {
                currentlyFollowing.rootUpdated.RemoveListener(UpdateTransform);
            }

            currentlyFollowing = controller;

            if (currentlyFollowing != null)
            {
                currentlyFollowing.rootUpdated.AddListener(UpdateTransform);
            }
        }

        private void UpdateTransform(Transform t)
        {
            transform.position = t.position;
            transform.rotation = t.rotation;
            transform.localScale = t.localScale;
        }
    }
}