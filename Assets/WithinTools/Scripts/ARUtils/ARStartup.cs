﻿using System.Collections;
using UnityARInterface;
using UnityEngine;
using UnityEngine.Assertions;

namespace Within.SuperBloom
{
    /// <summary>
    /// ARManager is responsible for waiting for the AR system to initialize and 
    /// managing the AR configuration flow.
    /// </summary>
    [RequireComponent(typeof(ARController))]
    public class ARStartup : MonoBehaviour
    {
        [Tooltip("Only works in editor.")]
        [SerializeField] private bool alwaysShowAnchorsInEditor = false;
        private ARController arController;
        private IAnchorManager[] anchorManagers;
        private StoryArea storyArea = null;
        private bool startupSequenceGuard = false;

        private void Awake()
        {
            arController = GetComponent<ARController>();
            anchorManagers = GetComponentsInChildren<IAnchorManager>();
            Assert.IsTrue(anchorManagers != null && anchorManagers.Length != 0, "No anchor managers found. They should be children of the ARRoot");
        }

        private void OnEnable()
        {
            StartCoroutine(StartupSequence());
        }

        private void OnDisable()
        {
            StopAllCoroutines();
            startupSequenceGuard = false;

            if (arController != null)
            {
                arController.enabled = false;
            }

            if (anchorManagers != null)
            {
                foreach (IAnchorManager anchorManager in anchorManagers)
                {
                    anchorManager.Reset();
                }
            }

            // Pause playback and hide Story content
            if ((storyArea != null) && (storyArea.gameObject != null))
            {
                ShowStoryArea(false);
            }
        }

#if !UNITY_EDITOR
        private void OnApplicationPause(bool paused)
        {
            if (paused)
            {
                OnDisable();
            }
            else
            {
                OnEnable();
            }
        }
#endif

        private IEnumerator StartupSequence()
        {
            if (startupSequenceGuard)
            {
                Debug.Log("[ARStartup] Startup already in progress, ignored call to StartupSequence");
                yield break;
            }

            startupSequenceGuard = true;
            Debug.Log("[ARStartup] Startup Sequence...");

            yield return StartCoroutine(WaitForARService());

            // TODO: Better place for this? We want to request around the same time as camera request is made.
            MicrophoneRequest.Request();

            // Make sure AR scene root maintains orientation that matches camera to keep AR elements in AR scene correctly positioned
            // This is needed as we have the camera in the main scene (so content creators can potentially modify/add FX to it at some point) 
            // rather than as a child of the ARScene ARRoot. 
            if (arController.gameObject.GetComponent<SecondaryARRoot>() == null)
            {
                SecondaryARRoot followCameraRoot = arController.gameObject.AddComponent<SecondaryARRoot>();
                followCameraRoot.FollowCameraRoot(arController);
            }

            yield return StartCoroutine(WaitForStoryScene());

            if (storyArea == null)
            {
                storyArea = SBSceneLocator.StoryHierarchy.GetComponentInChildren<StoryArea>(true);
                Assert.IsNotNull(storyArea, "No story areas found. Please ensure there is one StoryArea in the story scene.");
            }

            // Assign story areas to AR anchors
            yield return StartCoroutine(SelectAnchorForStoryArea(storyArea));

            ShowStoryArea(true);

            Debug.Log("[ARStartup] Startup Sequence completed");
            Debug.Log("[ARStartup] Dispatching: Ready to Start");

            SequencerControlDispatcher.OnReadyToStart.Invoke();

            startupSequenceGuard = false;
        }

        private IEnumerator WaitForARService()
        {
            // Wait for AR to initialize
            arController.enabled = true;

            while (!arController.IsRunning)
            {
                yield return null;
            }

            Debug.Log("[ARStartup] ARInterface ready.");
        }

        private IEnumerator WaitForStoryScene()
        {
            // Wait for story scene
            while (StorySequencer.Instance == null || (StorySequencer.Instance != null && !StorySequencer.Instance.IsReady))
            {
                yield return null;
            }

            Debug.Log("[ARStartup] Story Scene Ready");
        }

        private IEnumerator SelectAnchorForStoryArea(StoryArea storyArea)
        {
            Debug.Log("[ARStartup] Selecting Story Area for " + storyArea.name);

            storyArea.AnchorSelector.AnchorManager.ShowAnchors(true);
            bool assigned = false;

            while (!assigned)
            {
                Anchor selectedAnchor = storyArea.AnchorSelector.TrySelectAnchor();
                if (selectedAnchor != null)
                {
                    storyArea.AlignToAnchor(selectedAnchor, arController.arCamera);
                    assigned = true;
                }

                yield return null;
            }

            Debug.Log("[ARStartup] Selected Story Area for " + storyArea.name);

            if (!Application.isEditor || !alwaysShowAnchorsInEditor)
            {
                storyArea.AnchorSelector.AnchorManager.ShowAnchors(false);
            }
        }

        private void ShowStoryArea(bool show)
        {
            if (StorySequencer.Instance != null)
            {
                if (show)
                {
                    StorySequencer.Instance.Unpause();
                }
                else
                {
                    StorySequencer.Instance.Pause();
                }
            }
            storyArea.gameObject.SetActive(show);
            SequencerFlowEvents.storyVisibilityChanged.Invoke(show);
        }
    }
}
