﻿using UnityARInterface;
using UnityEngine;
using UnityEngine.Assertions;

namespace Within.SuperBloom
{
    /// <summary>
    /// An item from the real world that the AR library provides as a reference.
    /// Only a plane is currently implemented, but in the future images or other Anchors/Features
    /// could be used.
    /// </summary>
    public abstract class Anchor : MonoBehaviour
    {
        /// <summary>
        /// Note: Not available until after Start()
        /// </summary>
        protected ARController arController;

        /// <summary>
        /// Sub classes must call base.Start() when overriding before doing anything else
        /// </summary>
        protected virtual void Start()
        {
            arController = GetComponentInParent<ARController>();
            Assert.IsNotNull(arController, "Anchors need a reference to the AR Controller. It is expected that the AR controller is a parent/grandparent of all Anchors");
        }

        /// <summary>
        /// Sub classes must implement alignment for StoryAreas. New overloads should be added to Anchor
        /// for new types of story area.
        /// </summary>
        /// <param name="storyAreaPlane"></param>
        /// <param name="arCamera"></param>
        public abstract void Align(StoryAreaPlane storyAreaPlane, Camera arCamera);
    }
}