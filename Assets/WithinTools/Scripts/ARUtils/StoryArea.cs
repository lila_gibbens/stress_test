﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Within.SuperBloom
{
    /// <summary>
    /// This will define the 2D area containing all the story elements in the Unity Scene
    /// that the story creator wants to fit on the real world surface.
    /// 
    /// Subclasses should be made for different surface shapes and Anchor and subclasses
    /// must be updated so that they know how to map the StoryArea[Shape] to themselves.
    /// </summary>
    public abstract class StoryArea : MonoBehaviour
    {
        private AnchorSelector anchorSelector;
        /// <summary>
        /// Stories must have an Anchor selector. By having the selector as a separate component it
        /// allows the play area to be anchored to different anchors based on the component the author
        /// chooses to use.
        /// </summary>
        public AnchorSelector AnchorSelector
        {
            get
            {
                if (anchorSelector == null)
                {
                    anchorSelector = GetComponent<AnchorSelector>();
                    Assert.IsNotNull(anchorSelector, "StoryAreas require an AnchorSelector on the same gameobject");
                }
                return anchorSelector;
            }

            protected set
            {
                anchorSelector = value;
            }
        }

        /// <summary>
        /// subclasses should implement as anchor.Align(this); This is needed so correct type is used
        /// for alignment in the Anchor.
        /// </summary>
        /// <param name="anchor"></param>
        /// <param name="arCamera"></param>
        public abstract void AlignToAnchor(Anchor anchor, Camera arCamera);

        /// <summary>
        /// Overriding subclasses must call base.Awake()
        /// </summary>
        protected virtual void Awake()
        {
            gameObject.SetActive(false);
        }
    }
}
