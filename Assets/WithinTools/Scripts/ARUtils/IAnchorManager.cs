﻿
namespace Within.SuperBloom
{
    /// <summary>
    /// Common interface for AnchorManagers. Example anchor manager: ARPlaneManager
    /// </summary>
    public interface IAnchorManager
    {
        /// <summary>
        /// How many of this feature type are currently being tracked
        /// </summary>
        int FeatureCount { get; }

        /// <summary>
        /// Shows/hides the anchors
        /// </summary>
        /// <param name="enable"></param>
        void ShowAnchors(bool enable);

        /// <summary>
        /// Clear all anchors
        /// </summary>
        void Reset();
    }
}