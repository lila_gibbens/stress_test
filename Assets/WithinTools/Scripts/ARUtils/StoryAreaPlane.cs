﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Within.SuperBloom
{
    /// <summary>
    /// This defines the plane containing all the story elements in the Unity Scene
    /// that the story creator wants to fit on the real world surface.
    /// </summary>
    public class StoryAreaPlane : StoryArea
    {
        public enum AxisDirection
        {
            None = 0,
            Positive = 1,
            Negative = 2
        }

        [SerializeField] private float width = 20;
        [SerializeField] private float height = 20;
        [SerializeField] private AxisDirection zAxisFrontSide = AxisDirection.Negative;
        [SerializeField] private AxisDirection xAxisFrontSide = AxisDirection.Positive;

        public float AspectRatio { get { return width / height; } }
        public float Width { get { return width; } }
        public float Height { get { return height; } }
        public AxisDirection ZAxisFrontSide { get { return zAxisFrontSide; } }
        public AxisDirection XAxisFrontSide { get { return xAxisFrontSide; } }

        protected override void Awake()
        {
            Assert.IsTrue(transform.rotation == Quaternion.identity, "Story Area Plane must aligned with the world");
            Assert.IsTrue((ZAxisFrontSide != AxisDirection.None) || (XAxisFrontSide != AxisDirection.None), "One of the alignment axis must be set");

            base.Awake();
        }

        public void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(transform.position, new Vector3(width, 0, height));
        }

        public override void AlignToAnchor(Anchor anchor, Camera arCamera)
        {
            anchor.Align(this, arCamera);
        }
    }
}
