﻿using UnityEngine;

public class FollowTransform : MonoBehaviour
{
    public Transform ToFollow { get; set; }
    
    private void Update()
    {
        if (ToFollow != null)
        {
            transform.position = ToFollow.position;
            transform.rotation = ToFollow.rotation;
            transform.localScale = ToFollow.localScale;
        }
    }
}
