﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Within.SuperBloom
{
    /// <summary>
    /// Responsible for selecting an Anchor to use as the play area.
    /// </summary>
    [DisallowMultipleComponent]
    public abstract class AnchorSelector : MonoBehaviour
    {
        private IAnchorManager anchorManager;
        public IAnchorManager AnchorManager
        {
            get
            {
                if (anchorManager == null)
                {
                    anchorManager = FindAnchorManager();
                    Assert.IsNotNull(anchorManager, "AnchorManager is null. Where is the anchor manager?");
                }
                return anchorManager;
            }
        }

        /// <summary>
        /// Given a screen location tap/click this should return an anchor if there
        /// is one at the location.
        /// </summary>
        /// <returns></returns>
        public abstract Anchor TrySelectAnchor();
        
        /// <summary>
        /// Overriding classes must call base.Awake() before doing anything else.
        /// </summary>
        private void Awake()
        {
            anchorManager = FindAnchorManager();
        }

        /// <summary>
        /// Find and return appropriate anchor manager for subclass. Must not access AnchorManager property of AnchorSelector
        /// </summary>
        /// <returns></returns>
        protected abstract IAnchorManager FindAnchorManager();
    }
}
