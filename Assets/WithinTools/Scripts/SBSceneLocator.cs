﻿using UnityEngine;

namespace Within.SuperBloom
{	
	public static class SBSceneLocator 
	{		
		private static StoryContentContainer storyContentObject;
		public static StoryContentContainer StoryContentObject
		{
			get
			{
				if (storyContentObject == null)
				{
					storyContentObject = GameObject.FindObjectOfType<StoryContentContainer>();
				}
				return storyContentObject;
			}
		}
		
		private static StoryHierarchy storyHierarchy;
		public static StoryHierarchy StoryHierarchy
		{
			get
			{
				if (storyHierarchy == null)
				{
					storyHierarchy = GameObject.FindObjectOfType<StoryHierarchy>();
				}
				return storyHierarchy;
			}
		}
			
		private static StorySequencer storySequencerScript;
		public static StorySequencer StorySequencerScript
		{
			get
			{
				if (storySequencerScript == null)
				{
					storySequencerScript = GameObject.FindObjectOfType<StorySequencer>();
				}
				return storySequencerScript;
			}
		}
		
		private static GameObject pagesContainer;
		public static GameObject PagesContainer
		{
			get
			{
				if (pagesContainer == null)
				{
					pagesContainer = GameObject.Find("Pages");
				}
				return pagesContainer;
			}
		}
	}
}
