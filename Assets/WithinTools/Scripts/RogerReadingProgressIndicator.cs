﻿using TMPro;
using UnityEngine;

namespace Within.SuperBloom
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class RogerReadingProgressIndicator : MonoBehaviour
    {
        [SerializeField] private PassageText passage;

        [SerializeField] private Color readColor;
        [SerializeField] private Color currentWordColor;
        [SerializeField] private Color toReadColor;

        private TextMeshProUGUI text;
        private string[] words;

        private string startTagRead;
        private string startTagCurrent;
        private string startTagToRead;
        private string endTag;

        // Use this for initialization
        private void Start()
        {
            startTagRead = string.Format("<color=#{0}>", ColorUtility.ToHtmlStringRGB(readColor));
            startTagCurrent = string.Format("<color=#{0}>", ColorUtility.ToHtmlStringRGB(currentWordColor));
            startTagToRead = string.Format("<color=#{0}>", ColorUtility.ToHtmlStringRGB(toReadColor));
            endTag = "</color>";

            passage.textChanged.AddListener(OnTextChanged);
            text = GetComponent<TextMeshProUGUI>();
            SequencerFlowEvents.onWordsRead.AddListener(AdvanceWords);
            OnTextChanged(passage.Text);
        }
        
        private void OnTextChanged(string newText)
        {
            words = newText.Split();
            AdvanceWords(0);
        }

        private void AdvanceWords(int totalWords)
        {
            // Add color tags around words that have been matched by ROGER
            string result = startTagRead;
            int numWords = words.Length;
            for (int i = 0; i < numWords; ++i)
            {
                // The word we've just read
                if (totalWords == i + 1)
                {
                    result += endTag;
                    result += startTagCurrent;
                }
                // The next word
                else if (totalWords == i)
                {
                    result += endTag;
                    result += startTagToRead;
                }

                result += words[i] + " ";
            }

            result += endTag;
            text.text = result;
        }
    }
}