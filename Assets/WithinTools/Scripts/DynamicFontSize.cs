﻿using TMPro;
using UnityEngine;

namespace Within.SuperBloom
{
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class DynamicFontSize : MonoBehaviour
    {
        [SerializeField] private float fontScaler = 2.5f;
        [SerializeField] private float baseFontSize = 9.57f;

        private void Start()
        {
            RectTransform trans = (RectTransform)transform;
            float width = trans.rect.width;
            float height = trans.rect.height;
            float ratio = width / height;
            TextMeshProUGUI text = GetComponent<TextMeshProUGUI>();
            text.fontSize = fontScaler * ratio + baseFontSize;
        }
    }
}
