﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Within.SuperBloom
{
    public class StoryScenesLoader : MonoBehaviour
    {
        private static StoryScenesLoader instance;

        public static StoryScenesLoader Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject loaderObject = GameObject.Find("StorySceneLoader");
                    if (loaderObject == null)
                    {
                        loaderObject = new GameObject("StorySceneLoader");
                        DontDestroyOnLoad(loaderObject);
                        instance = loaderObject.AddComponent<StoryScenesLoader>();
                    }
                }
                return instance;
            }
        }

        private bool fromBundles;

        private string storyBundleName;

        private Scene loadingScreen_Scene;

        private string lastLoadedStoryBundle;

        public void LoadHomeScreen()
        {
            StartCoroutine(OpenHomeScreenRoutine());
        }

        private IEnumerator OpenHomeScreenRoutine()
        {
            AsyncOperation async = SceneManager.LoadSceneAsync(SBToolsPaths.LOADING_SCREEN_SCENE_NAME, LoadSceneMode.Single);
            yield return new WaitUntil(() => async.isDone);
            
            loadingScreen_Scene = SceneManager.GetActiveScene();
            
            // -- if we have a bundle loaded from before
            if (!String.IsNullOrEmpty(lastLoadedStoryBundle))
            {
                AssetBundle lastBundle = Resources.FindObjectsOfTypeAll<AssetBundle>().First(b => b.name == lastLoadedStoryBundle);
                lastBundle.Unload(true);
            }
            
            async = SceneManager.LoadSceneAsync(SBToolsPaths.HOME_SCREEN_SCENE_NAME, LoadSceneMode.Additive);

            yield return new WaitUntil(() => async.isDone);

            async = SceneManager.UnloadSceneAsync(loadingScreen_Scene);

            yield return new WaitUntil(() => async.isDone);
        }

        public void LoadStoryFromBundle(string _assetBundleName, bool _useDebugUI = false)
        {
            Debug.Log("[StoryScenesLoader] Load Story from bundle: " + _assetBundleName);

            fromBundles = true;
            storyBundleName = _assetBundleName;
            StartCoroutine(OpenLoadingScreenAndLoad());
        }

        public void LoadStoryFromBuildSettings(bool _useDebugUI = false)
        {
            Debug.Log("[StoryScenesLoader] Load Story Build Settings");

            fromBundles = false;
            StartCoroutine(OpenLoadingScreenAndLoad());
        }

        private IEnumerator OpenLoadingScreenAndLoad()
        {
            AsyncOperation async = SceneManager.LoadSceneAsync(SBToolsPaths.LOADING_SCREEN_SCENE_NAME, LoadSceneMode.Single);
            yield return new WaitUntil(() => async.isDone);

            loadingScreen_Scene = SceneManager.GetActiveScene();

            if (fromBundles)
            {
                yield return StartCoroutine(LoadEmbeddedStorySceneAsync());
            }
            else
            {
                yield return StartCoroutine(LoadStoryFromBuildSettings());
            }
        }

        private IEnumerator UnloadScenesBeforeLoading()
        {
            List<string> loadedScenes = new List<string>();

            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                loadedScenes.Add(SceneManager.GetSceneAt(i).name);
            }

            string[] scenesToUnload = new string[]
            {
            SBToolsPaths.SEQUENCER_FINAL_UI_SCENE_NAME,
            SBToolsPaths.AR_SCENE_NAME,
            SBToolsPaths.STORY_SCENE_NAME
            };

            Debug.Log("[StoryScenesLoader] Unloading scenes:");

            foreach (var sceneToRemove in scenesToUnload)
            {
                if (!loadedScenes.Contains(sceneToRemove))
                {
                    continue;
                }

                AsyncOperation async = SceneManager.UnloadSceneAsync(sceneToRemove);

                Debug.Log("[StoryScenesLoader] Unloading: " + sceneToRemove);

                yield return new WaitUntil(() => async == null || async.isDone);
            }
        }

        private IEnumerator LoadScenes(string _storySceneName)
        {
            AsyncOperation async = SceneManager.LoadSceneAsync(_storySceneName, LoadSceneMode.Additive);

            yield return new WaitUntil(() => async.isDone);

            async = SceneManager.LoadSceneAsync(SBToolsPaths.SEQUENCER_FINAL_UI_SCENE_NAME, LoadSceneMode.Additive);

            yield return new WaitUntil(() => async.isDone);

            async = SceneManager.LoadSceneAsync(SBToolsPaths.AR_SCENE_NAME, LoadSceneMode.Additive);

            yield return new WaitUntil(() => async.isDone);

            async = SceneManager.UnloadSceneAsync(loadingScreen_Scene);

            yield return new WaitUntil(() => async.isDone);

            SBAppEvents.OnStoryReadyToPlay.Invoke();
        }

        private IEnumerator LoadEmbeddedStorySceneAsync()
        {
            Debug.Log("Loading scene from " + storyBundleName + " bundle");

            AssetBundle bundle = AssetBundle.LoadFromFile(SBToolsPaths.URL_STREAMING_ASSETS_FOLDER + storyBundleName + ".unity3d");

            lastLoadedStoryBundle = bundle.name;
            
            string storySceneName = String.Empty;

            if (bundle.isStreamedSceneAssetBundle)
            {
                string[] scenePaths = bundle.GetAllScenePaths();

                storySceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);
            }

            if (storySceneName == string.Empty)
            {
                Debug.LogError("[StoryScenesLoader] Scenes not found in bundles");
                yield break;
            }

            yield return StartCoroutine(LoadScenes(storySceneName));
        }

        private IEnumerator LoadStoryFromBuildSettings()
        {
            yield return StartCoroutine(LoadScenes(SBToolsPaths.STORY_SCENE_NAME));
        }
    }
}