﻿namespace Within.SuperBloom
{
    public static class SBArguments
    {
        public static bool IsBatchMode
        {
            get
            {
                string commandLineOptions = System.Environment.CommandLine;

                return commandLineOptions.Contains("-batchmode");
            }
        }
    }
}