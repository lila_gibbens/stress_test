using UnityEngine;

public class MicrophoneRequest
{
    public static bool requested = false;
    public static void Request()
    {
        if (!requested)
        {
            Microphone.Start(null, false, 1, 44100);
            requested = true;
        }
    }
}
