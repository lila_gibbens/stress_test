﻿using UnityEngine;

namespace Within.SuperBloom
{
    public class CoroutinesRunner : MonoBehaviour
    {
        private static CoroutinesRunner instance;

        public static CoroutinesRunner Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject runnerGo = new GameObject("CoroutinesRunner");

                    instance = runnerGo.AddComponent<CoroutinesRunner>();
                    
                    DontDestroyOnLoad(runnerGo);
                }
                
                return instance;
            }
        }
    }
}