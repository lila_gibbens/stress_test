﻿using UnityEngine;

namespace Within.SuperBloom
{
    public class AutoLoadingForContentCreators : MonoBehaviour
    {
        private void Start()
        {
            StoryScenesLoader.Instance.LoadStoryFromBuildSettings();
        }
    }
}