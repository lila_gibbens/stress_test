﻿using System;
using System.IO;
using UnityEngine;

namespace Within.SuperBloom
{
    public static class SBToolsPaths
    {
        public static string ConvertToAbsolute(string localPath)
        {
            string[] parts = localPath.Split(new string[] { "Assets/" }, StringSplitOptions.None);

            string localUrl = parts.Length == 2 ? parts[1] : parts[0];

            return Application.dataPath + "/" + localUrl;
        }

        public static string GetProjectDirectory()
        {
            return Directory.GetParent(Application.dataPath).FullName.Replace('\\', '/') + "/";
        }

/*--------------------------------------------------------Developer-Tools---------------------------------------------------------------------------*/
        public static string VERSION_FILE_PATH { get { return Application.dataPath + "/" + "WithinTools/Data/tool_version.txt"; } }
        
        public static string STORY_TOOL_VERSION_FILE_PATH { get { return Application.dataPath + "/" + "StoryData/story_tool_version.txt"; } }

        public static readonly string SETTINGS_ASSET_NAME = "Settings";
        public static readonly string URL_SETTINGS_ASSET  = "Assets/WithinTools/Data/" + SETTINGS_ASSET_NAME;
        
        public static string URL_STREAMING_ASSETS_FOLDER { get { return Application.streamingAssetsPath + "/"; } }
        
/*----------------------------------------------------------Content-Tools---------------------------------------------------------------------------*/       
        public const string URL_STORY_DATA_FOLDER = "Assets/StoryData/";

        public const string STORY_DATA_ASSET_NAME = "StoryDataAsset";
        public const string STORY_DATA_ASSET_PATH = URL_STORY_DATA_FOLDER + STORY_DATA_ASSET_NAME;
        
        public const string URL_ASSET_BUNDLE_EXPORT_FOLDER = "Assets/StoryData/AssetBundles/";
        
        private const string STORY_META_JSON_PATH  = URL_ASSET_BUNDLE_EXPORT_FOLDER;

        public static string GetStoryDataJsonPath(string bundleName)
        {
            return STORY_META_JSON_PATH + bundleName + ".json";
        }

        public const string STORY_PAGES_DATA_PATH = URL_STORY_DATA_FOLDER + "StoryPages/";
        public const string STORY_PAGES_DATA_DELETED_PATH = URL_STORY_DATA_FOLDER + "DeletedFiles/";

        public static readonly string[] TOOL_FOLDERS_TO_EXPORT = { "Assets/WithinTools", "Assets/Demigiant", "Assets/TextMesh Pro", "Assets/DynamicBone", "Assets/Crazy Minnow Studio" };
        public static readonly string TOOL_PACKAGE_NAME = "WithinTools.unitypackage";

/*--------------------------------------------------------------Scenes------------------------------------------------------------------------------*/
                
        public static readonly string MAIN_SCENE_FOLDER_PATH = "Assets/WithinTools/Scenes/";
        public static readonly string UNITY_SCENE_EXTENSION = ".unity";

        public static readonly string LOADING_SCREEN_SCENE_NAME = "StoryLoadingScreen";
        public static readonly string LOADING_SCENE_ASSET_PATH = MAIN_SCENE_FOLDER_PATH + LOADING_SCREEN_SCENE_NAME + UNITY_SCENE_EXTENSION;
        
        public static readonly string HOME_SCREEN_SCENE_NAME = "StorySelect";
        public static readonly string HOME_SCENE_ASSET_PATH = "Assets/SBApp/Scenes/" + LOADING_SCREEN_SCENE_NAME + UNITY_SCENE_EXTENSION;
        
        public static readonly string STORY_SCENE_NAME = "StoryScene";
        public static readonly string STORY_SCENE_ASSET_PATH = URL_STORY_DATA_FOLDER + STORY_SCENE_NAME + UNITY_SCENE_EXTENSION;
        
        public static readonly string SEQUENCER_FINAL_UI_SCENE_NAME = "ARExperienceUI";
        public static readonly string SEQUENCER_FINAL_UI_SCENE_PATH = MAIN_SCENE_FOLDER_PATH + SEQUENCER_FINAL_UI_SCENE_NAME + UNITY_SCENE_EXTENSION;
        
        public static readonly string AR_REMOTE_SCENE_NAME = "RemoteDevice";
        public static readonly string AR_REMOTE_SCENE_PATH = "Assets/WithinTools/External/UnityARInterface/ARRemote/" + AR_REMOTE_SCENE_NAME + UNITY_SCENE_EXTENSION;

        public static readonly string AR_SCENE_NAME = "ARControl";
        public static readonly string AR_SCENE_PATH = MAIN_SCENE_FOLDER_PATH + AR_SCENE_NAME + UNITY_SCENE_EXTENSION;

        public static readonly string CC_BOOT_SCENE = MAIN_SCENE_FOLDER_PATH + "BootScene_ContentCreators" + UNITY_SCENE_EXTENSION;
        
        public static readonly string[] REQUIRED_SCENES = 
            { 
                CC_BOOT_SCENE, LOADING_SCENE_ASSET_PATH, STORY_SCENE_ASSET_PATH, 
                SEQUENCER_FINAL_UI_SCENE_PATH, AR_SCENE_PATH, AR_REMOTE_SCENE_PATH 
            };
    }
}
